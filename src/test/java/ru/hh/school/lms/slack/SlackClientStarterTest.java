package ru.hh.school.lms.slack;

import com.github.seratch.jslack.api.methods.SlackApiException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.websocket.DeploymentException;
import java.io.IOException;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SlackClientStarterTest {
  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();

  @Mock
  public AnnotationConfigWebApplicationContext appContext;

  @Mock
  public AnnotationConfigWebApplicationContext wrongContext;

  @Mock
  public EventRtmClient jSlackClient;

  @Mock
  public EventRtmClient brokenJSlackClient;

  @Mock
  public EventRtmClient halfBrokenJSlackClient;

  SlackClientStarter slackClientStarter;


  @Before
  public void setUp() throws IOException, SlackRtmConnectionException, SlackApiException, DeploymentException {
    doAnswer(invocation -> {
        throw new IOException();
      }
    ).when(brokenJSlackClient).connect();
    doAnswer(new Answer() {

      private int attemps = 0;

      @Override
      public Object answer(InvocationOnMock invocation) throws Throwable {
        attemps++;
        if (attemps < 2) {
          throw new IOException();
        }
        return null;
      }
    }).when(halfBrokenJSlackClient).connect();
    slackClientStarter = new SlackClientStarter(appContext, jSlackClient);
  }

  @Test
  public void testConnectOnStartup() throws IOException, SlackRtmConnectionException, SlackApiException, DeploymentException {
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    verify(jSlackClient, times(1)).connect();
  }

  @Test
  public void testDontConnectOnWrongContextStartup() throws IOException, SlackRtmConnectionException, SlackApiException, DeploymentException {
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(wrongContext));
    verify(jSlackClient, times(0)).connect();
  }

  @Test
  public void testBrokenConnection() {
    slackClientStarter = new SlackClientStarter(appContext, brokenJSlackClient);
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    verify(appContext, times(1)).close();
  }

  @Test
  public void testDoubleContextRefresh() throws IOException, SlackRtmConnectionException, SlackApiException, DeploymentException {
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    verify(jSlackClient, times(1)).connect();
  }

  @Test
  public void testDoubleContextRefreshWithHalfBrokenConnection()
    throws IOException, SlackRtmConnectionException, SlackApiException, DeploymentException {
    slackClientStarter = new SlackClientStarter(appContext, halfBrokenJSlackClient);
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    slackClientStarter.onApplicationEvent(new ContextRefreshedEvent(appContext));
    verify(halfBrokenJSlackClient, times(2)).connect();
  }

}
