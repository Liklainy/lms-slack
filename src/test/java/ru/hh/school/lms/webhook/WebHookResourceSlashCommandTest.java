package ru.hh.school.lms.webhook;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.test.context.ContextConfiguration;
import ru.hh.nab.starter.NabApplication;
import ru.hh.nab.testbase.NabTestBase;
import ru.hh.school.lms.slack.slashcommand.SlashCommand;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandler;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandlingException;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ContextConfiguration(classes = WebHookTestConfig.class)
public class WebHookResourceSlashCommandTest extends NabTestBase {

  private static final String URL = "/slash-command";

  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();

  @Inject
  MockSlashCommandHandler mockWrapper;

  @Mock
  SlashCommandHandler handler;

  @Before
  public void setUp() throws SlashCommandHandlingException {
    // setup handler
    mockWrapper.setMock(handler);
    doAnswer(invocation -> {
      SlashCommand command = invocation.getArgument(0);
      return command.getCommand().equals("/testcommand");
    }).when(handler).test(any(SlashCommand.class));
    doAnswer(invocation -> {
      return null;
    }).when(handler).process(any(SlashCommand.class));
    // setup
  }

  @SuppressWarnings("unchecked")
  private final MultivaluedHashMap<String, String> validFormMap = new MultivaluedHashMap() {
    {
      put("token", List.of("ABsdAHeTSsQT33yk4G43sdNz"));
      put("team_id", List.of("TGJSasFG5"));
      put("team_domain", List.of("test_domain"));
      put("enterprise_id", List.of("null"));
      put("enterprise_name", List.of("null"));
      put("channel_id", List.of("DGJGASRQV"));
      put("channel_name", List.of("directmessage"));
      put("user_id", List.of("UGH5ASDRQ"));
      put("user_name", List.of("test_user"));
      put("command", List.of("/testcommand"));
      put("text", List.of("https://hooks.slack.com/commands/TGJSasFG5/458679621857/1TWqsVkiEq6bXZNUrG7df30W"));
      put("response_url", List.of("519572813834.285622089354.ed74d1c3ea94c801089bfaba338d122d"));
      put("trigger_id", List.of(""));
    }
  };


  @Test
  public void testValidSlashCommand() throws UnsupportedEncodingException, SlashCommandHandlingException {
    Response response = createRequest(URL)
      .post(Entity.form(validFormMap));
    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    verify(handler, times(1)).test(any(SlashCommand.class));
    verify(handler, times(1)).process(any(SlashCommand.class));
  }

  @Test
  public void testInvalidRequestSlashCommand() throws SlashCommandHandlingException {
    final Form form = new Form().param("president", "Barack Obama");
    Response response = createRequest(URL)
      .post(Entity.form(form));
    assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
    verify(handler, never()).test(any(SlashCommand.class));
    verify(handler, never()).process(any(SlashCommand.class));
  }

  @Test
  public void testNoCommandMatch() throws SlashCommandHandlingException {
    validFormMap.put("command", List.of("/some-unregistered-slash-command"));
    Response response = createRequest(URL)
      .post(Entity.form(new Form(validFormMap)));
    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    verify(handler, only()).test(any(SlashCommand.class));
    verify(handler, never()).process(any(SlashCommand.class));
  }

  @Override
  protected NabApplication getApplication() {
    return NabApplication.builder().configureJersey().bindToRoot().build();
  }
}
