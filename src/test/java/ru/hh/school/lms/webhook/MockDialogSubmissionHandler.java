package ru.hh.school.lms.webhook;

import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

public class MockDialogSubmissionHandler implements DialogSubmissionHandler {

  private DialogSubmissionHandler mock;

  public DialogSubmissionHandler getMock() {
    return mock;
  }

  public void setMock(DialogSubmissionHandler mock) {
    this.mock = mock;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    mock.onEvent(event);
  }

  @Override
  public boolean test(DialogSubmissionEvent dialogSubmissionEvent) {
    return true;
  }
}
