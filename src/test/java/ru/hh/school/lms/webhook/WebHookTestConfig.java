package ru.hh.school.lms.webhook;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.nab.testbase.NabTestConfig;
import ru.hh.school.lms.service.person.PersonService;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Configuration
@Import({NabTestConfig.class, WebHookResource.class})
public class WebHookTestConfig {

  @Bean
  public MockSlashCommandHandler testSlashCommandHandler() {
    return new MockSlashCommandHandler();
  }

  @Bean
  public MockDialogSubmissionHandler mockDialogSubmissionListener() {
    return new MockDialogSubmissionHandler();
  }

  @Bean
  public Gson gson() {
    return new GsonBuilder()
      .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      .create();
  }

  @Bean
  public PersonService personService() {
    PersonService mockPersonService = mock(PersonService.class);
    when(mockPersonService.findBySlackAccountId(any(String.class))).thenReturn(Optional.empty());
    return mockPersonService;
  }

}
