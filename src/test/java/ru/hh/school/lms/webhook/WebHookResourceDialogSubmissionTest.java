package ru.hh.school.lms.webhook;

import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.test.context.ContextConfiguration;
import ru.hh.nab.starter.NabApplication;
import ru.hh.nab.testbase.NabTestBase;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandlingException;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(classes = WebHookTestConfig.class)
public class WebHookResourceDialogSubmissionTest extends NabTestBase {

  private static final String URL = "/interactive-component";

  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();

  @Inject
  MockDialogSubmissionHandler mockWrapper;

  @Mock
  DialogSubmissionHandler listener;


  private final JsonObject jsonObject = new JsonObject();

  @Before
  public void setUp() throws SlashCommandHandlingException {
    jsonObject.addProperty("type", "dialog_submission");
    jsonObject.addProperty("token", "HZXT4NQGBdTY4owzGA3ykEuX");
    jsonObject.addProperty("action_ts", "070194");
    jsonObject.addProperty("callback_id", "text");
    jsonObject.addProperty("response_url", "https://hooks.slack.com/app/TGG195JSL/596704359597/f1LCiZz2p0CIjnHQL49YUsKT");
    jsonObject.addProperty("state", ":");

    JsonObject teamObject = new JsonObject();
    teamObject.addProperty("id", "TG1J9SLG5");
    teamObject.addProperty("domain", "test_domain");
    jsonObject.add("team", teamObject);

    JsonObject userObject = new JsonObject();
    teamObject.addProperty("id", "UHF3GD7U3");
    teamObject.addProperty("name", "test_user_name");
    jsonObject.add("user", userObject);

    JsonObject channelObject = new JsonObject();
    teamObject.addProperty("id", "DE7GJG7RD");
    teamObject.addProperty("name", "directmessage");
    jsonObject.add("channel", userObject);

    JsonObject submissionObject = new JsonObject();
    teamObject.addProperty("quantity", "1");
    teamObject.addProperty("color", "#FFFFFF");
    jsonObject.add("submission", userObject);

    mockWrapper.setMock(listener);
  }

  @Test
  public void testValidDialogSubmission() throws UnsupportedEncodingException, SlashCommandHandlingException {
    final Form form = new Form().param("payload", jsonObject.toString());
    Response response = createRequest(URL)
      .post(Entity.form(form));
    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
  }

  @Override
  protected NabApplication getApplication() {
    return NabApplication.builder().configureJersey().bindToRoot().build();
  }
}
