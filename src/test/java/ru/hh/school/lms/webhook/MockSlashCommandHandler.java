package ru.hh.school.lms.webhook;

import ru.hh.school.lms.slack.slashcommand.SlashCommand;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandler;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandlingException;

class MockSlashCommandHandler implements SlashCommandHandler {

  private SlashCommandHandler mock;

  public SlashCommandHandler getMock() {
    return mock;
  }

  public void setMock(SlashCommandHandler mock) {
    this.mock = mock;
  }

  @Override
  public void process(SlashCommand command) throws SlashCommandHandlingException {
    mock.process(command);
  }

  @Override
  public boolean test(SlashCommand slashCommand) {
    return mock.test(slashCommand);
  }


}
