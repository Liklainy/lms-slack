package ru.hh.school.lms.service.grading.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.NamingConventions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.datasource.DataSourceFactory;
import ru.hh.nab.datasource.DataSourceType;
import ru.hh.nab.testbase.postgres.embedded.EmbeddedPostgresDataSourceFactory;
import ru.hh.school.lms.repository.ReviewRepository;
import ru.hh.school.lms.repository.SubmissionRepository;
import ru.hh.school.lms.service.grading.dao.GradingDao;
import ru.hh.school.lms.service.grading.dao.SpringDataGradingDao;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("ru.hh.school.lms.repository")
class GradingServiceTestConfig {

  @Bean
  DataSource dataSource(Environment props) {
    DataSourceFactory factory = new EmbeddedPostgresDataSourceFactory();
    Properties dsProps = new Properties();
    dsProps.setProperty("master.pool.maximumPoolSize", "2");
    DataSource dataSource = factory.create(DataSourceType.MASTER, false, new FileSettings(dsProps));
    String schemaSql = "sql/schema.sql";
    Resource sqlResource = new ClassPathResource(schemaSql);
    DatabasePopulator populator = new ResourceDatabasePopulator(sqlResource);
    DatabasePopulatorUtils.execute(populator, dataSource);
    return dataSource;
  }

  @Bean
  JpaTransactionManager transactionManager(EntityManagerFactory emf) {
    return new JpaTransactionManager(emf);
  }

  @Bean
  JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
    jpaVendorAdapter.setDatabase(Database.POSTGRESQL);
    jpaVendorAdapter.setShowSql(true);
    jpaVendorAdapter.setGenerateDdl(false);
    return jpaVendorAdapter;
  }

  @Bean
  LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
    LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
    factoryBean.setDataSource(dataSource);
    factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
    factoryBean.setPackagesToScan(
      "ru.hh.school.lms.service.person",
      "ru.hh.school.lms.service.grading"
      // <add your package here>
    );
    return factoryBean;
  }

  @Bean
  ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.getConfiguration()
      .setFieldMatchingEnabled(true)
      .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
      .setSourceNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
    return modelMapper;
  }

  @Bean
  GradingDao gradingDao(SubmissionRepository submissionRepository, ReviewRepository reviewRepository) {
    return new SpringDataGradingDao(submissionRepository, reviewRepository);
  }

  @Bean
  GradingService homeworkGradingService(ModelMapper modelMapper, GradingDao gradingDao) {
    return new GradingService(modelMapper, gradingDao);
  }
}
