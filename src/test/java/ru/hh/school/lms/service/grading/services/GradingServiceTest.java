package ru.hh.school.lms.service.grading.services;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.hh.school.lms.service.grading.dto.ReviewDto;
import ru.hh.school.lms.service.grading.dto.SubmissionDto;

import java.util.List;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {GradingServiceTestConfig.class})
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
public class GradingServiceTest {

  @Autowired
  private GradingService service;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Ignore
  @Test
  public void addSubmission() {
    final Long studentId = 1L;
    final Long homeworkId = 1L;
    final String githubUrl = "https://github.com/student/homework";
    final String description = "my first homework";

    SubmissionDto result = service.submitHomework(studentId, homeworkId, githubUrl, description);

    Assert.assertEquals(studentId, result.getStudentId());
    Assert.assertEquals(homeworkId, result.getHomeworkId());
    Assert.assertEquals(githubUrl, result.getGithubUrl());
    Assert.assertEquals(description, result.getDescription());
  }

  @Ignore
  @Test
  public void getSubmission() {
    final Long studentId = 1L;
    final Long homeworkId = 1L;
    final String githubUrl = "https://github.com/student/homework";
    final String description = "my first homework";
    service.submitHomework(studentId, homeworkId, githubUrl, description);

    SubmissionDto result = service.getSubmission(studentId, homeworkId);

    Assert.assertEquals(studentId, result.getStudentId());
    Assert.assertEquals(homeworkId, result.getHomeworkId());
    Assert.assertEquals(githubUrl, result.getGithubUrl());
    Assert.assertEquals(description, result.getDescription());
  }

  @Ignore
  @Test
  public void getSubmissions() {
    final Long studentId = 1L;
    final Long homeworkId = 1L;
    final String githubUrl = "https://github.com/student/homework";
    final String description = "my first homework";
    service.submitHomework(studentId, homeworkId, githubUrl, description);

    List<SubmissionDto> result = service.getSubmissions(homeworkId);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(studentId, result.get(0).getStudentId());
    Assert.assertEquals(homeworkId, result.get(0).getHomeworkId());
    Assert.assertEquals(githubUrl, result.get(0).getGithubUrl());
    Assert.assertEquals(description, result.get(0).getDescription());
  }

  @Ignore
  @Test
  public void updateSubmission() {
    final Long studentId = 1L;
    final Long homeworkId = 1L;
    final String githubUrl = "https://github.com/student/homework";
    final String description = "my first homework";
    final String updatedDescription = "fixed";
    SubmissionDto submission = service.submitHomework(studentId, homeworkId, githubUrl, description);

    submission.setDescription(updatedDescription);
    service.updateSubmission(submission);

    SubmissionDto result = service.getSubmission(studentId, homeworkId);
    Assert.assertEquals(studentId, result.getStudentId());
    Assert.assertEquals(homeworkId, result.getHomeworkId());
    Assert.assertEquals(githubUrl, result.getGithubUrl());
    Assert.assertEquals(updatedDescription, result.getDescription());
  }

  @Ignore
  @Test
  public void addReview() {
    final Long studentId = 1L;
    final Long homeworkId = 1L;
    final String githubUrl = "https://github.com/student/homework";
    final String description = "my first homework";
    SubmissionDto submission = service.submitHomework(studentId, homeworkId, githubUrl, description);
    final double grade = 5.0;
    final String content = "Good";

    ReviewDto result = service.reviewSubmission(submission.getSubmissionId(), grade, content);

    Assert.assertEquals(submission.getSubmissionId(), result.getSubmissionId());
    Assert.assertEquals(grade, result.getGrade(), 0.0);
    Assert.assertEquals(content, result.getContent());
  }

  @Ignore
  @Test
  public void getUnreviewedSubmissions() {
    final Long homeworkId = 1L;
    SubmissionDto firstSubmission = service.submitHomework(1L, homeworkId, "https://github.com/student_1/hw", "1");
    SubmissionDto secondSubmission = service.submitHomework(2L, homeworkId, "https://github.com/student_2/hw", "2");
    service.reviewSubmission(secondSubmission.getSubmissionId(), null, "review");

    List<SubmissionDto> result = service.getUnreviewedSubmissions(homeworkId);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(firstSubmission.getGithubUrl(), result.get(0).getGithubUrl());
    Assert.assertEquals(firstSubmission.getDescription(), result.get(0).getDescription());
  }

  @Ignore
  @Test
  public void getUngradedSubmissions() {
    final Long homeworkId = 1L;
    SubmissionDto firstSubmission = service.submitHomework(1L, homeworkId, "https://github.com/student_1/hw", "1");
    SubmissionDto secondSubmission = service.submitHomework(2L, homeworkId, "https://github.com/student_2/hw", "2");
    service.reviewSubmission(firstSubmission.getSubmissionId(), 5.0, "review_1");
    service.reviewSubmission(secondSubmission.getSubmissionId(), null, "review_2");

    List<SubmissionDto> result = service.getUngradedSubmissions(homeworkId);

    Assert.assertEquals(1, result.size());
    Assert.assertEquals(secondSubmission.getGithubUrl(), result.get(0).getGithubUrl());
    Assert.assertEquals(secondSubmission.getDescription(), result.get(0).getDescription());
  }
}
