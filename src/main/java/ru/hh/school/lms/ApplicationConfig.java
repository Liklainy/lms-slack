package ru.hh.school.lms;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.nab.starter.NabProdConfig;
import ru.hh.school.lms.sheets.GoogleSpreadsheetsConfig;
import ru.hh.school.lms.slack.SlackConfig;
import ru.hh.school.lms.slack.event.CallbackStorage;

@Configuration
@Import({NabProdConfig.class, DataConfig.class, SlackConfig.class, GoogleSpreadsheetsConfig.class})
@ComponentScan("ru.hh.school.lms")
class ApplicationConfig {

  @Bean
  public Gson gson() {
    return new GsonBuilder()
      .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      .create();
  }

  @Bean
  ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Bean
  ObjectMapper objectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    return objectMapper;
  }

  @Bean
  CallbackStorage callbackStorage() {
    return new CallbackStorage();
  }
}
