package ru.hh.school.lms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import ru.hh.nab.starter.NabApplication;
import ru.hh.school.lms.webhook.ExceptionLogger;

public final class Application {

  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) {
    LOGGER.info("Application is starting...");
    AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
    context.register(ApplicationConfig.class);
    context.refresh();
    NabApplication nabApplication = NabApplication.builder()
      .configureJersey(JerseyConfig.class)
      .registerResources(ExceptionLogger.class)
      .bindToRoot()
      .build();
    nabApplication.run(context);
  }
}
