package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.person.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByTitle(String title);
}
