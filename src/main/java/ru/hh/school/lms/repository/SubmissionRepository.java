package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.grading.entities.Submission;

import java.util.List;

public interface SubmissionRepository extends JpaRepository<Submission, Long> {
  Submission findByStudentIdAndHomeworkId(Long studentId, Long homeworkId);

  List<Submission> findByHomeworkId(Long homeworkId);

  List<Submission> findByStudentId(Long studentId);

  List<Submission> findByReviewsIsNull();

  List<Submission> findByReviewsGradeIsNull();
}
