package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.notifications.model.Notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends JpaRepository<Notification, Long> {

  List<Notification> findAllByDateTimeLessThanEqualOrderByDateTime(LocalDateTime date);

  void deleteAllByDateTimeLessThanEqual(LocalDateTime date);

  Optional<Notification> findFirstByOrderByDateTime();
}
