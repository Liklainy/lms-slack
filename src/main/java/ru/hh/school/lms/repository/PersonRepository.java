package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.person.model.Role;

import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {

  Iterable<Person> findAllByRolesContains(Role role);

  Optional<Person> findBySlackAccountId(String slackAccountId);
}
