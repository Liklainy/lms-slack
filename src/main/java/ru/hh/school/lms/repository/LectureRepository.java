package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.lectures.model.Lecture;

import java.time.LocalDate;
import java.util.List;

public interface LectureRepository extends JpaRepository<Lecture, Long> {

  List<Lecture> findAllByOrderByDateAsc();

  List<Lecture> findAllByDateGreaterThanEqualOrderByDate(LocalDate date);

  List<Lecture> findByDateBetweenOrderByDate(LocalDate dateAfter, LocalDate dateBefore);
}
