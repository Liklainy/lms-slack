package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.grading.entities.Review;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {
  List<Review> findBySubmissionId(Long submissionId);
}
