package ru.hh.school.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hh.school.lms.service.tasks.model.Task;

import java.time.LocalDate;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

  List<Task> findAllByOrderByDeadlineAsc();

  List<Task> findAllByDeadlineGreaterThanEqualOrderByDeadline(LocalDate date);

  List<Task> findByDeadlineBetweenOrderByDeadline(LocalDate dateAfter, LocalDate dateBefore);
}
