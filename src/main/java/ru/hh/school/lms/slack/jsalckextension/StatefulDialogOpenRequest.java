package ru.hh.school.lms.slack.jsalckextension;

import com.github.seratch.jslack.api.methods.request.dialog.DialogOpenRequest;

public class StatefulDialogOpenRequest {

  private final DialogOpenRequest dialogOpenRequest;
  private final StatefulDialog dialog;

  public StatefulDialogOpenRequest(DialogOpenRequest dialogOpenRequest, String state) {
    this.dialogOpenRequest = dialogOpenRequest;
    this.dialog = new StatefulDialog(dialogOpenRequest.getDialog(), state);
  }

  DialogOpenRequest getDialogOpenRequest() {
    return dialogOpenRequest;
  }

  public StatefulDialog getDialog() {
    return dialog;
  }
}
