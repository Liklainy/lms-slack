package ru.hh.school.lms.slack.event;

import com.google.gson.annotations.SerializedName;

public final class MessagePostedEvent extends ContextAwareEvent {
  private String text;
  private String username;
  private String botId;
  @SerializedName("mrkdwn")
  private String markdown;
  private String subtype;
  @SerializedName("event_ts")
  private String eventTs;
  private String ts;
  @SerializedName("client_msg_id")
  private String clientMsgId;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getBotId() {
    return botId;
  }

  public void setBotId(String botId) {
    this.botId = botId;
  }

  public String getMrkdwn() {
    return markdown;
  }

  public void setMrkdwn(String mrkdwn) {
    this.markdown = markdown;
  }

  public String getSubtype() {
    return subtype;
  }

  public void setSubtype(String subtype) {
    this.subtype = subtype;
  }

  public String getEventTs() {
    return eventTs;
  }

  public void setEventTs(String eventTs) {
    this.eventTs = eventTs;
  }

  public String getTs() {
    return ts;
  }

  public void setTs(String ts) {
    this.ts = ts;
  }

  public String getClientMsgId() {
    return clientMsgId;
  }

  public void setClientMsgId(String clientMsgId) {
    this.clientMsgId = clientMsgId;
  }

  @Override
  public String toString() {
    return "MessagePostedEvent{" +
      "text='" + text + '\'' +
      ", username='" + username + '\'' +
      ", botId='" + botId + '\'' +
      ", mrkdwn='" + markdown + '\'' +
      ", type='" + getType() + '\'' +
      ", subtype='" + subtype + '\'' +
      ", eventTs='" + eventTs + '\'' +
      ", ts='" + ts + '\'' +
      ", clientMsgId='" + clientMsgId + '\'' +
      '}';
  }
}
