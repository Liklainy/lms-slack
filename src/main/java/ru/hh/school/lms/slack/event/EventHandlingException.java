package ru.hh.school.lms.slack.event;

public class EventHandlingException extends Exception {

  private Event event;

  public EventHandlingException(Event event) {
    this.event = event;
  }

  public EventHandlingException(Event event, Throwable cause) {
    super(cause);
    this.event = event;
  }

  public EventHandlingException(Event event, String message) {
    super(message);
    this.event = event;
  }

  public EventHandlingException(Event event, String message, Throwable cause) {
    super(message, cause);
    this.event = event;
  }
}
