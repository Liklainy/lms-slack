package ru.hh.school.lms.slack;

import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.methods.SlackApiResponse;
import com.github.seratch.jslack.api.methods.request.chat.ChatPostMessageRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatUpdateRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsHistoryRequest;
import com.github.seratch.jslack.api.methods.request.dialog.DialogOpenRequest;
import com.github.seratch.jslack.api.methods.request.im.ImOpenRequest;
import com.github.seratch.jslack.api.methods.response.chat.ChatPostMessageResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatUpdateResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsHistoryResponse;
import com.github.seratch.jslack.api.methods.response.dialog.DialogOpenResponse;
import com.github.seratch.jslack.api.methods.response.im.ImOpenResponse;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.school.lms.slack.event.CallbackEventHandler;
import ru.hh.school.lms.slack.event.ContextAwareEvent;
import ru.hh.school.lms.slack.event.Event;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.jsalckextension.MethodsClientExtension;
import ru.hh.school.lms.slack.jsalckextension.StatefulDialogOpenRequest;

import java.io.IOException;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Component
public final class SlackHelper {

  private final String slackBotToken;
  private final MethodsClientExtension methodsClient;

  @Autowired
  SlackHelper(FileSettings settings, MethodsClientExtension methodsClient) {
    this.slackBotToken = settings.getString("ru.hh.school.lms.slack.botToken");
    this.methodsClient = methodsClient;
  }

  public void notify(String userId, String responseMessage, List<Attachment> attachments)
    throws IOException, SlackApiException, NullPointerException {
    ImOpenResponse openResponse = methodsClient.imOpen(ImOpenRequest.builder()
      .token(slackBotToken)
      .user(userId)
      .build());
    String channelId;
    try {
      channelId = openResponse.getChannel().getId();
      ChatPostMessageRequest.ChatPostMessageRequestBuilder builder = ChatPostMessageRequest.builder()
        .token(slackBotToken)
        .channel(channelId)
        .text(responseMessage)
        .attachments(attachments);
      ChatPostMessageResponse response = methodsClient.chatPostMessage(builder.build());
    } catch (NullPointerException e) {
      throw new NullPointerException();
    }
  }

  public void respond(ContextAwareEvent event, String responseMessage, Class<? extends CallbackEventHandler> handlerClass, Attachment attachment)
      throws EventHandlingException {
    attachment.setCallbackId(handlerClass.getCanonicalName());
    respond(event, responseMessage, attachment);
  }

  public void respond(ContextAwareEvent event, String responseMessage, Attachment... attachments) throws EventHandlingException {
    try {
      ChatPostMessageRequest.ChatPostMessageRequestBuilder builder = ChatPostMessageRequest.builder()
        .token(slackBotToken)
        .channel(event.getChannel().getId())
        .text(responseMessage)
        .attachments(List.of(attachments));
      ChatPostMessageResponse response = methodsClient.chatPostMessage(builder.build());
      checkResponseIsOk(event, response);
    } catch (IOException | SlackApiException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  public void update(MessageInteractionEvent event, String responseMessage, Attachment... attachments) throws EventHandlingException {
    String channel = requireNonNull(event.getChannel().getId());
    Message originalMessage = requireNonNull(event.getOriginalMessage());
    String timeStamp = requireNonNull(originalMessage.getTs());
    update(event, channel, timeStamp, responseMessage, attachments);
  }

  public void update(MessageInteractionEvent event, String responseMessage, Class<? extends CallbackEventHandler> handlerClass, Attachment attachment)
    throws EventHandlingException {
    attachment.setCallbackId(handlerClass.getCanonicalName());
    String channel = requireNonNull(event.getChannel().getId());
    Message originalMessage = requireNonNull(event.getOriginalMessage());
    String timeStamp = requireNonNull(originalMessage.getTs());
    update(event, channel, timeStamp, responseMessage, attachment);
  }

  public void update(Event event, String channel, String ts, String responseMessage, Attachment... attachments) throws EventHandlingException {
    try {
      requireNonNull(event);
      ChatUpdateRequest.ChatUpdateRequestBuilder builder = ChatUpdateRequest.builder()
        .token(slackBotToken)
        .channel(channel)
        .ts(ts)
        .text(responseMessage)
        .attachments(List.of(attachments));
      ChatUpdateResponse response = methodsClient.chatUpdate(builder.build());
      checkResponseIsOk(event, response);
    } catch (IOException | SlackApiException | NullPointerException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  public void dialog(MessageInteractionEvent event,
                     Class<? extends CallbackEventHandler> handlerClass, Dialog dialog) throws EventHandlingException {
    try {
      dialog.setCallbackId(handlerClass.getCanonicalName());
      dialog(event, dialog);
    } catch (NullPointerException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  public void dialog(MessageInteractionEvent event, Dialog dialog) throws EventHandlingException {
    try {
      requireNonNull(event);
      String triggerId = requireNonNull(event.getTriggerId());
      DialogOpenResponse response = methodsClient.dialogOpen(DialogOpenRequest.builder()
        .token(slackBotToken)
        .triggerId(triggerId)
        .dialog(dialog)
        .build());
      checkResponseIsOk(event, response);
    } catch (IOException | SlackApiException | NullPointerException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  public void dialog(MessageInteractionEvent event, Dialog dialog,
                     Class<? extends CallbackEventHandler> handlerClass, String state) throws EventHandlingException {
    dialog.setCallbackId(handlerClass.getCanonicalName());
    dialog(event, dialog, state);
  }

  public void dialog(MessageInteractionEvent event, Dialog dialog, String state) throws EventHandlingException {
    try {
      requireNonNull(event);
      String triggerId = requireNonNull(event.getTriggerId());
      DialogOpenRequest dialogOpenRequest = DialogOpenRequest.builder()
        .token(slackBotToken)
        .triggerId(triggerId)
        .dialog(dialog)
        .build();
      StatefulDialogOpenRequest request =
        new StatefulDialogOpenRequest(dialogOpenRequest, state);
      DialogOpenResponse response = methodsClient.dialogOpen(request);
      checkResponseIsOk(event, response);
    } catch (IOException | SlackApiException | NullPointerException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  public Message getSingleMessage(Event event, String channel, String ts) throws EventHandlingException {
    return getLatestMessages(event, channel, ts).get(0);
  }

  public List<Message> getLatestMessages(Event event, String channel, String ts) throws EventHandlingException {
    try {
      ConversationsHistoryResponse historyResponse = methodsClient.conversationsHistory(ConversationsHistoryRequest.builder()
        .token(slackBotToken)
        .channel(channel)
        .inclusive(true)
        .oldest(ts)
        .build());
      if (historyResponse.getMessages().isEmpty()) {
        throw new EventHandlingException(event, String.format("Can't get messages from %s channel from %s timestamp", channel, ts));
      }
      return historyResponse.getMessages();
    } catch (IOException | SlackApiException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  private static void checkResponseIsOk(Event event, SlackApiResponse response) throws EventHandlingException {
    if (!response.isOk()) {
      throw new EventHandlingException(event, "Slack method invocation response contains errors: " + response);
    }
  }
}
