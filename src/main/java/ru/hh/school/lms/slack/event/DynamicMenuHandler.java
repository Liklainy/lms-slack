package ru.hh.school.lms.slack.event;


public interface DynamicMenuHandler extends EventHandler<MessageInteractionEvent> {

  Object respondOnMessagePosted(MessageInteractionEvent event);

  @Override
  default void onEvent(MessageInteractionEvent event) {
    respondOnMessagePosted(event);
  }
}
