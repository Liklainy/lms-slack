package ru.hh.school.lms.slack.event;

public interface DialogSubmissionHandler extends CallbackEventHandler<DialogSubmissionEvent> {
}
