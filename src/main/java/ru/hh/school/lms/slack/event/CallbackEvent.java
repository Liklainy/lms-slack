package ru.hh.school.lms.slack.event;

import com.google.gson.annotations.SerializedName;

public class CallbackEvent extends ContextAwareEvent {
  @SerializedName("callback_id")
  protected String callbackId;

  public String getCallbackId() {
    return callbackId;
  }

  public void setCallbackId(String callbackId) {
    this.callbackId = callbackId;
  }
}
