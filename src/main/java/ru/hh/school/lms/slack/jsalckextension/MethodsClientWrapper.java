package ru.hh.school.lms.slack.jsalckextension;

import com.github.seratch.jslack.api.methods.MethodsClient;
import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.methods.impl.MethodsClientImpl;
import com.github.seratch.jslack.api.methods.request.api.ApiTestRequest;
import com.github.seratch.jslack.api.methods.request.apps.permissions.AppsPermissionsInfoRequest;
import com.github.seratch.jslack.api.methods.request.apps.permissions.AppsPermissionsRequestRequest;
import com.github.seratch.jslack.api.methods.request.auth.AuthRevokeRequest;
import com.github.seratch.jslack.api.methods.request.auth.AuthTestRequest;
import com.github.seratch.jslack.api.methods.request.bots.BotsInfoRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsArchiveRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsCreateRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsHistoryRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsInfoRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsInviteRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsJoinRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsKickRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsLeaveRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsListRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsMarkRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsRenameRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsRepliesRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsSetPurposeRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsSetTopicRequest;
import com.github.seratch.jslack.api.methods.request.channels.ChannelsUnarchiveRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatDeleteRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatGetPermalinkRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatMeMessageRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatPostEphemeralRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatPostMessageRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatUnfurlRequest;
import com.github.seratch.jslack.api.methods.request.chat.ChatUpdateRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsArchiveRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsCloseRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsCreateRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsHistoryRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsInfoRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsInviteRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsJoinRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsKickRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsLeaveRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsListRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsMembersRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsOpenRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsRenameRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsRepliesRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsSetPurposeRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsSetTopicRequest;
import com.github.seratch.jslack.api.methods.request.conversations.ConversationsUnarchiveRequest;
import com.github.seratch.jslack.api.methods.request.dialog.DialogOpenRequest;
import com.github.seratch.jslack.api.methods.request.dnd.DndEndDndRequest;
import com.github.seratch.jslack.api.methods.request.dnd.DndEndSnoozeRequest;
import com.github.seratch.jslack.api.methods.request.dnd.DndInfoRequest;
import com.github.seratch.jslack.api.methods.request.dnd.DndSetSnoozeRequest;
import com.github.seratch.jslack.api.methods.request.dnd.DndTeamInfoRequest;
import com.github.seratch.jslack.api.methods.request.emoji.EmojiListRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesDeleteRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesInfoRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesListRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesRevokePublicURLRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesSharedPublicURLRequest;
import com.github.seratch.jslack.api.methods.request.files.FilesUploadRequest;
import com.github.seratch.jslack.api.methods.request.files.comments.FilesCommentsAddRequest;
import com.github.seratch.jslack.api.methods.request.files.comments.FilesCommentsDeleteRequest;
import com.github.seratch.jslack.api.methods.request.files.comments.FilesCommentsEditRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsArchiveRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsCloseRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsCreateChildRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsCreateRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsHistoryRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsInfoRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsInviteRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsKickRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsLeaveRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsListRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsMarkRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsOpenRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsRenameRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsRepliesRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsSetPurposeRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsSetTopicRequest;
import com.github.seratch.jslack.api.methods.request.groups.GroupsUnarchiveRequest;
import com.github.seratch.jslack.api.methods.request.im.ImCloseRequest;
import com.github.seratch.jslack.api.methods.request.im.ImHistoryRequest;
import com.github.seratch.jslack.api.methods.request.im.ImListRequest;
import com.github.seratch.jslack.api.methods.request.im.ImMarkRequest;
import com.github.seratch.jslack.api.methods.request.im.ImOpenRequest;
import com.github.seratch.jslack.api.methods.request.im.ImRepliesRequest;
import com.github.seratch.jslack.api.methods.request.migration.MigrationExchangeRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimCloseRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimHistoryRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimListRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimMarkRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimOpenRequest;
import com.github.seratch.jslack.api.methods.request.mpim.MpimRepliesRequest;
import com.github.seratch.jslack.api.methods.request.oauth.OAuthAccessRequest;
import com.github.seratch.jslack.api.methods.request.oauth.OAuthTokenRequest;
import com.github.seratch.jslack.api.methods.request.pins.PinsAddRequest;
import com.github.seratch.jslack.api.methods.request.pins.PinsListRequest;
import com.github.seratch.jslack.api.methods.request.pins.PinsRemoveRequest;
import com.github.seratch.jslack.api.methods.request.reactions.ReactionsAddRequest;
import com.github.seratch.jslack.api.methods.request.reactions.ReactionsGetRequest;
import com.github.seratch.jslack.api.methods.request.reactions.ReactionsListRequest;
import com.github.seratch.jslack.api.methods.request.reactions.ReactionsRemoveRequest;
import com.github.seratch.jslack.api.methods.request.reminders.RemindersAddRequest;
import com.github.seratch.jslack.api.methods.request.reminders.RemindersCompleteRequest;
import com.github.seratch.jslack.api.methods.request.reminders.RemindersDeleteRequest;
import com.github.seratch.jslack.api.methods.request.reminders.RemindersInfoRequest;
import com.github.seratch.jslack.api.methods.request.reminders.RemindersListRequest;
import com.github.seratch.jslack.api.methods.request.rtm.RTMConnectRequest;
import com.github.seratch.jslack.api.methods.request.rtm.RTMStartRequest;
import com.github.seratch.jslack.api.methods.request.search.SearchAllRequest;
import com.github.seratch.jslack.api.methods.request.search.SearchFilesRequest;
import com.github.seratch.jslack.api.methods.request.search.SearchMessagesRequest;
import com.github.seratch.jslack.api.methods.request.stars.StarsAddRequest;
import com.github.seratch.jslack.api.methods.request.stars.StarsListRequest;
import com.github.seratch.jslack.api.methods.request.stars.StarsRemoveRequest;
import com.github.seratch.jslack.api.methods.request.team.TeamAccessLogsRequest;
import com.github.seratch.jslack.api.methods.request.team.TeamBillableInfoRequest;
import com.github.seratch.jslack.api.methods.request.team.TeamInfoRequest;
import com.github.seratch.jslack.api.methods.request.team.TeamIntegrationLogsRequest;
import com.github.seratch.jslack.api.methods.request.team.profile.TeamProfileGetRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.UsergroupsCreateRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.UsergroupsDisableRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.UsergroupsEnableRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.UsergroupsListRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.UsergroupsUpdateRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.users.UsergroupUsersListRequest;
import com.github.seratch.jslack.api.methods.request.usergroups.users.UsergroupUsersUpdateRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersConversationsRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersDeletePhotoRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersGetPresenceRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersIdentityRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersInfoRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersListRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersLookupByEmailRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersSetActiveRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersSetPhotoRequest;
import com.github.seratch.jslack.api.methods.request.users.UsersSetPresenceRequest;
import com.github.seratch.jslack.api.methods.request.users.profile.UsersProfileGetRequest;
import com.github.seratch.jslack.api.methods.request.users.profile.UsersProfileSetRequest;
import com.github.seratch.jslack.api.methods.response.api.ApiTestResponse;
import com.github.seratch.jslack.api.methods.response.apps.permissions.AppsPermissionsInfoResponse;
import com.github.seratch.jslack.api.methods.response.apps.permissions.AppsPermissionsRequestResponse;
import com.github.seratch.jslack.api.methods.response.auth.AuthRevokeResponse;
import com.github.seratch.jslack.api.methods.response.auth.AuthTestResponse;
import com.github.seratch.jslack.api.methods.response.bots.BotsInfoResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsArchiveResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsCreateResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsHistoryResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsInfoResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsInviteResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsJoinResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsKickResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsLeaveResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsListResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsMarkResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsRenameResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsRepliesResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsSetPurposeResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsSetTopicResponse;
import com.github.seratch.jslack.api.methods.response.channels.ChannelsUnarchiveResponse;
import com.github.seratch.jslack.api.methods.response.channels.UsersLookupByEmailResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatDeleteResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatGetPermalinkResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatMeMessageResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatPostEphemeralResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatPostMessageResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatUnfurlResponse;
import com.github.seratch.jslack.api.methods.response.chat.ChatUpdateResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsArchiveResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsCloseResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsCreateResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsHistoryResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsInfoResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsInviteResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsJoinResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsKickResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsLeaveResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsListResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsMembersResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsOpenResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsRenameResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsRepliesResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsSetPurposeResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsSetTopicResponse;
import com.github.seratch.jslack.api.methods.response.conversations.ConversationsUnarchiveResponse;
import com.github.seratch.jslack.api.methods.response.dialog.DialogOpenResponse;
import com.github.seratch.jslack.api.methods.response.dnd.DndEndDndResponse;
import com.github.seratch.jslack.api.methods.response.dnd.DndEndSnoozeResponse;
import com.github.seratch.jslack.api.methods.response.dnd.DndInfoResponse;
import com.github.seratch.jslack.api.methods.response.dnd.DndSetSnoozeResponse;
import com.github.seratch.jslack.api.methods.response.dnd.DndTeamInfoResponse;
import com.github.seratch.jslack.api.methods.response.emoji.EmojiListResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesDeleteResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesInfoResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesListResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesRevokePublicURLResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesSharedPublicURLResponse;
import com.github.seratch.jslack.api.methods.response.files.FilesUploadResponse;
import com.github.seratch.jslack.api.methods.response.files.comments.FilesCommentsAddResponse;
import com.github.seratch.jslack.api.methods.response.files.comments.FilesCommentsDeleteResponse;
import com.github.seratch.jslack.api.methods.response.files.comments.FilesCommentsEditResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsArchiveResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsCloseResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsCreateChildResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsCreateResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsHistoryResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsInfoResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsInviteResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsKickResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsLeaveResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsListResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsMarkResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsOpenResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsRenameResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsRepliesResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsSetPurposeResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsSetTopicResponse;
import com.github.seratch.jslack.api.methods.response.groups.GroupsUnarchiveResponse;
import com.github.seratch.jslack.api.methods.response.im.ImCloseResponse;
import com.github.seratch.jslack.api.methods.response.im.ImHistoryResponse;
import com.github.seratch.jslack.api.methods.response.im.ImListResponse;
import com.github.seratch.jslack.api.methods.response.im.ImMarkResponse;
import com.github.seratch.jslack.api.methods.response.im.ImOpenResponse;
import com.github.seratch.jslack.api.methods.response.im.ImRepliesResponse;
import com.github.seratch.jslack.api.methods.response.migration.MigrationExchangeResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimCloseResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimHistoryResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimListResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimMarkResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimOpenResponse;
import com.github.seratch.jslack.api.methods.response.mpim.MpimRepliesResponse;
import com.github.seratch.jslack.api.methods.response.oauth.OAuthAccessResponse;
import com.github.seratch.jslack.api.methods.response.oauth.OAuthTokenResponse;
import com.github.seratch.jslack.api.methods.response.pins.PinsAddResponse;
import com.github.seratch.jslack.api.methods.response.pins.PinsListResponse;
import com.github.seratch.jslack.api.methods.response.pins.PinsRemoveResponse;
import com.github.seratch.jslack.api.methods.response.reactions.ReactionsAddResponse;
import com.github.seratch.jslack.api.methods.response.reactions.ReactionsGetResponse;
import com.github.seratch.jslack.api.methods.response.reactions.ReactionsListResponse;
import com.github.seratch.jslack.api.methods.response.reactions.ReactionsRemoveResponse;
import com.github.seratch.jslack.api.methods.response.reminders.RemindersAddResponse;
import com.github.seratch.jslack.api.methods.response.reminders.RemindersCompleteResponse;
import com.github.seratch.jslack.api.methods.response.reminders.RemindersDeleteResponse;
import com.github.seratch.jslack.api.methods.response.reminders.RemindersInfoResponse;
import com.github.seratch.jslack.api.methods.response.reminders.RemindersListResponse;
import com.github.seratch.jslack.api.methods.response.rtm.RTMConnectResponse;
import com.github.seratch.jslack.api.methods.response.rtm.RTMStartResponse;
import com.github.seratch.jslack.api.methods.response.search.SearchAllResponse;
import com.github.seratch.jslack.api.methods.response.search.SearchFilesResponse;
import com.github.seratch.jslack.api.methods.response.search.SearchMessagesResponse;
import com.github.seratch.jslack.api.methods.response.stars.StarsAddResponse;
import com.github.seratch.jslack.api.methods.response.stars.StarsListResponse;
import com.github.seratch.jslack.api.methods.response.stars.StarsRemoveResponse;
import com.github.seratch.jslack.api.methods.response.team.TeamAccessLogsResponse;
import com.github.seratch.jslack.api.methods.response.team.TeamBillableInfoResponse;
import com.github.seratch.jslack.api.methods.response.team.TeamInfoResponse;
import com.github.seratch.jslack.api.methods.response.team.TeamIntegrationLogsResponse;
import com.github.seratch.jslack.api.methods.response.team.profile.TeamProfileGetResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.UsergroupsCreateResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.UsergroupsDisableResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.UsergroupsEnableResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.UsergroupsListResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.UsergroupsUpdateResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.users.UsergroupUsersListResponse;
import com.github.seratch.jslack.api.methods.response.usergroups.users.UsergroupUsersUpdateResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersConversationsResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersDeletePhotoResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersGetPresenceResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersIdentityResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersInfoResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersListResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersSetActiveResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersSetPhotoResponse;
import com.github.seratch.jslack.api.methods.response.users.UsersSetPresenceResponse;
import com.github.seratch.jslack.api.methods.response.users.profile.UsersProfileGetResponse;
import com.github.seratch.jslack.api.methods.response.users.profile.UsersProfileSetResponse;
import com.github.seratch.jslack.common.http.SlackHttpClient;

import java.io.IOException;

// delegates all invocations to methodsClient
class MethodsClientWrapper extends MethodsClientImpl {
  private final MethodsClient methodsClient;

  MethodsClientWrapper(SlackHttpClient slackHttpClient, MethodsClient methodsClient) {
    super(slackHttpClient);
    this.methodsClient = methodsClient;
  }

  @Override
  public void setEndpointUrlPrefix(String endpointUrlPrefix) {
    methodsClient.setEndpointUrlPrefix(endpointUrlPrefix);
  }

  @Override
  public ApiTestResponse apiTest(ApiTestRequest req) throws IOException, SlackApiException {
    return methodsClient.apiTest(req);
  }

  @Override
  public AppsPermissionsInfoResponse appsPermissionsInfo(AppsPermissionsInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.appsPermissionsInfo(req);
  }

  @Override
  public AppsPermissionsRequestResponse appsPermissionsRequest(AppsPermissionsRequestRequest req) throws IOException, SlackApiException {
    return methodsClient.appsPermissionsRequest(req);
  }

  @Override
  public AuthRevokeResponse authRevoke(AuthRevokeRequest req) throws IOException, SlackApiException {
    return methodsClient.authRevoke(req);
  }

  @Override
  public AuthTestResponse authTest(AuthTestRequest req) throws IOException, SlackApiException {
    return methodsClient.authTest(req);
  }

  @Override
  public BotsInfoResponse botsInfo(BotsInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.botsInfo(req);
  }

  @Override
  public ChannelsArchiveResponse channelsArchive(ChannelsArchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsArchive(req);
  }

  @Override
  public ChannelsCreateResponse channelsCreate(ChannelsCreateRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsCreate(req);
  }

  @Override
  public ChannelsHistoryResponse channelsHistory(ChannelsHistoryRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsHistory(req);
  }

  @Override
  public ChannelsRepliesResponse channelsReplies(ChannelsRepliesRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsReplies(req);
  }

  @Override
  public ChannelsInfoResponse channelsInfo(ChannelsInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsInfo(req);
  }

  @Override
  public ChannelsListResponse channelsList(ChannelsListRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsList(req);
  }

  @Override
  public ChannelsInviteResponse channelsInvite(ChannelsInviteRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsInvite(req);
  }

  @Override
  public ChannelsJoinResponse channelsJoin(ChannelsJoinRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsJoin(req);
  }

  @Override
  public ChannelsKickResponse channelsKick(ChannelsKickRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsKick(req);
  }

  @Override
  public ChannelsLeaveResponse channelsLeave(ChannelsLeaveRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsLeave(req);
  }

  @Override
  public ChannelsMarkResponse channelsMark(ChannelsMarkRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsMark(req);
  }

  @Override
  public ChannelsRenameResponse channelsRename(ChannelsRenameRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsRename(req);
  }

  @Override
  public ChannelsSetPurposeResponse channelsSetPurpose(ChannelsSetPurposeRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsSetPurpose(req);
  }

  @Override
  public ChannelsSetTopicResponse channelsSetTopic(ChannelsSetTopicRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsSetTopic(req);
  }

  @Override
  public ChannelsUnarchiveResponse channelsUnarchive(ChannelsUnarchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.channelsUnarchive(req);
  }

  @Override
  public ChatGetPermalinkResponse chatGetPermalink(ChatGetPermalinkRequest req) throws IOException, SlackApiException {
    return methodsClient.chatGetPermalink(req);
  }

  @Override
  public ChatDeleteResponse chatDelete(ChatDeleteRequest req) throws IOException, SlackApiException {
    return methodsClient.chatDelete(req);
  }

  @Override
  public ChatMeMessageResponse chatMeMessage(ChatMeMessageRequest req) throws IOException, SlackApiException {
    return methodsClient.chatMeMessage(req);
  }

  @Override
  public ChatPostEphemeralResponse chatPostEphemeral(ChatPostEphemeralRequest req) throws IOException, SlackApiException {
    return methodsClient.chatPostEphemeral(req);
  }

  @Override
  public ChatPostMessageResponse chatPostMessage(ChatPostMessageRequest req) throws IOException, SlackApiException {
    return methodsClient.chatPostMessage(req);
  }

  @Override
  public ChatUpdateResponse chatUpdate(ChatUpdateRequest req) throws IOException, SlackApiException {
    return methodsClient.chatUpdate(req);
  }

  @Override
  public ChatUnfurlResponse chatUnfurl(ChatUnfurlRequest req) throws IOException, SlackApiException {
    return methodsClient.chatUnfurl(req);
  }

  @Override
  public ConversationsArchiveResponse conversationsArchive(ConversationsArchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsArchive(req);
  }

  @Override
  public ConversationsCloseResponse conversationsClose(ConversationsCloseRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsClose(req);
  }

  @Override
  public ConversationsCreateResponse conversationsCreate(ConversationsCreateRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsCreate(req);
  }

  @Override
  public ConversationsHistoryResponse conversationsHistory(ConversationsHistoryRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsHistory(req);
  }

  @Override
  public ConversationsInfoResponse conversationsInfo(ConversationsInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsInfo(req);
  }

  @Override
  public ConversationsInviteResponse conversationsInvite(ConversationsInviteRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsInvite(req);
  }

  @Override
  public ConversationsJoinResponse conversationsJoin(ConversationsJoinRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsJoin(req);
  }

  @Override
  public ConversationsKickResponse conversationsKick(ConversationsKickRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsKick(req);
  }

  @Override
  public ConversationsLeaveResponse conversationsLeave(ConversationsLeaveRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsLeave(req);
  }

  @Override
  public ConversationsListResponse conversationsList(ConversationsListRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsList(req);
  }

  @Override
  public ConversationsMembersResponse conversationsMembers(ConversationsMembersRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsMembers(req);
  }

  @Override
  public ConversationsOpenResponse conversationsOpen(ConversationsOpenRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsOpen(req);
  }

  @Override
  public ConversationsRenameResponse conversationsRename(ConversationsRenameRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsRename(req);
  }

  @Override
  public ConversationsRepliesResponse conversationsReplies(ConversationsRepliesRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsReplies(req);
  }

  @Override
  public ConversationsSetPurposeResponse conversationsSetPurpose(ConversationsSetPurposeRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsSetPurpose(req);
  }

  @Override
  public ConversationsSetTopicResponse conversationsSetTopic(ConversationsSetTopicRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsSetTopic(req);
  }

  @Override
  public ConversationsUnarchiveResponse conversationsUnarchive(ConversationsUnarchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.conversationsUnarchive(req);
  }

  @Override
  public DialogOpenResponse dialogOpen(DialogOpenRequest req) throws IOException, SlackApiException {
    return methodsClient.dialogOpen(req);
  }

  @Override
  public DndEndDndResponse dndEndDnd(DndEndDndRequest req) throws IOException, SlackApiException {
    return methodsClient.dndEndDnd(req);
  }

  @Override
  public DndEndSnoozeResponse dndEndSnooze(DndEndSnoozeRequest req) throws IOException, SlackApiException {
    return methodsClient.dndEndSnooze(req);
  }

  @Override
  public DndInfoResponse dndInfo(DndInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.dndInfo(req);
  }

  @Override
  public DndSetSnoozeResponse dndSetSnooze(DndSetSnoozeRequest req) throws IOException, SlackApiException {
    return methodsClient.dndSetSnooze(req);
  }

  @Override
  public DndTeamInfoResponse dndTeamInfo(DndTeamInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.dndTeamInfo(req);
  }

  @Override
  public EmojiListResponse emojiList(EmojiListRequest req) throws IOException, SlackApiException {
    return methodsClient.emojiList(req);
  }

  @Override
  public FilesDeleteResponse filesDelete(FilesDeleteRequest req) throws IOException, SlackApiException {
    return methodsClient.filesDelete(req);
  }

  @Override
  public FilesInfoResponse filesInfo(FilesInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.filesInfo(req);
  }

  @Override
  public FilesListResponse filesList(FilesListRequest req) throws IOException, SlackApiException {
    return methodsClient.filesList(req);
  }

  @Override
  public FilesRevokePublicURLResponse filesRevokePublicURL(FilesRevokePublicURLRequest req) throws IOException, SlackApiException {
    return methodsClient.filesRevokePublicURL(req);
  }

  @Override
  public FilesSharedPublicURLResponse filesSharedPublicURL(FilesSharedPublicURLRequest req) throws IOException, SlackApiException {
    return methodsClient.filesSharedPublicURL(req);
  }

  @Override
  public FilesUploadResponse filesUpload(FilesUploadRequest req) throws IOException, SlackApiException {
    return methodsClient.filesUpload(req);
  }

  @Override
  public FilesCommentsAddResponse filesCommentsAdd(FilesCommentsAddRequest req) throws IOException, SlackApiException {
    return methodsClient.filesCommentsAdd(req);
  }

  @Override
  public FilesCommentsDeleteResponse filesCommentsDelete(FilesCommentsDeleteRequest req) throws IOException, SlackApiException {
    return methodsClient.filesCommentsDelete(req);
  }

  @Override
  public FilesCommentsEditResponse filesCommentEdit(FilesCommentsEditRequest req) throws IOException, SlackApiException {
    return methodsClient.filesCommentEdit(req);
  }

  @Override
  public GroupsArchiveResponse groupsArchive(GroupsArchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsArchive(req);
  }

  @Override
  public GroupsCloseResponse groupsClose(GroupsCloseRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsClose(req);
  }

  @Override
  public GroupsCreateChildResponse groupsCreateChild(GroupsCreateChildRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsCreateChild(req);
  }

  @Override
  public GroupsCreateResponse groupsCreate(GroupsCreateRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsCreate(req);
  }

  @Override
  public GroupsHistoryResponse groupsHistory(GroupsHistoryRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsHistory(req);
  }

  @Override
  public GroupsInfoResponse groupsInfo(GroupsInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsInfo(req);
  }

  @Override
  public GroupsInviteResponse groupsInvite(GroupsInviteRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsInvite(req);
  }

  @Override
  public GroupsKickResponse groupsKick(GroupsKickRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsKick(req);
  }

  @Override
  public GroupsLeaveResponse groupsLeave(GroupsLeaveRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsLeave(req);
  }

  @Override
  public GroupsListResponse groupsList(GroupsListRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsList(req);
  }

  @Override
  public GroupsMarkResponse groupsMark(GroupsMarkRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsMark(req);
  }

  @Override
  public GroupsOpenResponse groupsOpen(GroupsOpenRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsOpen(req);
  }

  @Override
  public GroupsRenameResponse groupsRename(GroupsRenameRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsRename(req);
  }

  @Override
  public GroupsSetPurposeResponse groupsSetPurpose(GroupsSetPurposeRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsSetPurpose(req);
  }

  @Override
  public GroupsSetTopicResponse groupsSetTopic(GroupsSetTopicRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsSetTopic(req);
  }

  @Override
  public GroupsUnarchiveResponse groupsUnarchive(GroupsUnarchiveRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsUnarchive(req);
  }

  @Override
  public GroupsRepliesResponse groupsReplies(GroupsRepliesRequest req) throws IOException, SlackApiException {
    return methodsClient.groupsReplies(req);
  }

  @Override
  public ImCloseResponse imClose(ImCloseRequest req) throws IOException, SlackApiException {
    return methodsClient.imClose(req);
  }

  @Override
  public ImHistoryResponse imHistory(ImHistoryRequest req) throws IOException, SlackApiException {
    return methodsClient.imHistory(req);
  }

  @Override
  public ImListResponse imList(ImListRequest req) throws IOException, SlackApiException {
    return methodsClient.imList(req);
  }

  @Override
  public ImMarkResponse imMark(ImMarkRequest req) throws IOException, SlackApiException {
    return methodsClient.imMark(req);
  }

  @Override
  public ImOpenResponse imOpen(ImOpenRequest req) throws IOException, SlackApiException {
    return methodsClient.imOpen(req);
  }

  @Override
  public ImRepliesResponse imReplies(ImRepliesRequest req) throws IOException, SlackApiException {
    return methodsClient.imReplies(req);
  }

  @Override
  public MigrationExchangeResponse migrationExchange(MigrationExchangeRequest req) throws IOException, SlackApiException {
    return methodsClient.migrationExchange(req);
  }

  @Override
  public MpimCloseResponse mpimClose(MpimCloseRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimClose(req);
  }

  @Override
  public MpimHistoryResponse mpimHistory(MpimHistoryRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimHistory(req);
  }

  @Override
  public MpimListResponse mpimList(MpimListRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimList(req);
  }

  @Override
  public MpimRepliesResponse mpimReplies(MpimRepliesRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimReplies(req);
  }

  @Override
  public MpimMarkResponse mpimMark(MpimMarkRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimMark(req);
  }

  @Override
  public MpimOpenResponse mpimOpen(MpimOpenRequest req) throws IOException, SlackApiException {
    return methodsClient.mpimOpen(req);
  }

  @Override
  public OAuthAccessResponse oauthAccess(OAuthAccessRequest req) throws IOException, SlackApiException {
    return methodsClient.oauthAccess(req);
  }

  @Override
  public OAuthTokenResponse oauthToken(OAuthTokenRequest req) throws IOException, SlackApiException {
    return methodsClient.oauthToken(req);
  }

  @Override
  public PinsAddResponse pinsAdd(PinsAddRequest req) throws IOException, SlackApiException {
    return methodsClient.pinsAdd(req);
  }

  @Override
  public PinsListResponse pinsList(PinsListRequest req) throws IOException, SlackApiException {
    return methodsClient.pinsList(req);
  }

  @Override
  public PinsRemoveResponse pinsRemove(PinsRemoveRequest req) throws IOException, SlackApiException {
    return methodsClient.pinsRemove(req);
  }

  @Override
  public ReactionsAddResponse reactionsAdd(ReactionsAddRequest req) throws IOException, SlackApiException {
    return methodsClient.reactionsAdd(req);
  }

  @Override
  public ReactionsGetResponse reactionsGet(ReactionsGetRequest req) throws IOException, SlackApiException {
    return methodsClient.reactionsGet(req);
  }

  @Override
  public ReactionsListResponse reactionsList(ReactionsListRequest req) throws IOException, SlackApiException {
    return methodsClient.reactionsList(req);
  }

  @Override
  public ReactionsRemoveResponse reactionsRemove(ReactionsRemoveRequest req) throws IOException, SlackApiException {
    return methodsClient.reactionsRemove(req);
  }

  @Override
  public RemindersAddResponse remindersAdd(RemindersAddRequest req) throws IOException, SlackApiException {
    return methodsClient.remindersAdd(req);
  }

  @Override
  public RemindersCompleteResponse remindersComplete(RemindersCompleteRequest req) throws IOException, SlackApiException {
    return methodsClient.remindersComplete(req);
  }

  @Override
  public RemindersDeleteResponse remindersDelete(RemindersDeleteRequest req) throws IOException, SlackApiException {
    return methodsClient.remindersDelete(req);
  }

  @Override
  public RemindersInfoResponse remindersInfo(RemindersInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.remindersInfo(req);
  }

  @Override
  public RemindersListResponse remindersList(RemindersListRequest req) throws IOException, SlackApiException {
    return methodsClient.remindersList(req);
  }

  @Override
  public RTMConnectResponse rtmConnect(RTMConnectRequest req) throws IOException, SlackApiException {
    return methodsClient.rtmConnect(req);
  }

  @Override
  public RTMStartResponse rtmStart(RTMStartRequest req) throws IOException, SlackApiException {
    return methodsClient.rtmStart(req);
  }

  @Override
  public SearchAllResponse searchAll(SearchAllRequest req) throws IOException, SlackApiException {
    return methodsClient.searchAll(req);
  }

  @Override
  public SearchMessagesResponse searchMessages(SearchMessagesRequest req) throws IOException, SlackApiException {
    return methodsClient.searchMessages(req);
  }

  @Override
  public SearchFilesResponse searchFiles(SearchFilesRequest req) throws IOException, SlackApiException {
    return methodsClient.searchFiles(req);
  }

  @Override
  public StarsAddResponse starsAdd(StarsAddRequest req) throws IOException, SlackApiException {
    return methodsClient.starsAdd(req);
  }

  @Override
  public StarsListResponse starsList(StarsListRequest req) throws IOException, SlackApiException {
    return methodsClient.starsList(req);
  }

  @Override
  public StarsRemoveResponse starsRemove(StarsRemoveRequest req) throws IOException, SlackApiException {
    return methodsClient.starsRemove(req);
  }

  @Override
  public TeamAccessLogsResponse teamAccessLogs(TeamAccessLogsRequest req) throws IOException, SlackApiException {
    return methodsClient.teamAccessLogs(req);
  }

  @Override
  public TeamBillableInfoResponse teamBillableInfo(TeamBillableInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.teamBillableInfo(req);
  }

  @Override
  public TeamInfoResponse teamInfo(TeamInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.teamInfo(req);
  }

  @Override
  public TeamIntegrationLogsResponse teamIntegrationLogs(TeamIntegrationLogsRequest req) throws IOException, SlackApiException {
    return methodsClient.teamIntegrationLogs(req);
  }

  @Override
  public TeamProfileGetResponse teamProfileGet(TeamProfileGetRequest req) throws IOException, SlackApiException {
    return methodsClient.teamProfileGet(req);
  }

  @Override
  public UsergroupsCreateResponse usergroupsCreate(UsergroupsCreateRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupsCreate(req);
  }

  @Override
  public UsergroupsDisableResponse usergroupsDisable(UsergroupsDisableRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupsDisable(req);
  }

  @Override
  public UsergroupsEnableResponse usergroupsEnable(UsergroupsEnableRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupsEnable(req);
  }

  @Override
  public UsergroupsListResponse usergroupsList(UsergroupsListRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupsList(req);
  }

  @Override
  public UsergroupsUpdateResponse usergroupsUpdate(UsergroupsUpdateRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupsUpdate(req);
  }

  @Override
  public UsergroupUsersListResponse usergroupUsersList(UsergroupUsersListRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupUsersList(req);
  }

  @Override
  public UsergroupUsersUpdateResponse usergroupUsersUpdate(UsergroupUsersUpdateRequest req) throws IOException, SlackApiException {
    return methodsClient.usergroupUsersUpdate(req);
  }

  @Override
  public UsersConversationsResponse usersConversations(UsersConversationsRequest req) throws IOException, SlackApiException {
    return methodsClient.usersConversations(req);
  }

  @Override
  public UsersDeletePhotoResponse usersDeletePhoto(UsersDeletePhotoRequest req) throws IOException, SlackApiException {
    return methodsClient.usersDeletePhoto(req);
  }

  @Override
  public UsersGetPresenceResponse usersGetPresence(UsersGetPresenceRequest req) throws IOException, SlackApiException {
    return methodsClient.usersGetPresence(req);
  }

  @Override
  public UsersIdentityResponse usersIdentity(UsersIdentityRequest req) throws IOException, SlackApiException {
    return methodsClient.usersIdentity(req);
  }

  @Override
  public UsersInfoResponse usersInfo(UsersInfoRequest req) throws IOException, SlackApiException {
    return methodsClient.usersInfo(req);
  }

  @Override
  public UsersListResponse usersList(UsersListRequest req) throws IOException, SlackApiException {
    return methodsClient.usersList(req);
  }

  @Override
  public UsersLookupByEmailResponse usersLookupByEmail(UsersLookupByEmailRequest req) throws IOException, SlackApiException {
    return methodsClient.usersLookupByEmail(req);
  }

  @Override
  public UsersSetActiveResponse usersSetActive(UsersSetActiveRequest req) throws IOException, SlackApiException {
    return methodsClient.usersSetActive(req);
  }

  @Override
  public UsersSetPhotoResponse usersSetPhoto(UsersSetPhotoRequest req) throws IOException, SlackApiException {
    return methodsClient.usersSetPhoto(req);
  }

  @Override
  public UsersSetPresenceResponse usersSetPresence(UsersSetPresenceRequest req) throws IOException, SlackApiException {
    return methodsClient.usersSetPresence(req);
  }

  @Override
  public UsersProfileGetResponse usersProfileGet(UsersProfileGetRequest req) throws IOException, SlackApiException {
    return methodsClient.usersProfileGet(req);
  }

  @Override
  public UsersProfileSetResponse usersProfileSet(UsersProfileSetRequest req) throws IOException, SlackApiException {
    return methodsClient.usersProfileSet(req);
  }
}
