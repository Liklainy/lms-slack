package ru.hh.school.lms.slack.event;

import java.util.HashMap;
import java.util.Map;

public class CallbackStorage {
  private Map<String, EventConsumer<DialogSubmissionEvent>> storageDialogSubmission;
  private Map<String, EventConsumer<MessageInteractionEvent>> storageInteractiveMessage;

  public CallbackStorage() {
    storageDialogSubmission = new HashMap<>();
    storageInteractiveMessage = new HashMap<>();
  }

  public EventConsumer<DialogSubmissionEvent> getDialogSubmissionConsumer(String callbackId) {
    return storageDialogSubmission.get(callbackId);
  }

  public void putDialogSubmissionConsumer(String callbackId, EventConsumer<DialogSubmissionEvent> consumer) {
    storageDialogSubmission.put(callbackId, consumer);
  }

  public boolean hasDialogSubmissionConsumer(String callbackId) {
    return storageDialogSubmission.containsKey(callbackId);
  }

  public void removeDialogSubmissionConsumer(String callbackId) {
    storageDialogSubmission.remove(callbackId);
  }

  public EventConsumer<MessageInteractionEvent> getInteractiveMessageConsumer(String callbackId) {
    return storageInteractiveMessage.get(callbackId);
  }

  public void putInteractiveMessageConsumer(String callbackId, EventConsumer<MessageInteractionEvent> consumer) {
    storageInteractiveMessage.put(callbackId, consumer);
  }

  public boolean hasInteractiveMessageConsumer(String callbackId) {
    return storageInteractiveMessage.containsKey(callbackId);
  }

  public void removeInteractiveMessageConsumer(String callbackId) {
    storageInteractiveMessage.remove(callbackId);
  }
}
