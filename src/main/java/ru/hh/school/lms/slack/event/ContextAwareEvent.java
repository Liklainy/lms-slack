package ru.hh.school.lms.slack.event;

import com.github.seratch.jslack.api.model.Channel;
import com.github.seratch.jslack.api.model.Team;
import com.github.seratch.jslack.api.model.User;

public class ContextAwareEvent extends Event {
  private Team team;
  private Channel channel;
  private User user;

  private final Context context = new Context();

  public Context getContext() {
    return context;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  public Channel getChannel() {
    return channel;
  }

  public void setChannel(Channel channel) {
    this.channel = channel;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
