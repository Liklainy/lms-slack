package ru.hh.school.lms.slack.slashcommand;

public class SlashCommandHandlingException extends Exception {
  public SlashCommandHandlingException(Exception e) {
    super(e);
  }
}
