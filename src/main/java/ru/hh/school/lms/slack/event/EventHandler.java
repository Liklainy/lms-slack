package ru.hh.school.lms.slack.event;

import java.util.function.Predicate;

public interface EventHandler<T extends Event> extends Predicate<T> {

  void onEvent(T event) throws EventHandlingException;
}
