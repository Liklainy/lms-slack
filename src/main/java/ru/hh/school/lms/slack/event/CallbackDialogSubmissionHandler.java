package ru.hh.school.lms.slack.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CallbackDialogSubmissionHandler implements DialogSubmissionHandler {

  private final CallbackStorage callbackStorage;

  @Autowired
  public CallbackDialogSubmissionHandler(CallbackStorage callbackStorage) {
    this.callbackStorage = callbackStorage;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    callbackStorage.getDialogSubmissionConsumer(event.getCallbackId()).accept(event);
  }

  @Override
  public boolean test(DialogSubmissionEvent dialogSubmissionEvent) {
    return callbackStorage.hasDialogSubmissionConsumer(dialogSubmissionEvent.getCallbackId());
  }
}
