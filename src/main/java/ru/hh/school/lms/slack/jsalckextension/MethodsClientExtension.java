package ru.hh.school.lms.slack.jsalckextension;

import com.github.seratch.jslack.api.methods.Methods;
import com.github.seratch.jslack.api.methods.MethodsClient;
import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.methods.request.dialog.DialogOpenRequest;
import com.github.seratch.jslack.api.methods.response.dialog.DialogOpenResponse;
import com.github.seratch.jslack.common.http.SlackHttpClient;
import com.github.seratch.jslack.common.json.GsonFactory;
import okhttp3.FormBody;

import java.io.IOException;

public final class MethodsClientExtension extends MethodsClientWrapper {

  private static final String TRIGGER_ID_FIELD = "trigger_id";
  private static final String DIALOG_FIELD = "dialog";

  public MethodsClientExtension(SlackHttpClient slackHttpClient, MethodsClient methodsClient) {
    super(slackHttpClient, methodsClient);
  }

  public DialogOpenResponse dialogOpen(StatefulDialogOpenRequest dialogOpenRequest)
    throws IOException, SlackApiException {
    DialogOpenRequest req = dialogOpenRequest.getDialogOpenRequest();
    FormBody.Builder form = new FormBody.Builder();
    String triggerIdValue = req.getTriggerId();
    if (triggerIdValue != null) {
      form.add(TRIGGER_ID_FIELD, triggerIdValue);
    }
    if (req.getDialog() != null) {
      String json = GsonFactory.createSnakeCase().toJson(dialogOpenRequest.getDialog());
      form.add(DIALOG_FIELD, json);
    }
    return doPostFormWithToken(form, Methods.DIALOG_OPEN, req.getToken(), DialogOpenResponse.class);
  }
}
