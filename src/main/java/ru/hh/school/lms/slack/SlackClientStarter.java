package ru.hh.school.lms.slack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.websocket.DeploymentException;
import java.io.IOException;


/**
 * Application listener that launches Slack RTM API client
 * on application startup.
 *
 * @see <a href="https://api.slack.com/rtm">https://api.slack.com/rtm</a>
 */
@Component
final class SlackClientStarter implements ApplicationListener<ContextRefreshedEvent> {

  private static final Logger LOGGER = LoggerFactory.getLogger(SlackClientStarter.class);
  private boolean started = false;

  private final AnnotationConfigWebApplicationContext appContext;
  private final EventRtmClient jSlackClient;

  @Autowired
  SlackClientStarter(AnnotationConfigWebApplicationContext appContext, EventRtmClient jSlackClient) {
    this.appContext = appContext;
    this.jSlackClient = jSlackClient;
  }

  /**
   * Establishes Slack RTM API connection on first application context refresh
   */
  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    final ApplicationContext applicationContext = event.getApplicationContext();
    if (appContext.equals(applicationContext) && !started) {
      try {
        jSlackClient.connect();
        started = true;
      } catch (IOException | SlackRtmConnectionException | DeploymentException exc) {
        LOGGER.error(exc.getMessage());
        appContext.close();
        // TODO throw new StartupException() ?
      }
    }
  }
}
