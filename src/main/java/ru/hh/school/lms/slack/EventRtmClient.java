package ru.hh.school.lms.slack;

import com.github.seratch.jslack.api.methods.MethodsClient;
import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.methods.request.rtm.RTMConnectRequest;
import com.github.seratch.jslack.api.methods.response.rtm.RTMConnectResponse;
import com.github.seratch.jslack.api.model.Channel;
import com.github.seratch.jslack.api.model.Team;
import com.github.seratch.jslack.api.model.User;
import com.github.seratch.jslack.api.rtm.RTMClient;
import com.github.seratch.jslack.api.rtm.RTMMessageHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.event.Event;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@Component
class EventRtmClient implements AutoCloseable {

  private static final Logger LOGGER = LoggerFactory.getLogger(EventRtmClient.class);
  private static final JsonParser PARSER = new JsonParser();

  private final MethodsClient methods;
  private final PersonService personService;
  private final String slackBotToken;
  private final List<MessagePostedHandler> messageHandlers;
  private final Gson gson;
  private RTMClient rtm;
  private String selfId;

  @Autowired
  EventRtmClient(MethodsClient methods, FileSettings settings,
                 List<MessagePostedHandler> messageHandlers, Gson gson,
                 PersonService personService) {
    this.methods = methods;
    this.personService = personService;
    this.slackBotToken = settings.getString("ru.hh.school.lms.slack.botToken");
    this.messageHandlers = messageHandlers;
    this.gson = gson;
  }

  void connect() throws IOException, SlackRtmConnectionException, DeploymentException {
    LOGGER.debug("Establishing RTM Slack API connection.");
    try {
      RTMConnectResponse response = methods.rtmConnect(RTMConnectRequest.builder().token(slackBotToken).build());
      LOGGER.debug("RTM connect response: error={}, warning={}, ok={}", response.getError(), response.getWarning(), response.isOk());
      if (response.isOk()) {
        this.selfId = requireNonNull(response.getSelf().getId());
        rtm = new RTMClient(response.getUrl());
      } else {
        throw new SlackRtmConnectionException("Failed to connect to RTM Slack API. Response error: " + response.getError());
      }
    } catch (NullPointerException exc) {
      throw new SlackRtmConnectionException("Failed to get self id.", exc);
    } catch (SlackApiException | URISyntaxException exc) {
      throw new SlackRtmConnectionException("Failed to connect to RTM Slack API.", exc);
    }
    rtm.addMessageHandler(message -> LOGGER.debug("RTM Slack message: {}", message));
    rtm.addErrorHandler(reason -> LOGGER.error("Error in RTM Slack Client: {}", reason));
    rtm.addCloseHandler(reason -> LOGGER.info("Closing RTM Slack Client: {}", reason));
    rtm.addMessageHandler(new CommonEventRTMMessageHandler());
    rtm.connect();
    LOGGER.debug("RTM Slack connection  has been successfully established.");
  }

  private final class CommonEventRTMMessageHandler implements RTMMessageHandler {

    @Override
    public void handle(String messageJson) {
      Event event = gson.fromJson(messageJson, Event.class);
      switch (event.getType()) {
        case MESSAGE:
          MessagePostedEvent messageEvent = createEvent(messageJson);
          for (MessagePostedHandler handler : messageHandlers) {
            try {
              if (!selfId.equals(messageEvent.getUser().getId()) && handler.test(messageEvent)) {
                handler.onEvent(messageEvent);
              }
            } catch (EventHandlingException exc) {
              EventRtmClient.LOGGER.error("Failed to handle message: " + messageEvent, exc);
            }
          }
          break;
        default:
          EventRtmClient.LOGGER.trace("Unknown event type: {} JSON: {}", event.getType(), messageJson);
      }
    }
  }

  @Override
  public void close() throws IOException {
    if (rtm != null) {
      rtm.close();
      LOGGER.debug("RTM Slack connection has been successfully closed.");
    } else {
      LOGGER.debug("Can't close RTM Slack connection as it was not opened.");
    }
  }

  private static String getAsStringAndRemove(String fieldName, JsonObject jsonObject) {
    JsonElement element = jsonObject.remove(fieldName);
    if (element == null) {
      return null;
    }
    return element.getAsString();
  }

  private MessagePostedEvent createEvent(String messageJson) {
    JsonObject jsonObject = PARSER.parse(messageJson).getAsJsonObject();
    User user = new User();
    user.setId(getAsStringAndRemove("user", jsonObject));
    Team team = new Team();
    team.setId(getAsStringAndRemove("team", jsonObject));
    Channel channel = new Channel();
    channel.setId(getAsStringAndRemove("channel", jsonObject));
    MessagePostedEvent event = gson.fromJson(jsonObject, MessagePostedEvent.class);
    event.setUser(user);
    event.setTeam(team);
    event.setChannel(channel);
    Optional<Person> person = personService.findBySlackAccountId(user.getId());
    person.ifPresent(p -> event.getContext().setSender(p));
    return event;
  }
}
