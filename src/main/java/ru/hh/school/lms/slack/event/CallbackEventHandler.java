package ru.hh.school.lms.slack.event;

public interface CallbackEventHandler<T extends CallbackEvent> extends EventHandler<T> {
  @Override
  default boolean test(T t) {
    return t.getCallbackId().equals(this.getClass().getCanonicalName());
  }
}
