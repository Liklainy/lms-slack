package ru.hh.school.lms.slack;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.common.http.SlackHttpClient;
import com.github.seratch.jslack.shortcut.Shortcut;
import com.github.seratch.jslack.shortcut.model.ApiToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.school.lms.slack.jsalckextension.MethodsClientExtension;

@Configuration
public class SlackConfig {

  @Bean
  public Slack slack() {
    return Slack.getInstance(slackHttpClient());
  }

  @Bean
  SlackHttpClient slackHttpClient() {
    return new SlackHttpClient();
  }

  @Bean
  public MethodsClientExtension methodsClient() {
    return new MethodsClientExtension(slackHttpClient(), slack().methods());
  }

  @Bean
  public Shortcut shortcut(FileSettings settings) {
    String slackBotToken = settings.getString("ru.hh.school.lms.slack.botToken");
    return slack().shortcut(ApiToken.of(slackBotToken));
  }
}
