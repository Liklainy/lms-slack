package ru.hh.school.lms.slack.jsalckextension;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogElement;

import java.util.List;

final class StatefulDialog {

  private final String state;
  private String title;
  private String callbackId;
  private List<DialogElement> elements;
  private String submitLabel;

  StatefulDialog(Dialog dialog, String state) {
    this.title = dialog.getTitle();
    this.callbackId = dialog.getCallbackId();
    this.elements = dialog.getElements();
    this.submitLabel = dialog.getSubmitLabel();
    this.state = state;
  }
}
