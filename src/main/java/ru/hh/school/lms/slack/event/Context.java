package ru.hh.school.lms.slack.event;

import ru.hh.school.lms.service.person.model.Person;

import java.util.Optional;

public final class Context {

  private Person sender;

  Context() {

  }

  public Optional<Person> getSender() {
    return Optional.ofNullable(sender);
  }

  public void setSender(Person sender) {
    this.sender = sender;
  }
}
