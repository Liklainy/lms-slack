package ru.hh.school.lms.slack.event;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Message;

import java.util.List;

public final class MessageInteractionEvent extends CallbackEvent {
  private List<Action> actions;
  private String actionTs;
  private String messageTs;
  private String attachmentId;
  private String token;
  private Boolean isAppUnfurl;
  private Message originalMessage;
  private String responseUrl;
  private String triggerId;
  private String name;
  private String value;

  public List<Action> getActions() {
    return actions;
  }

  public void setActions(List<Action> actions) {
    this.actions = actions;
  }

  public String getActionTs() {
    return actionTs;
  }

  public void setActionTs(String actionTs) {
    this.actionTs = actionTs;
  }

  public String getMessageTs() {
    return messageTs;
  }

  public void setMessageTs(String messageTs) {
    this.messageTs = messageTs;
  }

  public String getAttachmentId() {
    return attachmentId;
  }

  public void setAttachmentId(String attachmentId) {
    this.attachmentId = attachmentId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Boolean getAppUnfurl() {
    return isAppUnfurl;
  }

  public void setAppUnfurl(Boolean appUnfurl) {
    isAppUnfurl = appUnfurl;
  }

  public Message getOriginalMessage() {
    return originalMessage;
  }

  public void setOriginalMessage(Message originalMessage) {
    this.originalMessage = originalMessage;
  }

  public String getResponseUrl() {
    return responseUrl;
  }

  public void setResponseUrl(String responseUrl) {
    this.responseUrl = responseUrl;
  }

  public String getTriggerId() {
    return triggerId;
  }

  public void setTriggerId(String triggerId) {
    this.triggerId = triggerId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "MessageInteractionEvent{" +
      "actions=" + actions +
      ", callbackId='" + callbackId + '\'' +
      ", actionTs='" + actionTs + '\'' +
      ", messageTs='" + messageTs + '\'' +
      ", attachmentId='" + attachmentId + '\'' +
      ", token='" + token + '\'' +
      ", isAppUnfurl=" + isAppUnfurl +
      ", originalMessage=" + originalMessage +
      ", responseUrl='" + responseUrl + '\'' +
      ", triggerId='" + triggerId + '\'' +
      '}';
  }
}
