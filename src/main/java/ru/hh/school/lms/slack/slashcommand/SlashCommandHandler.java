package ru.hh.school.lms.slack.slashcommand;

import java.util.function.Predicate;

public interface SlashCommandHandler extends Predicate<SlashCommand> {

  void process(SlashCommand command) throws SlashCommandHandlingException;

}
