package ru.hh.school.lms.slack.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CallbackInteractiveMessageHandler implements MessageInteractionHandler {

  private final CallbackStorage callbackStorage;

  @Autowired
  public CallbackInteractiveMessageHandler(CallbackStorage callbackStorage) {
    this.callbackStorage = callbackStorage;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    callbackStorage.getInteractiveMessageConsumer(event.getCallbackId()).accept(event);
  }

  @Override
  public boolean test(MessageInteractionEvent messageInteractionEvent) {
    return callbackStorage.hasInteractiveMessageConsumer(messageInteractionEvent.getCallbackId());
  }
}
