package ru.hh.school.lms.slack;

final class SlackRtmConnectionException extends RuntimeException {

  SlackRtmConnectionException(String message) {
    super(message);
  }

  SlackRtmConnectionException(String message, Throwable cause) {
    super(message, cause);
  }
}
