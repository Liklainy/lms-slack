package ru.hh.school.lms.slack.event;

@FunctionalInterface
public interface EventConsumer<T extends Event> {
  void accept(T t) throws EventHandlingException;
}
