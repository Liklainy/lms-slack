package ru.hh.school.lms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.hh.nab.common.properties.FileSettings;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.List;

/**
 * Spring configurations that enables JPA repositories, transactions
 * and defines data source.
 * <p>
 * <i>Note:</i> add your package in entityManagerFactory bean description
 * to manage <code>@Entity</code> objects.
 */
@Configuration
@ComponentScan("ru.hh.school.lms")
@EnableTransactionManagement
@EnableJpaRepositories("ru.hh.school.lms.repository")
public class DataConfig {

  @Bean
  public DataSource dataSource(FileSettings settings) {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(settings.getString("ru.hh.school.lms.db.driver"));
    dataSource.setUrl(settings.getString("ru.hh.school.lms.db.url"));
    dataSource.setUsername(settings.getString("ru.hh.school.lms.db.username"));
    dataSource.setPassword(settings.getString("ru.hh.school.lms.db.password"));
    String schemaSql = settings.getString("ru.hh.school.lms.db.schema");
    String dataSql = settings.getString("ru.hh.school.lms.db.data");
    for (String sqlFile : List.of(schemaSql, dataSql)) {
      Resource sqlResource = new ClassPathResource(sqlFile);
      DatabasePopulator populator = new ResourceDatabasePopulator(sqlResource);
      DatabasePopulatorUtils.execute(populator, dataSource);
    }
    return dataSource;
  }

  @Bean
  public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
    return new JpaTransactionManager(emf);
  }

  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
    jpaVendorAdapter.setDatabase(Database.POSTGRESQL);
    jpaVendorAdapter.setShowSql(true);
    jpaVendorAdapter.setGenerateDdl(false);
    return jpaVendorAdapter;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(FileSettings settings) {
    LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
    factoryBean.setDataSource(dataSource(settings));
    factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
    factoryBean.setPackagesToScan(
        "ru.hh.school.lms.service.person",
        "ru.hh.school.lms.service.lectures",
        "ru.hh.school.lms.service.tasks",
        "ru.hh.school.lms.service.grading",
        "ru.hh.school.lms.service.notifications"
      // <add your package here>
    );
    return factoryBean;
  }
}
