package ru.hh.school.lms.webhook;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.event.ContextAwareEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.DynamicMenuHandler;
import ru.hh.school.lms.slack.event.EventHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;
import ru.hh.school.lms.slack.slashcommand.SlashCommand;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandler;
import ru.hh.school.lms.slack.slashcommand.SlashCommandHandlingException;
import ru.hh.school.lms.webhook.interactionexc.InteractionException;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Path("/")
public final class WebHookResource {


  private static final Logger LOGGER = LoggerFactory.getLogger(WebHookResource.class);

  private final List<SlashCommandHandler> commandProcessors;
  private final List<DialogSubmissionHandler> dialogSubmissionHandlers;
  private final List<MessageInteractionHandler> messageInteractionHandlers;
  private final List<DynamicMenuHandler> dynamicMenuHandlers;
  private final Gson gson;
  private final PersonService personService;

  @Autowired
  WebHookResource(List<SlashCommandHandler> commandProcessors,
                  List<DialogSubmissionHandler> dialogSubmissionListeners,
                  List<MessageInteractionHandler> messageInteractionHandlers,
                  List<DynamicMenuHandler> dynamicMenuHandlers,
                  Gson gson,
                  PersonService personService) {
    this.commandProcessors = commandProcessors;
    this.dialogSubmissionHandlers = dialogSubmissionListeners;
    this.messageInteractionHandlers = messageInteractionHandlers;
    this.dynamicMenuHandlers = dynamicMenuHandlers;
    this.gson = gson;
    this.personService = personService;
  }

  @POST
  @Path("/slash-command")
  public Response processSlashCommand(@BeanParam SlashCommand command) {
    try {
      if (command.getCommand() == null) {
        throw new SlashCommandHandlingException(new NullPointerException("Parameter 'command' must not be null"));
      }
      for (SlashCommandHandler processor : commandProcessors) {
        if (processor.test(command)) {
          processor.process(command);
          return Response.ok().build();
        }
      }
    } catch (SlashCommandHandlingException exc) {
      LOGGER.error("Error during command processing. Command:" + command.toString(), exc);
      return Response.serverError().build();
    }
    LOGGER.error("There is no handlers for the command:{}; {}", command.getCommand(), command.toString());
    return Response.status(404).build();
  }

  @POST
  @Path("/interactive-component")
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  public Response processInteractiveComponent(@FormParam("payload") String payload) {
    ContextAwareEvent event = gson.fromJson(payload, ContextAwareEvent.class);
    try {
      switch (event.getType()) {
        case DIALOG_SUBMISSION:
          DialogSubmissionEvent dse = gson.fromJson(payload, DialogSubmissionEvent.class);
          callAllHandlers(dialogSubmissionHandlers, dse);
          break;
        case INTERACTIVE_MESSAGE:
          MessageInteractionEvent mie = gson.fromJson(payload, MessageInteractionEvent.class);
          callAllHandlers(messageInteractionHandlers, mie);
          break;
        default:
          LOGGER.warn("Unknown event type:" + event.getType() + "\n" + event.toString());
      }
      return Response.ok().build();
    } catch (InteractionException error) {
      String errorToJson = gson.toJson(error.getError());
      return Response.ok(errorToJson).build();
    } catch (EventHandlingException exc) {
      LOGGER.error("Failed to handle component interaction: payload=" + payload, exc);
      return Response.serverError().build();
    }
  }

  private <T extends ContextAwareEvent> void callAllHandlers(List<? extends EventHandler<T>> handlers, T event)
    throws EventHandlingException {
    setSenderContext(event);
    for (EventHandler<T> handler : handlers) {
      if (handler.test(event)) {
        handler.onEvent(event);
      }
    }
  }

  private void setSenderContext(ContextAwareEvent event) {
    String slackAccountId = event.getUser().getId();
    Optional<Person> person = personService.findBySlackAccountId(slackAccountId);
    person.ifPresent(p -> event.getContext().setSender(p));
  }
}
