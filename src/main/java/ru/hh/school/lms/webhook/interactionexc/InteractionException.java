package ru.hh.school.lms.webhook.interactionexc;

import ru.hh.school.lms.slack.event.Event;
import ru.hh.school.lms.slack.event.EventHandlingException;

public class InteractionException extends EventHandlingException {

  final private Error error;

  public InteractionException(Event event, Error error) {
    super(event);
    this.error = error;
  }

  public Error getError() {
    return error;
  }
}
