package ru.hh.school.lms.service.grading.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "submission")
public class Submission {

  @Id
  @Column(name = "submission_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long submissionId;

  @Column(name = "student_id")
  private Long studentId;
  @Column(name = "homework_id")
  private Long homeworkId;
  @Column(name = "github_url")
  private String githubUrl;
  private String description;
  private LocalDateTime created;
  private LocalDateTime updated;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "submission_id")
  private List<Review> reviews = new ArrayList<>();

  public Long getSubmissionId() {
    return submissionId;
  }

  public void setSubmissionId(Long submissionId) {
    this.submissionId = submissionId;
  }

  public Long getStudentId() {
    return studentId;
  }

  public void setStudentId(Long studentId) {
    this.studentId = studentId;
  }

  public Long getHomeworkId() {
    return homeworkId;
  }

  public void setHomeworkId(Long homeworkId) {
    this.homeworkId = homeworkId;
  }

  public String getGithubUrl() {
    return githubUrl;
  }

  public void setGithubUrl(String githubUrl) {
    this.githubUrl = githubUrl;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public LocalDateTime getUpdated() {
    return updated;
  }

  public void setUpdated(LocalDateTime updated) {
    this.updated = updated;
  }

  public List<Review> getReviews() {
    return reviews;
  }

  public void setReviews(List<Review> reviews) {
    this.reviews = reviews;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Submission that = (Submission) o;
    return Objects.equals(submissionId, that.submissionId) &&
      Objects.equals(studentId, that.studentId) &&
      Objects.equals(homeworkId, that.homeworkId) &&
      Objects.equals(githubUrl, that.githubUrl) &&
      Objects.equals(description, that.description) &&
      Objects.equals(created, that.created) &&
      Objects.equals(updated, that.updated) &&
      Objects.equals(reviews, that.reviews);
  }

  @Override
  public int hashCode() {
    return Objects.hash(submissionId, studentId, homeworkId, githubUrl, description, created, updated, reviews);
  }
}
