package ru.hh.school.lms.service.grading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class SubmissionDto {

  @JsonProperty("submissionId")
  private Long submissionId;
  @JsonProperty("studentId")
  private Long studentId;
  @JsonProperty("homeworkId")
  private Long homeworkId;
  @JsonProperty("githubUrl")
  private String githubUrl;
  @JsonProperty("description")
  private String description;
  @JsonProperty("created")
  private Date created;
  @JsonProperty("updated")
  private Date updated;
  @JsonProperty("reviews")
  private List<ReviewDto> reviews;

  @JsonProperty("submissionId")
  public Long getSubmissionId() {
    return submissionId;
  }

  @JsonProperty("submissionId")
  public void setSubmissionId(Long submissionId) {
    this.submissionId = submissionId;
  }

  @JsonProperty("studentId")
  public Long getStudentId() {
    return studentId;
  }

  @JsonProperty("studentId")
  public void setStudentId(Long studentId) {
    this.studentId = studentId;
  }

  @JsonProperty("homeworkId")
  public Long getHomeworkId() {
    return homeworkId;
  }

  @JsonProperty("homeworkId")
  public void setHomeworkId(Long homeworkId) {
    this.homeworkId = homeworkId;
  }

  @JsonProperty("githubUrl")
  public String getGithubUrl() {
    return githubUrl;
  }

  @JsonProperty("githubUrl")
  public void setGithubUrl(String githubUrl) {
    this.githubUrl = githubUrl;
  }

  @JsonProperty("description")
  public String getDescription() {
    return description;
  }

  @JsonProperty("description")
  public void setDescription(String description) {
    this.description = description;
  }

  @JsonProperty("created")
  public Date getCreated() {
    return created;
  }

  @JsonProperty("created")
  public void setCreated(Date created) {
    this.created = created;
  }

  @JsonProperty("updated")
  public Date getUpdated() {
    return updated;
  }

  @JsonProperty("updated")
  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  @JsonProperty("reviews")
  public List<ReviewDto> getReviews() {
    return reviews;
  }

  @JsonProperty("reviews")
  public void setReviews(List<ReviewDto> reviews) {
    this.reviews = reviews;
  }
}
