package ru.hh.school.lms.service.grading.dao;

import ru.hh.school.lms.service.grading.entities.Review;
import ru.hh.school.lms.service.grading.entities.Submission;

import java.util.List;

public interface GradingDao {

  Long addReview(Review grade);

  void updateReview(Review grade);

  void deleteReview(Long reviewId);

  List<Review> getReviews(Long submissionId);

  Long addSubmission(Submission submission);

  void updateSubmission(Submission submission);

  void deleteSubmission(Long submissionId);

  Submission getSubmission(Long studentId, Long homeworkId);

  List<Submission> getSubmissions(Long homeworkId);

  List<Submission> getSubmissionsByStudent(Long studentId);

  List<Submission> getUnreviewedSubmissions(Long homeworkId);

  List<Submission> getUngradedSubmissions(Long homeworkId);

}
