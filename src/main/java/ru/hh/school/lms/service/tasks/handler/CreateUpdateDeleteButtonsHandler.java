package ru.hh.school.lms.service.tasks.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Option;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogOption;
import com.github.seratch.jslack.api.model.dialog.DialogSelectElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.Identifiable;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackEventHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Locale;

/**
 * Reacts to a push button action for creating, modifying, deleting tasks
 */
@Component
final class CreateUpdateDeleteButtonsHandler implements MessageInteractionHandler {
  private final TaskService taskService;
  private final PersonService personService;
  private final LectureService lectureService;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;
  private final CommonHandleMethods commonHandleMethods;

  @Autowired
  public CreateUpdateDeleteButtonsHandler(TaskService taskService, PersonService personService, LectureService lectureService,
                                          SlackHelper slackHelper, @Qualifier("taskMessageSource") MessageSource messages, Locale locale,
                                          CommonHandleMethods commonHandleMethods) {
    this.taskService = taskService;
    this.personService = personService;
    this.lectureService = lectureService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
    this.commonHandleMethods = commonHandleMethods;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    List<Action> actions = event.getActions(); // expected event is not null as it passed through test(Event event) method
    if (actions.isEmpty()) {
      throw new EventHandlingException(event, "Expected that message has at least one action");
    }
    String actionValue = actions.get(0).getValue();
    try {
      ManagementCrudAction action = Identifiable.ofId(actionValue, ManagementCrudAction.class);
      switch (action) {
        case DELETE:
          delete(event);
          break;
        case CREATE:
          create(event);
          break;
        case UPDATE:
          update(event);
          break;
        default:
          throw new EventHandlingException(event, "Unknown action type: " + action);
      }
    } catch (IOException | IllegalArgumentException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  private void create(MessageInteractionEvent event) throws EventHandlingException {
    Iterable<Person> lecturerPersons = personService.findAllWithRole("LECTURER");
    List<Lecture> lectures = lectureService.getAllLectures();

    Dialog dialog = commonHandleMethods
      .taskChangeDialog(messages.getMessage("create_dialog_title", null, locale), null, lectures, lecturerPersons);
    slackHelper.dialog(event, AddTaskDialogSubmissionHandler.class, dialog);
  }

  private void update(MessageInteractionEvent event) throws IOException, EventHandlingException {
    selectorToChangeTask(
      event,
      messages.getMessage("update_dialog_title", null, locale),
      UpdateTaskInteractionHandler.class
    );
  }

  private void delete(MessageInteractionEvent event) throws IOException, EventHandlingException {
    dialogWithSelectorToChangeTask(
      event,
      messages.getMessage("delete_label", null, locale),
      DeleteTaskDialogHandler.class
    );
  }

  private void selectorToChangeTask(MessageInteractionEvent event,
                                    String attachmentText,
                                    Class<? extends CallbackEventHandler> handlerClass) throws IOException, EventHandlingException {
    String actionName = handlerClass.getCanonicalName();

    List<Option> options = taskService.getAllTasks().stream()
      .map(task -> Option.builder()
        .text(task.toString())
        .value(String.valueOf(task.getId()))
        .build())
      .collect(Collectors.toList());

    slackHelper.respond(
      event,
      "",
      handlerClass,
      Attachment.builder()
        .text(attachmentText)
        .actions(List.of(
          Action.builder()
            .name(actionName)
            .text(messages.getMessage("select_placeholder", null, locale))
            .type(Action.Type.SELECT)
            .options(options)
            .build()
        ))
        .build()
    );
  }

  private void dialogWithSelectorToChangeTask(MessageInteractionEvent event,
                                              String attachmentText,
                                              Class<? extends CallbackEventHandler> handlerClass) throws IOException, EventHandlingException {
    String actionName = handlerClass.getCanonicalName();

    List<DialogOption> dialogOptions = taskService.getAllTasks().stream()
      .map(task ->
        DialogOption.builder()
          .label(task.getLabelToDelete())
          .value(String.valueOf(task.getId()))
          .build())
      .collect(Collectors.toList());

    DialogSelectElement element = DialogSelectElement.builder()
      .name(actionName)
      .label(messages.getMessage("select_placeholder", null, locale))
      .placeholder("")
      .options(dialogOptions)
      .build();

    Dialog dialog = Dialog.builder()
      .title(attachmentText)
      .elements(Collections.singletonList(element))
      .submitLabel(messages.getMessage("submit", null, locale))
      .build();

    slackHelper.dialog(event, handlerClass, dialog);
  }
}
