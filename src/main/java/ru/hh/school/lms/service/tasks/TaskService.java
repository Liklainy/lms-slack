package ru.hh.school.lms.service.tasks;

import ru.hh.school.lms.service.tasks.model.Task;

import java.time.LocalDate;
import java.util.List;

public interface TaskService {

  String getSheetUrl();

  Task findById(Long id);

  void deleteById(Long id);

  void saveTask(Task task);

  Task getNextTask();

  List<Task> getNextTasks();

  List<Task> getAllTasks();

  List<Task> getTasksBetween(LocalDate after, LocalDate before);
}
