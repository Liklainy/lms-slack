package ru.hh.school.lms.service.grading.dao;

import ru.hh.school.lms.service.grading.entities.Review;
import ru.hh.school.lms.service.grading.entities.Submission;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryGradingDao implements GradingDao {

  private Long submissionId = 0L;
  private HashMap<Long, Submission> submissions = new HashMap<>();

  private Long reviewId = 0L;
  private HashMap<Long, Review> reviews = new HashMap<>();


  @Override
  public Long addReview(Review review) {
    Long entityId = ++reviewId;
    review.setReviewId(entityId);
    reviews.put(entityId, review);
    // save review to submission
    submissions.get(review.getSubmissionId())
      .getReviews()
      .add(review);
    return entityId;
  }

  @Override
  public void updateReview(Review review) {
    reviews.put(review.getReviewId(), review);
  }

  @Override
  public void deleteReview(Long reviewId) {
    reviews.remove(reviewId);
  }

  @Override
  public List<Review> getReviews(Long submissionId) {
    return reviews.values()
      .stream()
      .filter(x -> x.getSubmissionId().equals(submissionId))
      .collect(Collectors.toList());
  }

  @Override
  public Long addSubmission(Submission submission) {
    Long entityId = ++submissionId;
    submission.setSubmissionId(entityId);
    submissions.put(entityId, submission);
    return entityId;
  }

  @Override
  public void updateSubmission(Submission submission) {
    submissions.put(submission.getSubmissionId(), submission);
  }

  @Override
  public void deleteSubmission(Long submissionId) {
    submissions.remove(submissionId);
  }

  @Override
  public Submission getSubmission(Long studentId, Long homeworkId) {
    return submissions.values()
      .stream()
      .filter(x -> x.getStudentId().equals(studentId) && x.getHomeworkId().equals(homeworkId))
      .findFirst()
      .orElse(null);
  }

  @Override
  public List<Submission> getSubmissions(Long homeworkId) {
    return submissions.values()
      .stream()
      .filter(x -> x.getHomeworkId().equals(homeworkId))
      .collect(Collectors.toList());
  }

  @Override
  public List<Submission> getSubmissionsByStudent(Long studentId) {
    return submissions.values()
      .stream()
      .filter(x -> x.getStudentId().equals(studentId))
      .collect(Collectors.toList());
  }

  @Override
  public List<Submission> getUnreviewedSubmissions(Long homeworkId) {
    return submissions.values()
      .stream()
      .filter(x -> x.getReviews().isEmpty())
      .collect(Collectors.toList());
  }

  @Override
  public List<Submission> getUngradedSubmissions(Long homeworkId) {
    return submissions.values()
      .stream()
      .filter(x -> x.getReviews().stream().allMatch(v -> v.getGrade() == null))
      .collect(Collectors.toList());
  }
}
