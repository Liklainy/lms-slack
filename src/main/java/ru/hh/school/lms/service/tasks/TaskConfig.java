package ru.hh.school.lms.service.tasks;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

@Configuration
public class TaskConfig {

  @Bean(name = "taskMessageSource")
  MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("tasks/messages", "tasks/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

  @Bean
  Locale locale() {
    return new Locale("ru", "RU");
  }
}
