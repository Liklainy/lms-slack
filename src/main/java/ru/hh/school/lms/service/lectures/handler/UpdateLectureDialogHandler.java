package ru.hh.school.lms.service.lectures.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.seratch.jslack.api.model.Attachment;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

@Component
final class UpdateLectureDialogHandler implements DialogSubmissionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;
  private final Gson gson;
  private final LectureService lectureService;
  private final ObjectMapper objectMapper;

  @Autowired
  public UpdateLectureDialogHandler(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale,
                                    SlackHelper slackHelper, Gson gson, LectureService lectureService, ObjectMapper objectMapper) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
    this.gson = gson;
    this.lectureService = lectureService;
    this.objectMapper = objectMapper;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    UpdateDialogState state = gson.fromJson(event.getState(), UpdateDialogState.class);
    Long lectureToUpdateId = state.getLmsEntityId();
    Optional<Lecture> oldLectureOpt = lectureService.findById(lectureToUpdateId);
    if (oldLectureOpt.isEmpty()) {
      throw new EventHandlingException(event, "There is no lecture with id=" + lectureToUpdateId);
    }
    Lecture oldLecture = oldLectureOpt.get();

    try {
      Lecture updatedLecture = objectMapper.readValue(event.getSubmission().toString(), Lecture.class);
      updatedLecture.setId(oldLecture.getId());

      lectureService.saveLecture(updatedLecture);

      String updatedLectureStr = lectureService.findById(updatedLecture.getId()).get().toString();


      slackHelper.update(
        event,
        event.getChannel().getId(),
        state.getTs(),
        "",
        Attachment.builder()
          .text(messages.getMessage("update_success", new Object[]{oldLecture, updatedLectureStr}, locale))
          .build());

    } catch (IOException e) {
      throw new EventHandlingException(event, "Map event.submission to Lecture failed (deserialization error)", e);
    }
  }
}
