package ru.hh.school.lms.service.grading.dao;

import ru.hh.school.lms.repository.ReviewRepository;
import ru.hh.school.lms.repository.SubmissionRepository;
import ru.hh.school.lms.service.grading.entities.Review;
import ru.hh.school.lms.service.grading.entities.Submission;

import java.util.List;

public class SpringDataGradingDao implements GradingDao {

  private final SubmissionRepository submissionRepository;
  private final ReviewRepository reviewRepository;

  public SpringDataGradingDao(
    SubmissionRepository submissionRepository,
    ReviewRepository reviewRepository
  ) {
    this.submissionRepository = submissionRepository;
    this.reviewRepository = reviewRepository;
  }

  @Override
  public Long addReview(Review review) {
    Review savedReview = reviewRepository.save(review);
    return savedReview.getReviewId();
  }

  @Override
  public void updateReview(Review review) {
    reviewRepository.save(review);
  }

  @Override
  public void deleteReview(Long reviewId) {
    reviewRepository.deleteById(reviewId);
  }

  @Override
  public List<Review> getReviews(Long submissionId) {
    return reviewRepository.findBySubmissionId(submissionId);
  }

  @Override
  public Long addSubmission(Submission submission) {
    Submission savedSubmission = submissionRepository.save(submission);
    return savedSubmission.getSubmissionId();
  }

  @Override
  public void updateSubmission(Submission submission) {
    submissionRepository.save(submission);
  }

  @Override
  public void deleteSubmission(Long submissionId) {
    submissionRepository.deleteById(submissionId);
  }

  @Override
  public Submission getSubmission(Long studentId, Long homeworkId) {
    return submissionRepository.findByStudentIdAndHomeworkId(studentId, homeworkId);
  }

  @Override
  public List<Submission> getSubmissions(Long homeworkId) {
    return submissionRepository.findByHomeworkId(homeworkId);
  }

  @Override
  public List<Submission> getSubmissionsByStudent(Long studentId) {
    return submissionRepository.findByStudentId(studentId);
  }

  @Override
  public List<Submission> getUnreviewedSubmissions(Long homeworkId) {
    return submissionRepository.findByReviewsIsNull();
  }

  @Override
  public List<Submission> getUngradedSubmissions(Long homeworkId) {
    return submissionRepository.findByReviewsGradeIsNull();
  }
}
