package ru.hh.school.lms.service.person.handler;

import ru.hh.school.lms.service.person.model.Person;

import java.util.Collections;

final class PersonDialogData {

  private String firstName;
  private String familyName;
  private String patronymic;
  private String slackAccountId;
  private String githubAccountLogin;

  Person createPerson() {
    return new Person(firstName, familyName, patronymic, slackAccountId, githubAccountLogin, Collections.emptyList());
  }

  void updatePerson(Person person) {
    person.setFirstName(firstName);
    person.setFamilyName(familyName);
    person.setPatronymic(patronymic);
    person.setGithubAccountLogin(githubAccountLogin);
    person.setSlackAccountId(slackAccountId);
  }
}



