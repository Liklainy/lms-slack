package ru.hh.school.lms.service.notifications;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Locale;

@EnableScheduling
@Configuration
public class NotificationConfig {

  @Bean(name = "notificationMessageSource")
  MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("notifications/messages", "notifications/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return  messageSource;
  }

  @Bean
  Locale locale() {
    return new Locale("ru", "RU");
  }
}
