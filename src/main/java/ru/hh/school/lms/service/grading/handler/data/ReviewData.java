package ru.hh.school.lms.service.grading.handler.data;

import com.google.gson.annotations.SerializedName;

public class ReviewData {
  @SerializedName("grade")
  public Double grade;
  @SerializedName("content")
  public String content;
}
