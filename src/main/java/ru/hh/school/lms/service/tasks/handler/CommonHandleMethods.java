package ru.hh.school.lms.service.tasks.handler;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogElement;
import com.github.seratch.jslack.api.model.dialog.DialogOption;
import com.github.seratch.jslack.api.model.dialog.DialogSelectElement;
import com.github.seratch.jslack.api.model.dialog.DialogTextElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.tasks.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import java.util.Locale;

import static ru.hh.school.lms.service.tasks.handler.TaskDialogElement.LECTURE_ID;
import static ru.hh.school.lms.service.tasks.handler.TaskDialogElement.VERIFIER_ID;
import static ru.hh.school.lms.util.DateUtilsLms.dateFormat;

/**
 * Helper methods
 */
@Component
final class CommonHandleMethods {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommonHandleMethods.class);
  private final MessageSource messages;
  private final Locale locale;

  CommonHandleMethods(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale) {
    this.messages = messages;
    this.locale = locale;
  }

  Dialog taskChangeDialog(String dialogTitle, Task taskToUpdate, List<Lecture> lectures, Iterable<Person> verifiers) {

    DialogElement element;
    List<DialogElement> listDialogElement = new ArrayList<>();
    List<Object> taskToUpdateAsList = taskToUpdate == null ? null : taskAsList(taskToUpdate);
    int i = 0;

    for (TaskDialogElement elem : TaskDialogElement.values()) {
      String valueToUpdate = taskToUpdate == null ? "" : String.valueOf(taskToUpdateAsList.get(i++));

      if (elem.getName().equals(LECTURE_ID.getName())) {
        List<DialogOption> dialogOptions = lectures.stream()
          .map(lecture ->
            DialogOption.builder()
              .label(lecture.getTitle())
              .value(String.valueOf(lecture.getId()))
              .build())
          .collect(Collectors.toList());

        element = DialogSelectElement.builder()
          .name(elem.getName())
          .label(elem.getLabel())
          .placeholder(elem.getPlaceholder())
          .value(taskToUpdate == null ? elem.getPlaceholder() : String.valueOf(taskToUpdate.getLectureId()))
          .options(dialogOptions)
          .build();
      } else if (elem.getName().equals(VERIFIER_ID.getName())) {
        List<DialogOption> dialogOptions = StreamSupport.stream(verifiers.spliterator(), false)
          .map(verifier ->
            DialogOption.builder()
              .label(verifier.toString())
              .value(String.valueOf(verifier.getId()))
              .build())
          .collect(Collectors.toList());

        element = DialogSelectElement.builder()
          .name(elem.getName())
          .label(elem.getLabel())
          .placeholder(elem.getPlaceholder())
          .value(taskToUpdate == null ? elem.getPlaceholder() : String.valueOf(taskToUpdate.getVerifierId()))
          .options(dialogOptions)
          .build();
      } else {
        element = DialogTextElement.builder()
          .name(elem.getName())
          .label(elem.getLabel())
          .maxLength(elem.getMaxLength())
          .minLength(elem.getMinLength())
          .placeholder(elem.getPlaceholder())
          .value(valueToUpdate)
          .build();
      }
      listDialogElement.add(element);
    }

    Dialog dialog = Dialog.builder()
      .title(dialogTitle)
      .elements(listDialogElement)
      .submitLabel(messages.getMessage("submit", null, locale))
      .build();

    return dialog;
  }

  static List<Object> taskAsList(Task task) {
    ArrayList<Object> listTask = new ArrayList<>();
    listTask.add(task.getLectureId());
    listTask.add(task.getVerifierId());
    listTask.add(dateFormat(task.getDeadline()));
    listTask.add(task.getTaskUrl());
    listTask.add(task.getDescription());
    return listTask;
  }
}
