package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogOption;
import com.github.seratch.jslack.api.model.dialog.DialogSelectElement;
import com.github.seratch.jslack.api.model.dialog.DialogTextElement;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.LectureDtoFieldName;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.util.DateUtilsLms;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Имхо неудобно, когда сообщение с кнопками CRUD при нажатии на одну из кнопок меняется. Этот класс - один
 * из вариантов решения - он содержит последний timestamp сообщения, и при помощи slack api можно получить сообщения
 * с Action != null, т.е. с интерактивными компонентами, и удалить их.
 * (В документации slack api рекоммендуется не накапливать интерактивные сообщения в чате)
 */
@Component("lecturesCommonHandleMethods")
final class CommonHandleMethods {
  // TODO: 25.03.2019 string -> map(chanelId, timestamp)
  private static String buttonsMessageLastTimestamp;
  private final MessageSource messages;
  private final Locale locale;

  CommonHandleMethods(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale) {
    this.messages = messages;
    this.locale = locale;
  }

  public static String getButtonsMessageLastTimestamp() {
    return buttonsMessageLastTimestamp;
  }

  public static void setButtonsMessageLastTimestamp(String buttonsMessageLastTimestamp) {
    CommonHandleMethods.buttonsMessageLastTimestamp = buttonsMessageLastTimestamp;
  }

  // fields of dialog are empty
  Dialog lectureCreateDialog(String dialogTitle, Iterable<Person> lecturers) {
    return lectureUpdateDialog(dialogTitle, null, lecturers);
  }

  // fields of dialog contain parameters of lectureToUpdate
  Dialog lectureUpdateDialog(String dialogTitle, Lecture lectureToUpdate, Iterable<Person> lecturers) {
    DialogTextElement lectureDateTextElement = DialogTextElement.builder()
      .label(LectureDialogElement.DATE.getLabel())
      .name(LectureDialogElement.DATE.getName())
      .hint(messages.getMessage("dialog_date_hint", null, locale) + " " + DateUtilsLms.DATE_PATTERN)
      .value(lectureToUpdate == null ? "" : DateUtilsLms.dateFormat(lectureToUpdate.getDate()))
      .maxLength(10)
      .minLength(10)
      .placeholder(DateUtilsLms.DATE_PATTERN)
      .build();

    DialogTextElement lectureTitleTextElement = DialogTextElement.builder()
      .label(LectureDialogElement.TITLE.getLabel())
      .name(LectureDialogElement.TITLE.getName())
      .value(lectureToUpdate == null ? "" : lectureToUpdate.getTitle())
      .maxLength(50)
      .minLength(1)
      .placeholder(messages.getMessage("dialog_title_placeholder", null, locale))
      .build();


    DialogSelectElement lecturerSelectElement = DialogSelectElement.builder()
      .label(LectureDialogElement.LECTURER_ID.getLabel())
      .name(LectureDialogElement.LECTURER_ID.getName())
      .value(lectureToUpdate == null ? "" : String.valueOf(lectureToUpdate.getLecturerId()))
      .placeholder(messages.getMessage("dialog_lecturer_placeholder", null, locale))
      .options(
        StreamSupport.stream(lecturers.spliterator(), false)
          .map(lecturer ->
            DialogOption.builder()
              .label(lecturer.toString())
              .value(String.valueOf(lecturer.getId()))
              .build())
          .collect(Collectors.toList())
      )
      .build();

    DialogSelectElement lectureSpecializationTextElement = DialogSelectElement.builder()
      .label(LectureDialogElement.SPECIALIZATION.getLabel())
      .name(LectureDialogElement.SPECIALIZATION.getName())
      .placeholder(messages.getMessage("select_placeholder", null, locale))
      .value(lectureToUpdate == null ? "" : lectureToUpdate.getSpecialization())
      .options(Arrays.asList(
        DialogOption.builder().label("Frontend").value("Frontend").build(),
        DialogOption.builder().label("Backend").value("Backend").build(),
        DialogOption.builder().label("All").value("All").build()
      ))
      .build();

    DialogSelectElement homework = DialogSelectElement.builder()
      .name(LectureDialogElement.HOMEWORK.getName())
      .label(LectureDialogElement.HOMEWORK.getLabel())
      .placeholder(messages.getMessage("select_placeholder", null, locale))
      .value(lectureToUpdate == null ? String.valueOf(false) : String.valueOf(lectureToUpdate.isHomework()))
      .options(Arrays.asList(
        DialogOption.builder().label(messages.getMessage("yes", null, locale)).value("true").build(),
        DialogOption.builder().label(messages.getMessage("no", null, locale)).value("false").build()
      ))
      .build();

    Dialog dialog = Dialog.builder()
      .title(dialogTitle)
      .elements(Arrays.asList(
        lectureDateTextElement,
        lectureTitleTextElement,
        lecturerSelectElement,
        lectureSpecializationTextElement,
        homework
      ))
      .submitLabel(messages.getMessage("submit", null, locale))
      .build();

    return dialog;
  }

  enum LectureDialogElement {
    DATE(LectureDtoFieldName.DATE, "Дата лекции"),
    TITLE(LectureDtoFieldName.TITLE, "Название лекции"),
    LECTURER_ID(LectureDtoFieldName.LECTURER_ID, "Лектор"),
    SPECIALIZATION(LectureDtoFieldName.SPECIALIZATION, "Специализация"),
    HOMEWORK(LectureDtoFieldName.HOMEWORK, "Домашнее задание");

    private final String name;
    private final String label;

    LectureDialogElement(String name, String label) {
      this.name = name;
      this.label = label;
    }

    public String getName() {
      return name;
    }

    public String getLabel() {
      return label;
    }
  }

  String getBundleString(String key) {
    return messages.getMessage(key, null, locale);
  }
}

