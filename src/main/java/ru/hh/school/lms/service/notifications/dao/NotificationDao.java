package ru.hh.school.lms.service.notifications.dao;

import ru.hh.school.lms.service.notifications.model.Notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface NotificationDao {

  void addNotification(Notification notification);

  Notification findById(Long id);

  void deleteById(Long id);

  List<Notification> getLessThanEqualTimeStamp(LocalDateTime dateTime);

  void deleteLessThanEqualTimeStamp(LocalDateTime dateTime);

  Optional<Notification> findFirstRowOrderByDateTime();

}
