package ru.hh.school.lms.service.tasks.handler;

import ru.hh.school.lms.service.tasks.TaskDtoFieldName;
import ru.hh.school.lms.util.DateUtilsLms;

enum TaskDialogElement {
  LECTURE_ID(TaskDtoFieldName.LECTURE_ID, "Lecture Name", 1, 100, "Название лекции, которой соответствует задача"),
  VERIFIER_ID(TaskDtoFieldName.VERIFIED_ID, "Verifier Name", 1, 100, "Проверяющий: лектор или другое лицо"),
  DEADLINE(TaskDtoFieldName.DEADLINE, "Deadline", 10, 10, DateUtilsLms.DATE_PATTERN + ", пример: 01/01/2000"),
  TASK_URL(TaskDtoFieldName.TASK_URL, "Task Url", 1, 100, "Ссылка на GitHub с условиями домашнего задания"),
  DESCRIPTION(TaskDtoFieldName.DESCRIPTION, "Description", 1, 150, "Краткое описание задачи");

  private final String name;
  private final String label;
  private final int minLength;
  private final int maxLength;
  private final String placeholder;

  TaskDialogElement(String name, String label, int minLength, int maxLength, String placeholder) {
    this.name = name;
    this.label = label;
    this.minLength = minLength;
    this.maxLength = maxLength;
    this.placeholder = placeholder;
  }

  public String getName() {
    return name;
  }

  public String getLabel() {
    return label;
  }

  public int getMinLength() {
    return minLength;
  }

  public int getMaxLength() {
    return maxLength;
  }

  public String getPlaceholder() {
    return placeholder;
  }
}
