package ru.hh.school.lms.service.lectures.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.seratch.jslack.api.model.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.io.IOException;
import java.util.Locale;

@Component
final class AddLectureDialogSubmissionHandler implements DialogSubmissionHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(AddLectureDialogSubmissionHandler.class);

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;
  private final LectureService lectureService;
  private final ObjectMapper objectMapper;

  @Autowired
  public AddLectureDialogSubmissionHandler(SlackHelper slackHelper,
                                           @Qualifier("lecturesMessageSource") MessageSource messages,
                                           Locale locale,
                                           LectureService lectureService,
                                           ObjectMapper objectMapper) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
    this.lectureService = lectureService;
    this.objectMapper = objectMapper;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    try {
      Lecture lectureToAdd = objectMapper.readValue(event.getSubmission().toString(), Lecture.class);

      lectureService.saveLecture(lectureToAdd);
      String newLectureStr = lectureService.findById(lectureToAdd.getId()).get().toString();
      slackHelper.respond(
        event,
        "",
        Attachment.builder().text(messages.getMessage("create_success", new String[]{newLectureStr}, locale)).build());
    } catch (IOException e) {
      throw new EventHandlingException(event, "Map event.submission to Lecture failed (deserialization error)", e);
    }
  }
}
