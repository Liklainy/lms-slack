package ru.hh.school.lms.service.person.handler;

import ru.hh.school.lms.service.common.Identifiable;

public enum Command implements Identifiable {
  LIST,
  MANAGEMENT
}
