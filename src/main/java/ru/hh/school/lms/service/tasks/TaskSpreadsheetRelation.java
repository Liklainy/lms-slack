package ru.hh.school.lms.service.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.TaskRepository;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.sheets.GoogleSheetsClient;
import ru.hh.school.lms.sheets.GoogleSpreadsheetRelation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Qualifier("tasks")
@PropertySource(value = "classpath:spreadsheets.properties", encoding = "UTF-8")
final class TaskSpreadsheetRelation implements GoogleSpreadsheetRelation {

  @Value("${tasks_spreadsheet_title}")
  private String tasksSpreadsheetTitle;

  private final List<Object> taskSheetHeader = Arrays.asList(
    "Тема задачи", "Проверяющий",
    "Дедлайн", "Ссылка на GitHub", "Краткое описание");
  private final TaskRepository taskRepository;

  TaskSpreadsheetRelation(@Autowired TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @Override
  public String getSpreadsheetTitle() {
    return tasksSpreadsheetTitle;
  }

  @Override
  public void mapDbToSpreadsheet(GoogleSheetsClient client, String spreadsheetId) throws IOException {
    List<List<Object>> response = new ArrayList<>(Collections.singletonList(taskSheetHeader));
    List<List<Object>> taskItems = taskRepository.findAll()
      .stream()
      .sorted(Comparator.comparing(Task::getDeadline))
      .map(this::taskToSheetRow)
      .collect(Collectors.toList());
    response.addAll(taskItems);
    client.setSheet(
      spreadsheetId,
      "A1",
      response
    );
  }

  private List<Object> taskToSheetRow(Task entity) {
    return List.of(
      entity.getLecture().getTitle(),
      entity.getPerson().toString(),
      entity.getDeadline().format(getDateTimeFormatter()),
      entity.getTaskUrl(),
      entity.getDescription()
    );
  }
}
