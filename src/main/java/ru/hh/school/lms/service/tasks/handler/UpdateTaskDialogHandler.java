package ru.hh.school.lms.service.tasks.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.github.seratch.jslack.api.model.Attachment;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.webhook.interactionexc.Error;
import ru.hh.school.lms.webhook.interactionexc.ErrorItem;
import ru.hh.school.lms.webhook.interactionexc.InteractionException;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

@Component
final class UpdateTaskDialogHandler implements DialogSubmissionHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(UpdateTaskDialogHandler.class);


  private final TaskService taskService;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Gson gson;
  private final ObjectMapper objectMapper;
  private final Locale locale;

  @Autowired
  public UpdateTaskDialogHandler(TaskService taskService, SlackHelper slackHelper,
                                 @Qualifier("taskMessageSource") MessageSource messages,
                                 Locale locale,
                                 Gson gson, ObjectMapper objectMapper) {

    this.taskService = taskService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.gson = gson;
    this.objectMapper = objectMapper;
    this.locale = locale;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    try {
      UpdateDialogState state = gson.fromJson(event.getState(), UpdateDialogState.class);
      Long taskToUpdateId = state.getLmsEntityId();
      Task oldTask = taskService.findById(taskToUpdateId);
      if (oldTask == null) {
        throw new EventHandlingException(event, "There is no task with id " + taskToUpdateId);
      }
      Task updatedTask = objectMapper.readValue(event.getSubmission().toString(), Task.class);
      updatedTask.setId(taskToUpdateId);
      taskService.saveTask(updatedTask);
      updatedTask = taskService.findById(taskToUpdateId);

      slackHelper.update(
        event,
        event.getChannel().getId(),
        state.getTs(),
        "",
        Attachment.builder()
          .text(messages.getMessage("update_success", new Object[]{oldTask, updatedTask}, locale))
          .build());
    } catch (InvalidFormatException e) {
      LOGGER.info("Invalid format exception (deserialization error)");
      Error error = new Error();
      error.setErrors(List.of(new ErrorItem(
        TaskDialogElement.DEADLINE.getName(),
        "Сheck that date is entered correctly using example: 01/01/2000"
      )));
      throw new InteractionException(event, error);
    } catch (IOException e) {
      LOGGER.error("Map event.submission to Task failed (deserialization error)", e);
    }

  }
}
