package ru.hh.school.lms.service.tasks;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.common.json.GsonFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.notifications.model.Notification;
import ru.hh.school.lms.service.notifications.service.NotificationService;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.tasks.model.Task;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class TaskNotifications {

  private final TaskService taskService;
  private final PersonService personService;
  private final NotificationService notificationService;
  private final MessageSource messages;
  private final Locale locale;

  public TaskNotifications(TaskService taskService, PersonService personService,
                           NotificationService notificationService,
                           @Qualifier("taskMessageSource")  MessageSource messages, Locale locale) {
    this.taskService = taskService;
    this.personService = personService;
    this.notificationService = notificationService;
    this.messages = messages;
    this.locale = locale;
  }

  @Scheduled(cron="0 30 10 * * FRI")
  private void reportNextWeekTasksDeadlines() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.MONDAY);
    calendar.add(Calendar.DAY_OF_WEEK, 7);
    LocalDate after = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.SUNDAY);
    LocalDate before = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    List<Attachment> attachments = getListTasksAttachment(taskService.getTasksBetween(after, before));
    if (attachments.isEmpty()) {
      return;
    }
    Iterable<Person> pupils = personService.findAllWithRole("STUDENT");
    LocalDateTime dateTimeNow = LocalDateTime.now();
    for (Person pupil : pupils) {
      Long notificationId = notificationService.setNotification(new Notification(
        dateTimeNow.plusSeconds(30),
        messages.getMessage("pupil_next_week_deadlines", null, locale),
        pupil.getSlackAccountId(),
        GsonFactory.createSnakeCase().toJson(attachments))
      );
    }
  }

  private List<Attachment> getListTasksAttachment(List<Task> tasks) {
    List<Attachment> attachments = tasks.stream()
      .map(task -> Attachment.builder()
        .title(task.getLecture().getTitle())
        .titleLink(task.getTaskUrl())
        .text(task.getLabelToNotify())
        .build())
      .collect(Collectors.toList());
    return attachments;
  }
}
