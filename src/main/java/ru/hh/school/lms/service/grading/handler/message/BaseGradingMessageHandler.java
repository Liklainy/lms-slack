package ru.hh.school.lms.service.grading.handler.message;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;
import ru.hh.school.lms.service.grading.handler.data.GradingAttachmentsFactory;
import ru.hh.school.lms.service.grading.services.GradingService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackStorage;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.Locale;
import java.util.regex.Pattern;

public abstract class BaseGradingMessageHandler implements MessagePostedHandler {

  protected final GradingService gradingService;
  protected final SlackHelper client;
  protected final CallbackStorage callbackStorage;
  protected final GradingAttachmentsFactory gradingAttachmentsFactory;
  protected final MessageSource messages;
  protected final Locale locale;

  public BaseGradingMessageHandler(
    GradingService gradingService,
    SlackHelper client,
    CallbackStorage callbackStorage,
    GradingAttachmentsFactory gradingAttachmentsFactory,
    MessageSource messages,
    Locale locale
  ) {
    this.gradingService = gradingService;
    this.client = client;
    this.callbackStorage = callbackStorage;
    this.gradingAttachmentsFactory = gradingAttachmentsFactory;
    this.messages = messages;
    this.locale = locale;
  }

  @Override
  public boolean test(MessagePostedEvent messagePostedEvent) {
    if (messagePostedEvent == null) {
      return false;
    }
    String text = messagePostedEvent.getText();
    if (StringUtils.isEmpty(text)) {
      return false;
    }
    String regexp = messages.getMessage(regExpKey(), null, locale);
    return Pattern.matches(regexp, text.toLowerCase().trim());
  }

  abstract String regExpKey();
}
