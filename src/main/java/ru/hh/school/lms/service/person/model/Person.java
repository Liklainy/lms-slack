package ru.hh.school.lms.service.person.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "person")
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "family_name")
  private String familyName;

  private String patronymic;

  @Column(name = "slack_account_id")
  private String slackAccountId;

  @Column(name = "github_account_login")
  private String githubAccountLogin;

  @ManyToMany(fetch = FetchType.EAGER) // TODO consider lazy initialization
  @JoinTable(name = "privileges",
    joinColumns = @JoinColumn(name = "person_id"),
    inverseJoinColumns = @JoinColumn(name = "role_id")
  )
  private Collection<Role> roles;

  public Person() {
  }

  public Person(String firstName, String familyName, String patronymic, String slackAccountId, String githubAccountLogin, Collection<Role> roles) {
    this.firstName = firstName;
    this.familyName = familyName;
    this.patronymic = patronymic;
    this.slackAccountId = slackAccountId;
    this.githubAccountLogin = githubAccountLogin;
    this.roles = roles;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFamilyName() {
    return familyName;
  }

  public void setFamilyName(String familyName) {
    this.familyName = familyName;
  }

  public String getPatronymic() {
    return patronymic;
  }

  public void setPatronymic(String patronymic) {
    this.patronymic = patronymic;
  }

  public String getSlackAccountId() {
    return slackAccountId;
  }

  public void setSlackAccountId(String slackAccountId) {
    this.slackAccountId = slackAccountId;
  }

  public String getGithubAccountLogin() {
    return githubAccountLogin;
  }

  public void setGithubAccountLogin(String githubAccountLogin) {
    this.githubAccountLogin = githubAccountLogin;
  }

  public Collection<Role> getRoles() {
    return roles;
  }

  public void setRoles(Collection<Role> roles) {
    this.roles = roles;
  }

  @Override
  public String toString() {
    return String.join(" ", familyName, firstName);
  }
}
