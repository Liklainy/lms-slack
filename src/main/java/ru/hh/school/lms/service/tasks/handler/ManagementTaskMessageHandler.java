package ru.hh.school.lms.service.tasks.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.github.seratch.jslack.api.model.Action.Type.BUTTON;
import static ru.hh.school.lms.service.common.ManagementCrudAction.getName;

/**
 * Creates a basic form with three buttons for creating, modifying, deleting tasks
 */
@Component
final class ManagementTaskMessageHandler implements MessagePostedHandler {

  private final String manageTasksCommand;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;

  @Autowired
  public ManagementTaskMessageHandler(SlackHelper slackHelper, @Qualifier("taskMessageSource") MessageSource messages, Locale locale) {
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
    manageTasksCommand = messages.getMessage("edit", null, locale);
  }

  private Action toSlackAction(ManagementCrudAction action) {
    return Action.builder()
      .name(getName())
      .text(messages.getMessage(action.getBundleKey(), null, locale))
      .type(BUTTON)
      .value(action.getId())
      .build();
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    List<Action> actions = Arrays.stream(ManagementCrudAction.values())
      .map(this::toSlackAction).collect(Collectors.toList());
    Attachment attachment = Attachment.builder()
      .text(messages.getMessage("choose_action_attachment", null, locale))
      .actions(actions)
      .build();
    slackHelper.respond(event,
      messages.getMessage("choose_action_message", null, locale),
      CreateUpdateDeleteButtonsHandler.class,
      attachment);
  }

  @Override
  public boolean test(MessagePostedEvent event) {
    return manageTasksCommand.equals(event.getText());
  }
}
