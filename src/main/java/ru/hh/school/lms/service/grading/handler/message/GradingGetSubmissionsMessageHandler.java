package ru.hh.school.lms.service.grading.handler.message;

import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.grading.dto.SubmissionDto;
import ru.hh.school.lms.service.grading.handler.data.GradingAttachmentsFactory;
import ru.hh.school.lms.service.grading.services.GradingService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackStorage;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Component
public class GradingGetSubmissionsMessageHandler extends BaseGradingMessageHandler {

  @Autowired
  public GradingGetSubmissionsMessageHandler(
    GradingService gradingService,
    SlackHelper client,
    CallbackStorage callbackStorage,
    GradingAttachmentsFactory gradingAttachmentsFactory,
    @Qualifier("gradingMessageSource") MessageSource messages,
    Locale locale
  ) {
    super(gradingService, client, callbackStorage, gradingAttachmentsFactory, messages, locale);
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    String selectCallbackId = UUID.randomUUID().toString();

    Attachment selectHomeworkAttachment = gradingAttachmentsFactory.getSelectHomework(selectCallbackId);

    callbackStorage.putInteractiveMessageConsumer(selectCallbackId, se -> {
      Long selectedHomeworkId = Long.parseLong(se.getActions().get(0).getSelectedOptions().get(0).getValue());
      List<SubmissionDto> submissions = gradingService.getSubmissions(selectedHomeworkId);
      Attachment[] attachments = gradingAttachmentsFactory.getSubmissions(submissions);
      client.update(se, null, attachments);
      callbackStorage.removeInteractiveMessageConsumer(selectCallbackId);
    });

    client.respond(event, null, selectHomeworkAttachment);
  }

  @Override
  String regExpKey() {
    return "get_submissions";
  }
}
