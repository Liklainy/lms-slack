package ru.hh.school.lms.service.person.handler;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.PersonRepository;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.util.Locale;

@Component
final class CreateDialogHandler implements DialogSubmissionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final PersonRepository personRepository;
  private final SlackHelper slackHelper;
  private final Gson gson;

  @Autowired
  public CreateDialogHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale,
                             PersonRepository personRepository, SlackHelper slackHelper, Gson gson) {
    this.messages = messages;
    this.locale = locale;
    this.personRepository = personRepository;
    this.slackHelper = slackHelper;
    this.gson = gson;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    PersonDialogData dialogData = gson.fromJson(event.getSubmission(), PersonDialogData.class);
    Person newPerson = dialogData.createPerson();
    personRepository.save(newPerson);
    slackHelper.respond(event, messages.getMessage("create_success", new Person[]{newPerson}, locale)); //TODO dialogData or newPerson ?
  }

  @Override
  public boolean test(DialogSubmissionEvent event) {
    return ManagementCrudAction.CREATE.getId().equals(event.getCallbackId());
  }
}
