package ru.hh.school.lms.service.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.repository.RoleRepository;
import ru.hh.school.lms.service.person.model.Role;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
class SpringDataRoleService implements RoleService {

  private final RoleRepository roleRepository;

  @Autowired
  SpringDataRoleService(RoleRepository roleRepository) {
    this.roleRepository = roleRepository;
  }

  @Override
  public Optional<Role> findByTitle(String title) {
    return roleRepository.findByTitle(title);
  }

  @Override
  public List<Role> findAll() {
    return roleRepository.findAll();
  }
}
