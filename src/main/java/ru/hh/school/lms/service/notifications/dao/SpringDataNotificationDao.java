package ru.hh.school.lms.service.notifications.dao;

import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.NotificationRepository;
import ru.hh.school.lms.service.notifications.model.Notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
class SpringDataNotificationDao implements NotificationDao {

  private final NotificationRepository notificationRepository;

  public SpringDataNotificationDao(NotificationRepository notificationRepository) {
    this.notificationRepository = notificationRepository;
  }

  @Override
  public void addNotification(Notification notification) {
    notificationRepository.save(notification);
  }

  @Override
  public Notification findById(Long id) {
    return notificationRepository.findById(id).orElse(null);
  }

  @Override
  public void deleteById(Long id) {
    notificationRepository.deleteById(id);
  }

  @Override
  public List<Notification> getLessThanEqualTimeStamp(LocalDateTime date) {
    return notificationRepository.findAllByDateTimeLessThanEqualOrderByDateTime(date);
  }

  @Override
  public void deleteLessThanEqualTimeStamp(LocalDateTime date) {
    notificationRepository.deleteAllByDateTimeLessThanEqual(date);
  }

  @Override
  public Optional<Notification> findFirstRowOrderByDateTime() {
    return notificationRepository.findFirstByOrderByDateTime();
  }
}
