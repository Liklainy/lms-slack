package ru.hh.school.lms.service.tasks.handler;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.List;
import java.util.Locale;

@Component
final class UpdateTaskInteractionHandler implements MessageInteractionHandler {

  private final TaskService taskService;
  private final PersonService personService;
  private final LectureService lectureService;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;
  private final Gson gson;
  private final CommonHandleMethods commonHandleMethods;

  @Autowired
  public UpdateTaskInteractionHandler(TaskService taskService, PersonService personService, LectureService lectureService,
                                      SlackHelper slackHelper, @Qualifier("taskMessageSource") MessageSource messages, Locale locale,
                                      Gson gson, CommonHandleMethods commonHandleMethods) {
    this.taskService = taskService;
    this.personService = personService;
    this.lectureService = lectureService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
    this.gson = gson;
    this.commonHandleMethods = commonHandleMethods;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    try {
      Long taskToUpdateId = Long.valueOf(
        event.getActions().get(0).
          getSelectedOptions().get(0).getValue()
      );

      Task taskToUpdate = taskService.findById(taskToUpdateId);
      Iterable<Person> lecturerPersons = personService.findAllWithRole("LECTURER");
      //TODO: стоит ли давать возможность менять лекцию на другую у уже созданного дз по определенной лекции?
      List<Lecture> lectures = lectureService.getAllLectures();

      Dialog dialog = commonHandleMethods
        .taskChangeDialog(messages.getMessage("update_dialog_title", null, locale), taskToUpdate, lectures, lecturerPersons);

      UpdateDialogState dialogState = new UpdateDialogState(
        taskToUpdateId,
        event.getChannel().getId(),
        event.getOriginalMessage().getTs()
      );

      slackHelper.dialog(event, dialog, UpdateTaskDialogHandler.class, gson.toJson(dialogState));
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has one action and one selected option");
    }
  }
}
