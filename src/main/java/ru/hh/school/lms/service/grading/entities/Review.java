package ru.hh.school.lms.service.grading.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "review")
public class Review {

  @Id
  @Column(name = "review_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long reviewId;

  @Column(name = "submission_id")
  private Long submissionId;
  @Column(name = "reviewer_id")
  private Long reviewerId;
  private Double grade;
  private String content;
  private LocalDateTime created;
  private LocalDateTime updated;

  public Long getReviewId() {
    return reviewId;
  }

  public void setReviewId(Long reviewId) {
    this.reviewId = reviewId;
  }

  public Long getSubmissionId() {
    return submissionId;
  }

  public void setSubmissionId(Long submissionId) {
    this.submissionId = submissionId;
  }

  public Long getReviewerId() {
    return reviewerId;
  }

  public void setReviewerId(Long reviewerId) {
    this.reviewerId = reviewerId;
  }

  public Double getGrade() {
    return grade;
  }

  public void setGrade(Double grade) {
    this.grade = grade;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public LocalDateTime getUpdated() {
    return updated;
  }

  public void setUpdated(LocalDateTime updated) {
    this.updated = updated;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Review that = (Review) o;
    return Objects.equals(reviewId, that.reviewId) &&
      Objects.equals(submissionId, that.submissionId) &&
      Objects.equals(reviewerId, that.reviewerId) &&
      Objects.equals(grade, that.grade) &&
      Objects.equals(content, that.content) &&
      Objects.equals(created, that.created) &&
      Objects.equals(updated, that.updated);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reviewId, submissionId, reviewerId, grade, content, created, updated);
  }
}
