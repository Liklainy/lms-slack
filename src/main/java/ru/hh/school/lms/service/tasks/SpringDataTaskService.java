package ru.hh.school.lms.service.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.repository.TaskRepository;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.sheets.GoogleSheetsSynchronizer;
import ru.hh.school.lms.sheets.GoogleSpreadsheetRelation;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
class SpringDataTaskService implements TaskService {

  private final TaskRepository taskRepository;
  private final GoogleSheetsSynchronizer sheetsSynchronizer;
  private final GoogleSpreadsheetRelation spreadsheetRelation;

  @Autowired
  public SpringDataTaskService(TaskRepository taskRepository, GoogleSheetsSynchronizer sheetsSynchronizer,
                               @Qualifier("tasks") GoogleSpreadsheetRelation spreadsheetRelation) {
    this.taskRepository = taskRepository;
    this.sheetsSynchronizer = sheetsSynchronizer;
    this.spreadsheetRelation = spreadsheetRelation;
  }

  @Override
  public String getSheetUrl() {
    return spreadsheetRelation.getSpreadsheetUrl();
  }

  @Override
  public Task findById(Long id) {
    Optional<Task> taskOptional = taskRepository.findById(id);
    return taskOptional.orElse(null);
  }

  @Override
  public void deleteById(Long id) {
    taskRepository.deleteById(id);
    sheetsSynchronizer.synchronize();
  }

  @Override
  public void saveTask(Task task) {
    taskRepository.save(task);
    sheetsSynchronizer.synchronize();
  }

  @Override
  public Task getNextTask() {
    List<Task> nextTasks = getNextTasks();
    if (nextTasks == null) {
      return null;
    }
    return nextTasks.get(0);
  }

  @Override
  public List<Task> getNextTasks() {
    return taskRepository.findAllByDeadlineGreaterThanEqualOrderByDeadline(LocalDate.now());
  }

  @Override
  public List<Task> getAllTasks() {
    return taskRepository.findAllByOrderByDeadlineAsc();
  }

  @Override
  public List<Task> getTasksBetween(LocalDate after, LocalDate before) {
    return taskRepository.findByDeadlineBetweenOrderByDeadline(after, before);
  }
}
