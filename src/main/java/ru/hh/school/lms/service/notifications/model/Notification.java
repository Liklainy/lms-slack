package ru.hh.school.lms.service.notifications.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "notification")
public class Notification {

  @Id
  @Column(name = "notification_id")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long notificationId;

  @Column(name = "date_time")
  private LocalDateTime dateTime;
  @Column(name = "text")
  private String text;
  @Column(name = "user_id")
  private String userId;
  @Column(name = "attachments")
  private String attachments;

  public Notification() {
  }

  public Notification(LocalDateTime dateTime, String text, String userId, String attachments) {
    this.dateTime = dateTime;
    this.text = text;
    this.userId = userId;
    this.attachments = attachments;
  }

  public Long getNotificationId() {
    return notificationId;
  }

  public void setNotificationId(Long notificationId) {
    this.notificationId = notificationId;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getAttachments() {
    return attachments;
  }

  public void setAttachments(String attachments) {
    this.attachments = attachments;
  }

  @Override
  public String toString() {
    return "Notification{" +
        "dateTime=" + dateTime +
        ", text='" + text + '\'' +
        ", userId='" + userId + '\'' +
        ", attachments='" + attachments + '\'' +
        '}';
  }
}
