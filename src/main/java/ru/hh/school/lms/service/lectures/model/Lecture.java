package ru.hh.school.lms.service.lectures.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.hh.school.lms.service.lectures.LectureDtoFieldName;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.util.DateUtilsLms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "lecture")
public class Lecture {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(value = LectureDtoFieldName.ID)
  private Long id;

  @JsonFormat(pattern = DateUtilsLms.DATE_PATTERN)
  @JsonProperty(value = LectureDtoFieldName.DATE)
  private LocalDate date;

  @JsonProperty(value = LectureDtoFieldName.TITLE)
  private String title;

  @Column(name = "lecturer_id")
  @JsonProperty(value = LectureDtoFieldName.LECTURER_ID)
  private Long lecturerId;

  @JsonProperty(value = LectureDtoFieldName.SPECIALIZATION)
  private String specialization;

  @JsonProperty(value = LectureDtoFieldName.HOMEWORK)
  private boolean homework;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "lecturer_id", referencedColumnName = "id", insertable = false, updatable = false)
  private Person lecturer;

  Lecture() {
  }

  public Lecture(LocalDate date, String title, Long lecturerId, String specialization, boolean homework) {
    this.date = date;
    this.title = title;
    this.lecturerId = lecturerId;
    this.specialization = specialization;
    this.homework = homework;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getLecturerId() {
    return lecturerId;
  }

  public void setLecturerId(Long lecturerId) {
    this.lecturerId = lecturerId;
  }

  public String getSpecialization() {
    return specialization;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }

  public boolean isHomework() {
    return homework;
  }

  public void setHomework(boolean homework) {
    this.homework = homework;
  }

  public Person getLecturer() {
    return lecturer;
  }

  public void setLecturer(Person lecturer) {
    this.lecturer = lecturer;
  }

  @Override
  public String toString() {
    // TODO: 05.03.2019 сделать так, что если завтра лекция, он в качестве даты выводил "завтра", а не тупо дд/ММ/гггг
    String dateStr = DateUtilsLms.dateFormat(date);
    String info = String
      .format("%s | %s | %s | %s", dateStr, title, lecturer.toString(), specialization);
    if (homework) {
      info += " | ДЗ будет";
    }
    return info;
  }
}
