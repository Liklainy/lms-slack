package ru.hh.school.lms.service.common;

public enum ManagementCrudAction implements Identifiable {
  CREATE("button_create"),
  UPDATE("button_update"),
  DELETE("button_delete");

  private final String bundleKey; // TODO Is it good practice?

  ManagementCrudAction(String bundleKey) {
    this.bundleKey = bundleKey;
  }

  public String getBundleKey() {
    return bundleKey;
  }

  public static String getName() {
    return ManagementCrudAction.class.getCanonicalName();
  }
}
