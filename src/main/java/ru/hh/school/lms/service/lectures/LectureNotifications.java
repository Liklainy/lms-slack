package ru.hh.school.lms.service.lectures;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.common.json.GsonFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.notifications.model.Notification;
import ru.hh.school.lms.service.notifications.service.NotificationService;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
class LectureNotifications {

  private final LectureService lectureService;
  private final PersonService personService;
  private final NotificationService notificationService;
  private final MessageSource messages;
  private final Locale locale;

  public LectureNotifications(LectureService lectureService, PersonService personService,
                              NotificationService notificationService,
                              @Qualifier("lecturesMessageSource") MessageSource messages, Locale locale) {
    this.lectureService = lectureService;
    this.personService = personService;
    this.notificationService = notificationService;
    this.messages = messages;
    this.locale = locale;
  }

  @Scheduled(cron="0 30 10 * * FRI")
  private void reportNextWeekLectures() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.MONDAY);
    calendar.add(Calendar.DAY_OF_WEEK, 7);
    LocalDate after = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.SUNDAY);
    LocalDate before = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    List<Lecture> lectures = lectureService.getLecturesBetween(after, before);
    if (lectures.isEmpty()) {
      return;
    }
    List<Attachment> attachments = getListLecturesAttachment(lectures);
    Iterable<Person> pupils = personService.findAllWithRole("STUDENT");
    LocalDateTime dateTimeNow = LocalDateTime.now();
    for (Person pupil : pupils) {
      Long notificationId = notificationService.setNotification(new Notification(
        dateTimeNow.plusSeconds(20),
        messages.getMessage("pupil_next_week_lectures", null, locale),
        pupil.getSlackAccountId(),
        GsonFactory.createSnakeCase().toJson(attachments))
      );
    }
    for (Lecture lecture : lectures) {
      Long notificationId = notificationService.setNotification(new Notification(
        dateTimeNow.plusSeconds(20),
        messages.getMessage("lecturer_next_week_lectures", null, locale),
        lecture.getLecturer().getSlackAccountId(),
        GsonFactory.createSnakeCase().toJson(getListLecturesAttachment(List.of(lecture)))
      ));
    }
  }

  @Scheduled(cron = "0 30 10 * * MON")
  private void reportTwoWeekLaterLectures() {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.MONDAY);
    calendar.add(Calendar.DAY_OF_WEEK, 14);
    LocalDate after = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    calendar.set(Calendar.DAY_OF_WEEK, calendar.SUNDAY);
    LocalDate before = new Timestamp(calendar.getTimeInMillis()).toLocalDateTime().toLocalDate();
    List<Lecture> lectures = lectureService.getLecturesBetween(after, before);
    if (lectures.isEmpty()) {
      return;
    }
    LocalDateTime dateTimeNow = LocalDateTime.now();
    for (Lecture lecture : lectures) {
      Long notificationId = notificationService.setNotification(new Notification(
        dateTimeNow.plusSeconds(20),
        messages.getMessage("lecturer_two_week_later_lectures", null, locale),
        lecture.getLecturer().getSlackAccountId(),
        GsonFactory.createSnakeCase().toJson(getListLecturesAttachment(List.of(lecture)))
      ));
    }
    List<Attachment> attachments = getListLecturesAttachment(lectures);
    Iterable<Person> admins = personService.findAllWithRole("ADMIN");
    for (Person admin : admins) {
      Long notificationId = notificationService.setNotification(new Notification(
        dateTimeNow.plusSeconds(40),
        messages.getMessage("admin_two_week_later_lectures", null, locale),
        admin.getSlackAccountId(),
        GsonFactory.createSnakeCase().toJson(attachments)
      ));
    }
  }

  private List<Attachment> getListLecturesAttachment(List<Lecture> lectures) {
    List<Attachment> attachments = lectures.stream()
      .map(lecture -> Attachment.builder()
        .title(lecture.getTitle())
        .text(String.format("Дата проведения: %s | Лектор: %s | \nСпециализация: %s",
          lecture.getDate(), lecture.getLecturer().toString(), lecture.getSpecialization()))
        .build())
      .collect(Collectors.toList());
    return attachments;
  }
}
