package ru.hh.school.lms.service.grading.handler.data;

import com.google.gson.annotations.SerializedName;

public class SubmissionData {
  @SerializedName("github_url")
  public String githubUrl;
  @SerializedName("description")
  public String description;
}
