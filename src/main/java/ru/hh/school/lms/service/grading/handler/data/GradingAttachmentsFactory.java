package ru.hh.school.lms.service.grading.handler.data;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Option;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogElement;
import com.github.seratch.jslack.api.model.dialog.DialogSubType;
import com.github.seratch.jslack.api.model.dialog.DialogTextAreaElement;
import com.github.seratch.jslack.api.model.dialog.DialogTextElement;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.grading.dto.SubmissionDto;
import ru.hh.school.lms.service.grading.services.GradingService;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;

import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GradingAttachmentsFactory {

  private static final String SELECT_HOMEWORK_NAME = "select_homework";
  public static final String BUTTON_SUBMIT_VALUE = "submit";
  public static final String BUTTON_SELECT_AGAIN_VALUE = "select_again";


  private final TaskService taskService;
  private final GradingService gradingService;
  private final MessageSource messages;
  private final Locale locale;

  public GradingAttachmentsFactory(
    TaskService taskService,
    GradingService gradingService,
    @Qualifier("gradingMessageSource") MessageSource messages,
    Locale locale
  ) {
    this.taskService = taskService;
    this.gradingService = gradingService;
    this.messages = messages;
    this.locale = locale;
  }

  private Attachment getListIsEmpty() {
    return Attachment.builder()
      .text(messages.getMessage("empty_list", null, locale))
      .build();
  }

  public Attachment getSelectHomework(String callbackId) {
    List<Option> options = taskService.getAllTasks().stream()
      .map(x -> Option.builder()
        .text(getTaskName(x))
        .value(String.valueOf(x.getId()))
        .build())
      .collect(Collectors.toList());
    List<Action> actions = List.of(
      Action.builder()
        .name(SELECT_HOMEWORK_NAME)
        .type(Action.Type.SELECT)
        .options(options)
        .build()
    );
    return Attachment.builder()
      .text(messages.getMessage("select_homework_to_show_submissions_label", null, locale))
      .actions(actions)
      .callbackId(callbackId)
      .build();
  }

  public Attachment getSelectHomeworkNotSubmitted(Long userId, String callbackId) {
    List<Task> tasks = getUnsubmittedTasks(userId);
    List<Option> options = tasks.stream()
      .map(x -> Option.builder()
        .text(getTaskName(x))
        .value(String.valueOf(x.getId()))
        .build())
      .collect(Collectors.toList());
    List<Action> actions = List.of(
      Action.builder()
        .name(SELECT_HOMEWORK_NAME)
        .type(Action.Type.SELECT)
        .options(options)
        .build()
    );
    return Attachment.builder()
      .text(messages.getMessage("select_homework_to_submit_label", null, locale))
      .actions(actions)
      .callbackId(callbackId)
      .build();
  }

  public Attachment getSelectedHomeworkAttachment(String callbackId, Long selectedHomeworkId) {
    List<Action> actions = List.of(
      Action.builder()
        .name(BUTTON_SUBMIT_VALUE)
        .value(BUTTON_SUBMIT_VALUE)
        .text(messages.getMessage("selected_submit_button_label", null, locale))
        .type(Action.Type.BUTTON)
        .build(),
      Action.builder()
        .name(BUTTON_SELECT_AGAIN_VALUE)
        .value(BUTTON_SELECT_AGAIN_VALUE)
        .text(messages.getMessage("selected_again_button_label", null, locale))
        .type(Action.Type.BUTTON)
        .build()
    );
    return Attachment.builder()
      .text(messages.getMessage("selected_homework_label",
        new Object[]{getTaskNameById(selectedHomeworkId)}, locale))
      .actions(actions)
      .callbackId(callbackId)
      .build();
  }


  public Attachment[] getSubmissions(List<SubmissionDto> submissions) {
    Attachment[] attachments = submissions.stream()
      .map(x -> Attachment.builder()
        .title(messages.getMessage("get_submissions_format",
          new Object[]{getTaskNameById(x.getHomeworkId())}, locale))
        .titleLink(x.getGithubUrl())
        .text(x.getDescription())
        .build()).toArray(Attachment[]::new);
    if (ArrayUtils.isEmpty(attachments)) {
      attachments = new Attachment[]{getListIsEmpty()};
    }
    return attachments;
  }

  public Dialog getSubmissionDialog(String callbackId) throws NoSuchFieldException {
    List<DialogElement> elements = List.of(
      DialogTextElement.builder()
        .name(SubmissionData.class.getField("githubUrl").getAnnotation(SerializedName.class).value())
        .label(messages.getMessage("submit_field_github_url_label", null, locale))
        .subtype(DialogSubType.URL)
        .placeholder(messages.getMessage("submit_field_github_url_placeholder", null, locale))
        .hint(messages.getMessage("submit_field_github_url_hint", null, locale))
        .maxLength(150)
        .build(),
      DialogTextAreaElement.builder()
        .name(SubmissionData.class.getField("description").getAnnotation(SerializedName.class).value())
        .label(messages.getMessage("submit_field_description_label", null, locale))
        .optional(true)
        .placeholder(messages.getMessage("submit_field_description_placeholder", null, locale))
        .hint(messages.getMessage("submit_field_description_hint", null, locale))
        .maxLength(3000)
        .build()
    );
    return Dialog.builder()
      .callbackId(callbackId)
      .submitLabel(messages.getMessage("submit_homework_label", null, locale))
      .title(messages.getMessage("submit_homework_title", null, locale))
      .elements(elements)
      .build();
  }

  public String getTaskNameById(Long homeworkId) {
    Task task = taskService.findById(homeworkId);
    return getTaskName(task);
  }

  private String getTaskName(Task task) {
    return messages.getMessage("task_format",
      new Object[]{
        task.getLecture().getTitle(),
        task.getDescription()
      },
      locale
    );
  }

  private List<Task> getUnsubmittedTasks(Long userId) {
    List<Task> tasks = taskService.getAllTasks();
    List<SubmissionDto> submissions = gradingService.getSubmissionsByStudent(userId);
    Set<Long> submittedTasks =
      submissions.stream()
        .map(SubmissionDto::getHomeworkId)
        .collect(Collectors.toSet());
    return tasks.stream()
      .filter(t -> !submittedTasks.contains(t.getId()))
      .collect(Collectors.toList());
  }
}
