package ru.hh.school.lms.service.tasks.handler;

import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.util.Locale;

@Component
final class DeleteTaskDialogHandler implements DialogSubmissionHandler {

  private final TaskService taskService;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;

  @Autowired
  public DeleteTaskDialogHandler(TaskService taskService, SlackHelper slackHelper,
                                 @Qualifier("taskMessageSource") MessageSource messages, Locale locale) {
    this.taskService = taskService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    Long taskToDeleteId = event.getSubmission().get(this.getClass().getCanonicalName()).getAsLong();
    Task taskToDelete = taskService.findById(taskToDeleteId);
    taskService.deleteById(taskToDeleteId);
    if (taskToDelete != null) {
      slackHelper.respond(
        event,
        "",
        Attachment.builder()
          .text(messages.getMessage("delete_success", new Task[]{taskToDelete}, locale))
          .build()
      );
    }
  }
}
