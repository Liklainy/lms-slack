package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.PersonRepository;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;

import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

@Component
final class UpdateDialogHandler implements DialogSubmissionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final PersonRepository personRepository;
  private final SlackHelper slackHelper;
  private final Gson gson;

  @Autowired
  public UpdateDialogHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale, PersonRepository personRepository,
                             SlackHelper slackHelper, Gson gson) {
    this.messages = messages;
    this.locale = locale;
    this.personRepository = personRepository;
    this.slackHelper = slackHelper;
    this.gson = gson;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    UpdateDialogState state = gson.fromJson(event.getState(), UpdateDialogState.class);
    Optional<Person> optionalPerson = personRepository.findById(state.getLmsEntityId());
    if (!optionalPerson.isPresent()) {
      throw new EventHandlingException(event, "There is no person with id=" + state.getLmsEntityId());
    }
    Person person = optionalPerson.get();
    String oldPersonString = person.toString();
    PersonDialogData data = gson.fromJson(event.getSubmission(), PersonDialogData.class);
    data.updatePerson(person);
    personRepository.save(person);
    Message message = slackHelper.getSingleMessage(event, state.getChannel(), state.getTs());
    Attachment attachment = message.getAttachments().get(0);
    attachment.setActions(Collections.emptyList());
    attachment.setText(attachment.getText() + "\n" +
      messages.getMessage("update_success", new Object[]{oldPersonString, person.toString()}, locale));
    slackHelper.update(event, state.getChannel(), state.getTs(), null, attachment);
  }

  @Override
  public boolean test(DialogSubmissionEvent event) {
    return ManagementCrudAction.UPDATE.getId().equals(event.getCallbackId());
  }
}
