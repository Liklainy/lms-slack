package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.Collections;
import java.util.Locale;

@Component
final class ListAllInteractionHandler implements MessageInteractionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final PersonService personService;
  private final SlackHelper slackHelper;

  @Autowired
  public ListAllInteractionHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale,
                                   PersonService personService, SlackHelper slackHelper) {
    this.messages = messages;
    this.locale = locale;
    this.personService = personService;
    this.slackHelper = slackHelper;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    try {
      Action action = event.getActions().get(0);
      Iterable<Person> persons = personService.findAllWithRole(action.getValue());
      StringBuilder msg = new StringBuilder();
      for (Person person : persons) {
        msg.append(person.toString()).append("\n");
      }
      Message originalMessage = event.getOriginalMessage();
      String originalText = originalMessage.getText();
      Attachment originalAttachment = originalMessage.getAttachments().get(0);
      originalAttachment.setText(originalAttachment.getText() + "\n" +
        messages.getMessage("read_selected_role", new String[]{action.getValue() + "\n" + msg.toString()}, locale));
      originalAttachment.setActions(Collections.emptyList());
      slackHelper.update(event, originalText, originalAttachment);
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has one action and one attachment");
    }
  }

  @Override
  public boolean test(MessageInteractionEvent event) {
    return Command.LIST.getId().equals(event.getCallbackId());
  }
}
