package ru.hh.school.lms.service.grading.services;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.grading.dao.GradingDao;
import ru.hh.school.lms.service.grading.dto.ReviewDto;
import ru.hh.school.lms.service.grading.dto.SubmissionDto;
import ru.hh.school.lms.service.grading.entities.Review;
import ru.hh.school.lms.service.grading.entities.Submission;

import java.lang.reflect.Type;
import java.util.List;

@Component
public class GradingService {

  private final ModelMapper mapper;

  private final GradingDao gradingDao;

  @Autowired
  public GradingService(
    ModelMapper mapper,
    GradingDao gradingDao
  ) {
    this.mapper = mapper;
    this.gradingDao = gradingDao;
  }

  public SubmissionDto submitHomework(Long studentId, Long homeworkId, String githubUrl, String description) {
    Submission entity = new Submission();
    entity.setStudentId(studentId);
    entity.setHomeworkId(homeworkId);
    entity.setGithubUrl(githubUrl);
    entity.setDescription(description);

    gradingDao.addSubmission(entity);

    return mapper.map(entity, SubmissionDto.class);
  }

  public List<SubmissionDto> getSubmissions(Long homeworkId) {
    List<Submission> entities = gradingDao.getSubmissions(homeworkId);
    Type targetListType = new TypeToken<List<SubmissionDto>>() {
    }.getType();
    return mapper.map(entities, targetListType);
  }

  public List<SubmissionDto> getSubmissionsByStudent(Long studentId) {
    List<Submission> entities = gradingDao.getSubmissionsByStudent(studentId);
    Type targetListType = new TypeToken<List<SubmissionDto>>() {
    }.getType();
    return mapper.map(entities, targetListType);
  }

  public SubmissionDto getSubmission(Long studentId, Long homeworkId) {
    Submission entity = gradingDao.getSubmission(studentId, homeworkId);
    return mapper.map(entity, SubmissionDto.class);
  }

  public void updateSubmission(SubmissionDto submission) {
    Submission entity = mapper.map(submission, Submission.class);
    gradingDao.updateSubmission(entity);
  }

  public void deleteSubmission(Long submissionId) {
    gradingDao.deleteSubmission(submissionId);
  }

  public List<SubmissionDto> getUnreviewedSubmissions(Long homeworkId) {
    List<Submission> entities = gradingDao.getUnreviewedSubmissions(homeworkId);
    Type targetListType = new TypeToken<List<SubmissionDto>>() {
    }.getType();
    return mapper.map(entities, targetListType);
  }

  public List<SubmissionDto> getUngradedSubmissions(Long homeworkId) {
    List<Submission> entities = gradingDao.getUngradedSubmissions(homeworkId);
    Type targetListType = new TypeToken<List<SubmissionDto>>() {
    }.getType();
    return mapper.map(entities, targetListType);
  }

  public ReviewDto reviewSubmission(Long submissionId, Double grade, String content) {
    Review entity = new Review();
    entity.setSubmissionId(submissionId);
    entity.setGrade(grade);
    entity.setContent(content);

    gradingDao.addReview(entity);

    return mapper.map(entity, ReviewDto.class);
  }

  public void updateReview(ReviewDto review) {
    Review entity = mapper.map(review, Review.class);
    gradingDao.updateReview(entity);
  }

  public void deleteReview(Long reviewId) {
    gradingDao.deleteReview(reviewId);
  }
}
