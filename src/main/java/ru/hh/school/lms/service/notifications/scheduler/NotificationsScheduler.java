package ru.hh.school.lms.service.notifications.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.notifications.service.NotificationService;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
class NotificationsScheduler {

  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsScheduler.class);

  private final static long FIXED_RATE = 20 * 1000;
  private final static long INITIAL_DELAY = 15 * 1000;

  private final NotificationService notificationService;

  @Autowired
  private NotificationsScheduler(NotificationService notificationService) {
    this.notificationService = notificationService;
  }

  @Scheduled(fixedRate = FIXED_RATE, initialDelay = INITIAL_DELAY)
  public void reportCurrentTime() {
    Long currentTime = System.currentTimeMillis();
    LocalDateTime nextDateToNotify = notificationService.getNextNotificationDate();
    if (nextDateToNotify == null) {
      notificationService.setNextNotificationDate();
    } else if (nextDateToNotify.isBefore(new Timestamp(currentTime).toLocalDateTime())) {
      notificationService.postAndDeleteExpiredNotifications(new Timestamp(currentTime).toLocalDateTime());
      notificationService.setNextNotificationDate();
    }
  }
}
