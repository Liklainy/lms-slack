package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Option;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackEventHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;
import ru.hh.school.lms.util.Colors;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static ru.hh.school.lms.service.lectures.handler.ManageLectureMessagePostedHandler.CrudLectureAction;

@Component
final class ManageLectureInteractionHandler implements MessageInteractionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;
  private final LectureService lectureService;
  private final PersonService personService;
  private final CommonHandleMethods commonMethods;

  @Autowired
  public ManageLectureInteractionHandler(LectureService lectureService,
                                         SlackHelper slackHelper,
                                         @Qualifier("lecturesMessageSource") MessageSource messages, Locale locale,
                                         PersonService personService,
                                         CommonHandleMethods commonMethods) {
    this.personService = personService;
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
    this.lectureService = lectureService;
    this.commonMethods = commonMethods;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    // if buttons messages not deleted, it will track them
    if (CommonHandleMethods.getButtonsMessageLastTimestamp() == null) {
      CommonHandleMethods.setButtonsMessageLastTimestamp(event.getOriginalMessage().getTs());
    }
    try {
      Action action = event.getActions().get(0);
      String actionValue = action.getValue();
      if (actionValue.equals(CrudLectureAction.DELETE.getValue())) {
        delete(event);
      } else if (actionValue.equals(CrudLectureAction.CREATE.getValue())) {
        create(event);
      } else if (actionValue.equals(CrudLectureAction.UPDATE.getValue())) {
        update(event);
      }
    } catch (IOException | SlackApiException exc) {
      throw new EventHandlingException(event, exc);
    }
  }

  private void create(MessageInteractionEvent event) throws EventHandlingException {
    Dialog dialog = commonMethods.lectureCreateDialog(
      messages.getMessage("create_dialog_title", null, locale),
      personService.findAllWithRole("LECTURER")
    );
    slackHelper.dialog(event, AddLectureDialogSubmissionHandler.class, dialog);
  }

  private void update(MessageInteractionEvent event) throws IOException, SlackApiException, EventHandlingException {
    changeLectures(
      event,
      messages.getMessage("update_dialog_title", null, locale),
      UpdateLectureInteractionHandler.class
    );
  }

  private void delete(MessageInteractionEvent event) throws IOException, SlackApiException, EventHandlingException {
    changeLectures(
      event,
      messages.getMessage("delete_label", null, locale),
      DeleteLectureInteractionHandler.class
    );
  }

  private void changeLectures(MessageInteractionEvent event,
                              String attachmentText,
                              Class<? extends CallbackEventHandler> handlerClass) throws IOException, EventHandlingException {
    // some unique value
    String actionName = handlerClass.getCanonicalName();

    Iterable<Lecture> lectures = lectureService.getAllLectures();
    List<Option> options = new ArrayList<>();
    for (Lecture lecture : lectures) {
      options.add(Option.builder()
        .text(lecture.toString())
        .value(String.valueOf(lecture.getId()))
        .build());
    }
    slackHelper.respond(
      event,
      "",
      handlerClass,
      Attachment.builder()
        .text(attachmentText)
        .color(Colors.hex(Color.WHITE))
        .actions(List.of(
          Action.builder()
            .name(actionName)
            .text(messages.getMessage("select_placeholder", null, locale))
            .type(Action.Type.SELECT)
            .options(options)
            .build()
        ))
        .build()
    );
  }
}
