package ru.hh.school.lms.service.notifications.service;

import ru.hh.school.lms.service.notifications.model.Notification;

import java.time.LocalDateTime;

public interface NotificationService {

  void setNextNotificationDate();

  LocalDateTime getNextNotificationDate();

  Long setNotification(Notification notification);

  void removeNotification(Long notificationId);

  void postAndDeleteExpiredNotifications(LocalDateTime dateTime);
}
