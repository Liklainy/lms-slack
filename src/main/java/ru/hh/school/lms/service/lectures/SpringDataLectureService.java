package ru.hh.school.lms.service.lectures;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.repository.LectureRepository;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.sheets.GoogleSheetsSynchronizer;
import ru.hh.school.lms.sheets.GoogleSpreadsheetRelation;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
class SpringDataLectureService implements LectureService {

  private final LectureRepository lectureRepository;
  private final GoogleSheetsSynchronizer sheetsSynchronizer;
  private final GoogleSpreadsheetRelation spreadsheetRelation;

  @Autowired
  public SpringDataLectureService(LectureRepository lectureRepository,
                                  GoogleSheetsSynchronizer sheetsSynchronizer,
                                  @Qualifier("lecturesMessageSource") GoogleSpreadsheetRelation spreadsheetRelation) {
    this.lectureRepository = lectureRepository;
    this.sheetsSynchronizer = sheetsSynchronizer;
    this.spreadsheetRelation = spreadsheetRelation;
  }

  @Override
  public String getSheetUrl() {
    return spreadsheetRelation.getSpreadsheetUrl();
  }

  @Override
  public Optional<Lecture> findById(Long id) {
    return lectureRepository.findById(id);
  }

  @Override
  public void deleteById(Long id) {
    lectureRepository.deleteById(id);
    sheetsSynchronizer.synchronize();
  }

  @Override
  public void saveLecture(Lecture newLecture) {
    lectureRepository.save(newLecture);
    sheetsSynchronizer.synchronize();
  }

  @Override
  public Lecture getNextLecture() {
    List<Lecture> nextLectures = getNextLectures();
    if (nextLectures == null) {
      return null;
    }
    return nextLectures.get(0);
  }

  @Override
  public List<Lecture> getNextLectures() {
    return lectureRepository.findAllByDateGreaterThanEqualOrderByDate(LocalDate.now());
  }

  @Override
  public List<Lecture> getAllLectures() {
    return lectureRepository.findAllByOrderByDateAsc();
  }

  @Override
  public List<Lecture> getLecturesBetween(LocalDate after, LocalDate before) {
    return lectureRepository.findByDateBetweenOrderByDate(after, before);
  }
}
