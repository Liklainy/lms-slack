package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
final class ManageLectureMessagePostedHandler implements MessagePostedHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;

  public ManageLectureMessagePostedHandler(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale,
                                           SlackHelper slackHelper) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
  }

  enum CrudLectureAction {
    CREATE("Создать", "create_action"),
    UPDATE("Изменить", "update_action"),
    DELETE("Удалить", "delete_action");

    private final String name;
    private final String value;

    CrudLectureAction(String name, String value) {
      this.name = name;
      this.value = value;
    }

    public String getName() {
      return name;
    }

    public String getValue() {
      return value;
    }
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    // delete buttons after next "lecture edit" command
    String prevTs = CommonHandleMethods.getButtonsMessageLastTimestamp();
    if (prevTs != null) {
      List<Message> prevMessages = slackHelper.getLatestMessages(event, event.getChannel().getId(), prevTs);

      for (Message message : prevMessages) {
        if (message.getAttachments() != null && message.getAttachments().get(0).getActions() != null) {
          slackHelper.update(event,
            event.getChannel().getId(),
            message.getTs(),
            "",
            Attachment.builder().text(messages.getMessage("session_closed", null, locale)).build());
        }
      }
    }
    prevTs = event.getTs();
    CommonHandleMethods.setButtonsMessageLastTimestamp(prevTs);


    List<Action> managementActions = new ArrayList<>();
    List<CrudLectureAction> actions = List.of(
      CrudLectureAction.CREATE,
      CrudLectureAction.DELETE,
      CrudLectureAction.UPDATE
    );
    for (CrudLectureAction action : actions) {
      managementActions.add(Action.builder()
        .name(action.getName())
        .text(action.getName())
        .type(Action.Type.BUTTON)
        .value(action.getValue())
        .build());
    }
    slackHelper.respond(
      event,
      messages.getMessage("choose_action_message", null, locale),
      ManageLectureInteractionHandler.class,
      Attachment.builder()
        .text(messages.getMessage("choose_action_attachment", null, locale))
        .actions(managementActions)
        .build()
    );
  }

  @Override
  public boolean test(MessagePostedEvent messagePostedEvent) {
    return messages.getMessage("lecture_edit", null, locale).equals(messagePostedEvent.getText());
  }
}
