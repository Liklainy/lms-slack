package ru.hh.school.lms.service.tasks.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
final class TaskMessagePostedHandler implements MessagePostedHandler {

  private final TaskService taskService;
  private final SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;

  @Autowired
  TaskMessagePostedHandler(TaskService taskService, SlackHelper slackHelper,
                           @Qualifier("taskMessageSource") MessageSource messages, Locale locale) {
    this.taskService = taskService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    String message = messages.getMessage("no_more_tasks", null, locale);
    if (event.getText().equals(messages.getMessage("next", null, locale))) {
      Task nextTask = taskService.getNextTask();
      if (nextTask != null) {
        message = nextTask.toString();
      }
    } else if (event.getText().equals(messages.getMessage("next_all", null, locale))) {
      List<Task> nextTasks = taskService.getNextTasks();
      if (nextTasks != null) {
        message = nextTasks.stream().map(Task::toString).collect(Collectors.joining("\n---------\n"));
      }
    } else if (event.getText().equals(messages.getMessage("sheets_url", null, locale))) {
      message = taskService.getSheetUrl();
    }
    slackHelper.respond(event, message);
  }

  @Override
  public boolean test(MessagePostedEvent event) {
    return messages.getMessage("next", null, locale).equals(event.getText())
      || messages.getMessage("next_all", null, locale).equals(event.getText())
      || messages.getMessage("sheets_url", null, locale).equals(event.getText());
  }

}
