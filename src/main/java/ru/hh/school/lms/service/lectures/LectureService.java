package ru.hh.school.lms.service.lectures;

import ru.hh.school.lms.service.lectures.model.Lecture;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LectureService {
  String getSheetUrl();

  Optional<Lecture> findById(Long id);

  void deleteById(Long id);

  void saveLecture(Lecture newLecture);

  Lecture getNextLecture();

  List<Lecture> getNextLectures();

  List<Lecture> getAllLectures();

  List<Lecture> getLecturesBetween(LocalDate after, LocalDate before);
}

