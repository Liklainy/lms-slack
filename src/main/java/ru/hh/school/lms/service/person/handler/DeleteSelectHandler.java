package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

@Component
class DeleteSelectHandler implements MessageInteractionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final PersonService personService;
  private final SlackHelper slackHelper;

  @Autowired
  public DeleteSelectHandler(@Qualifier("personMessageSource") MessageSource messageSource, Locale locale,
                             PersonService personService, SlackHelper slackHelper) {
    this.messages = messageSource;
    this.locale = locale;
    this.personService = personService;
    this.slackHelper = slackHelper;
  }

  @Transactional
  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    try {
      Long personToDeleteId = Long.valueOf(event.getActions().get(0)
        .getSelectedOptions().get(0)
        .getValue());
      Optional<Person> optionalPerson = personService.findByID(personToDeleteId); // TODO obtain from event object
      if (!optionalPerson.isPresent()) {
        throw new EventHandlingException(event); // TODO write message back with error.
      }
      personService.deleteById(personToDeleteId);
      Person deletedPerson = optionalPerson.get();

      Message originalMessage = event.getOriginalMessage();
      Attachment attachment = originalMessage.getAttachments().get(0);
      attachment.setText(attachment.getText() + "\n" + messages.getMessage("delete_success", new Person[]{deletedPerson}, locale));
      attachment.setActions(Collections.emptyList());
      slackHelper.update(event, originalMessage.getText(), attachment);
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has selected option and attachment", exc);
    }
  }

  @Override
  public boolean test(MessageInteractionEvent event) {
    return ManagementCrudAction.DELETE.getId().equals(event.getCallbackId());
  }
}
