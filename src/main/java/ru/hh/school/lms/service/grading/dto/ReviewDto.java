package ru.hh.school.lms.service.grading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ReviewDto {

  @JsonProperty("reviewId")
  private Long reviewId;
  @JsonProperty("submissionId")
  private Long submissionId;
  @JsonProperty("reviewerId")
  private Long reviewerId;
  @JsonProperty("grade")
  private Double grade;
  @JsonProperty("content")
  private String content;
  @JsonProperty("created")
  private Date created;
  @JsonProperty("updated")
  private Date updated;

  @JsonProperty("reviewId")
  public Long getReviewId() {
    return reviewId;
  }

  @JsonProperty("reviewId")
  public void setReviewId(Long reviewId) {
    this.reviewId = reviewId;
  }

  @JsonProperty("submissionId")
  public Long getSubmissionId() {
    return submissionId;
  }

  @JsonProperty("submissionId")
  public void setSubmissionId(Long submissionId) {
    this.submissionId = submissionId;
  }

  @JsonProperty("reviewerId")
  public Long getReviewerId() {
    return reviewerId;
  }

  @JsonProperty("reviewerId")
  public void setReviewerId(Long reviewerId) {
    this.reviewerId = reviewerId;
  }

  @JsonProperty("grade")
  public Double getGrade() {
    return grade;
  }

  @JsonProperty("grade")
  public void setGrade(Double grade) {
    this.grade = grade;
  }

  @JsonProperty("content")
  public String getContent() {
    return content;
  }

  @JsonProperty("content")
  public void setContent(String content) {
    this.content = content;
  }

  @JsonProperty("created")
  public Date getCreated() {
    return created;
  }

  @JsonProperty("created")
  public void setCreated(Date created) {
    this.created = created;
  }

  @JsonProperty("updated")
  public Date getUpdated() {
    return updated;
  }

  @JsonProperty("updated")
  public void setUpdatedd(Date updated) {
    this.updated = updated;
  }
}
