package ru.hh.school.lms.service.tasks;

public final class TaskDtoFieldName {
  public static final String ID = "task_id";
  public static final String LECTURE_ID = "lecture_id";
  public static final String VERIFIED_ID = "verifier_id";
  public static final String DEADLINE = "hard_deadline";
  public static final String TASK_URL = "task_url";
  public static final String DESCRIPTION = "description";
}
