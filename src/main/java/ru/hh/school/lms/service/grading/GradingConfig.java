package ru.hh.school.lms.service.grading;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import ru.hh.school.lms.repository.ReviewRepository;
import ru.hh.school.lms.repository.SubmissionRepository;
import ru.hh.school.lms.service.grading.dao.GradingDao;
import ru.hh.school.lms.service.grading.dao.SpringDataGradingDao;

import java.util.Locale;

@Configuration
public class GradingConfig {

  @Bean
  GradingDao homeworkGradingDao(SubmissionRepository submissionRepository, ReviewRepository reviewRepository) {
    return new SpringDataGradingDao(submissionRepository, reviewRepository);
  }

  @Bean(name = "gradingMessageSource")
  MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasenames("grading/messages", "grading/labels");
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

  @Bean
  Locale locale() {
    return new Locale("ru", "RU");
  }
}
