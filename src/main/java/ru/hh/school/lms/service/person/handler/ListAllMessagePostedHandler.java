package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.person.RoleService;
import ru.hh.school.lms.service.person.model.Role;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.github.seratch.jslack.api.model.Action.Type.BUTTON;

@Component
final class ListAllMessagePostedHandler implements MessagePostedHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;
  private final RoleService roleService;

  public ListAllMessagePostedHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale,
                                     SlackHelper slackHelper, RoleService roleService) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
    this.roleService = roleService;
  }

  private Action roleToAction(Role role) {
    return Action.builder()
      .name(role.getTitle())
      .text(role.getTitle())
      .type(BUTTON)
      .value(role.getTitle())
      .build();
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    List<Action> roleActions = roleService.findAll().stream().map(this::roleToAction).collect(Collectors.toList());
    Attachment attachment = Attachment.builder()
      .text(messages.getMessage("choose_role_attachment", null, locale))
      .callbackId(Command.LIST.getId())
      .actions(roleActions).build();
    slackHelper.respond(event, messages.getMessage("choose_role_message", null, locale), attachment);
  }

  @Override
  public boolean test(MessagePostedEvent event) {
    return messages.getMessage("command_list", null, locale).equals(event.getText());
  }
}
