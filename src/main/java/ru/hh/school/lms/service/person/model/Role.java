package ru.hh.school.lms.service.person.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  public String getTitle() {
    return title;
  }

  private String title;

  Role() {

  }

  @Override
  public String toString() {
    return title.toUpperCase();
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || getClass() != other.getClass()) {
      return false;
    }
    Role role = (Role) other;
    return Objects.equals(title, role.title);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title);
  }
}
