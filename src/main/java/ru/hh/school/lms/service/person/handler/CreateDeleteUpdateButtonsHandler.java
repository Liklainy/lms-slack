package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.Message;
import com.github.seratch.jslack.api.model.Option;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.Identifiable;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.github.seratch.jslack.api.model.Action.Type.SELECT;
import static ru.hh.school.lms.service.common.ManagementCrudAction.CREATE;
import static ru.hh.school.lms.service.common.ManagementCrudAction.DELETE;
import static ru.hh.school.lms.service.common.ManagementCrudAction.UPDATE;
import static ru.hh.school.lms.util.Colors.hex;

@Component
final class CreateDeleteUpdateButtonsHandler implements MessageInteractionHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final PersonService personService;
  private final SlackHelper slackHelper;

  @Autowired
  public CreateDeleteUpdateButtonsHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale,
                                          PersonService personService, SlackHelper slackHelper) {
    this.messages = messages;
    this.locale = locale;
    this.personService = personService;
    this.slackHelper = slackHelper;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    List<Action> actions = event.getActions(); // expected event is not null as it passed through test(Event event) method
    if (actions.isEmpty()) {
      throw new EventHandlingException(event, "Expected that message has at least one action");
    }
    String actionValue = actions.get(0).getValue();
    try {
      ManagementCrudAction action = Identifiable.ofId(actionValue, ManagementCrudAction.class);
      switch (action) {
        case DELETE:
          delete(event);
          break;
        case CREATE:
          create(event);
          break;
        case UPDATE:
          update(event);
          break;
        default:
          throw new EventHandlingException(event, "Unknown action type: " + action);
      }
    } catch (IllegalArgumentException exc) {
      throw new EventHandlingException(event, exc);
    }

  }

  @Override
  public boolean test(MessageInteractionEvent event) {
    return Command.MANAGEMENT.getId().equals(event.getCallbackId());
  }

  private List<Option> optionsFromPersons(Iterable<Person> personList) {
    List<Option> options = new ArrayList<>();
    for (Person person : personList) {
      options.add(Option.builder()
        .text(person.toString())
        .value(String.valueOf(person.getId()))
        .build());
    }
    return options;
  }

  private void create(MessageInteractionEvent event) throws EventHandlingException {
    List<DialogElement> elements = PersonDialogField.toDialogElements();
    Dialog dialog = Dialog.builder()
      .callbackId(CREATE.getId())
      .submitLabel(messages.getMessage("create_dialog_submit", null, locale))
      .title(messages.getMessage("create_dialog_title", null, locale))
      .elements(elements)
      .build();
    Message originalMessage = event.getOriginalMessage();
    String originalText = originalMessage.getText();
    Attachment attachment = originalMessage.getAttachments().get(0);
    String originalAttachmentText = attachment.getText();
    attachment.setText(originalAttachmentText + "\n" + messages.getMessage("create_label", null, locale));
    attachment.setActions(Collections.emptyList());
    slackHelper.update(event, originalText, attachment);
    slackHelper.dialog(event, dialog);
  }

  private void delete(MessageInteractionEvent event) throws EventHandlingException {
    try {
      List<Option> options = optionsFromPersons(personService.findAll());
      Message originalMessage = event.getOriginalMessage();
      String originalText = originalMessage.getText();
      Attachment originalAttachment = originalMessage.getAttachments().get(0);
      originalAttachment.setColor(hex(Color.RED));
      originalAttachment.setText(originalAttachment.getText() + "\n" + messages.getMessage("delete_label", null, locale));
      originalAttachment.setCallbackId(DELETE.getId());
      originalAttachment.setActions(
        List.of(
          Action.builder()
            .name(ManagementCrudAction.getName())
            .text(messages.getMessage("user_full_name", null, locale))
            .type(SELECT)
            .options(options)
            .build()
        )
      );
      slackHelper.update(event, originalText, originalAttachment);
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has one attachment");
    }

  }

  private void update(MessageInteractionEvent event) throws EventHandlingException {
    List<Option> options = optionsFromPersons(personService.findAll());
    Message originalMessage = event.getOriginalMessage();
    String originalText = originalMessage.getText();
    Attachment originalAttachment = originalMessage.getAttachments().get(0);
    Action personSelector = Action.builder()
      .name(ManagementCrudAction.getName())
      .text(messages.getMessage("user_full_name", null, locale))
      .type(SELECT)
      .options(options)
      .build();
    originalAttachment.setCallbackId(UPDATE.getId());
    originalAttachment.setText(originalAttachment.getText() + "\n" + messages.getMessage("update", null, locale));
    originalAttachment.setActions(List.of(personSelector));
    slackHelper.update(event, originalText, originalAttachment);
  }

}
