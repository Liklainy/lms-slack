package ru.hh.school.lms.service.tasks.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.tasks.TaskDtoFieldName;
import ru.hh.school.lms.util.DateUtilsLms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "task")
public class Task {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(value = TaskDtoFieldName.ID)
  private Long id;

  @Column(name = "lecture_id")
  @JsonProperty(value = TaskDtoFieldName.LECTURE_ID)
  private Long lectureId;

  @Column(name = "verifier_id")
  @JsonProperty(value = TaskDtoFieldName.VERIFIED_ID)
  private Long verifierId;

  @Column(name = "deadline")
  @JsonFormat(pattern = DateUtilsLms.DATE_PATTERN)
  @JsonProperty(value = TaskDtoFieldName.DEADLINE)
  private LocalDate deadline;

  @Column(name = "task_url")
  @JsonProperty(value = TaskDtoFieldName.TASK_URL)
  private String taskUrl;

  @Column(name = "description")
  @JsonProperty(value = TaskDtoFieldName.DESCRIPTION)
  private String description;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "verifier_id", referencedColumnName = "id", insertable = false, updatable = false)
  private Person person;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "lecture_id", referencedColumnName = "id", insertable = false, updatable = false)
  private Lecture lecture;

  public Task() {
  }

  public Task(Long lectureId,
              Long verifierId,
              LocalDate deadline,
              String taskUrl,
              String description) {
    this.lectureId = lectureId;
    this.verifierId = verifierId;
    this.deadline = deadline;
    this.taskUrl = taskUrl;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getLectureId() {
    return lectureId;
  }

  public void setLectureId(Long lectureId) {
    this.lectureId = lectureId;
  }

  public Long getVerifierId() {
    return verifierId;
  }

  public void setVerifierId(Long verifierId) {
    this.verifierId = verifierId;
  }

  public LocalDate getDeadline() {
    return deadline;
  }

  public void setDeadline(LocalDate deadline) {
    this.deadline = deadline;
  }

  public String getTaskUrl() {
    return taskUrl;
  }

  public void setTaskUrl(String taskUrl) {
    this.taskUrl = taskUrl;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Person getPerson() {
    return person;
  }

  public void setLecture(Lecture lecture) {
    this.lecture = lecture;
  }

  public Lecture getLecture() {
    return lecture;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public String getLabelToNotify() {
    String info = String
      .format("Сдать до %s | Проверяет: %s | Описание: %s",
        DateUtilsLms.dateFormat(deadline),
        person.toString(),
        description);
    return info;
  }

  public String getLabelToDelete() {
    String info = String
      .format("%s|%s",
        DateUtilsLms.dateFormat(deadline),
        lecture.getTitle());
    return info;
  }

  @Override
  public String toString() {
    String info = String
      .format("%s | %s | %s | %s | %s",
        lecture.getTitle(),
        person.toString(),
        DateUtilsLms.dateFormat(deadline),
        taskUrl,
        description);
    return info;
  }
}
