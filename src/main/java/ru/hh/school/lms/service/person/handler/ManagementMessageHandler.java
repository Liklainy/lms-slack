package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.Action;
import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.ManagementCrudAction;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.github.seratch.jslack.api.model.Action.Type.BUTTON;
import static ru.hh.school.lms.service.common.ManagementCrudAction.getName;

@Component
final class ManagementMessageHandler implements MessagePostedHandler {

  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;

  public ManagementMessageHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale, SlackHelper slackHelper) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
  }

  private Action toSlackAction(ManagementCrudAction action) {
    return Action.builder()
      .name(getName())
      .text(messages.getMessage(action.getBundleKey(), null, locale))
      .type(BUTTON)
      .value(action.getId())
      .build();
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    List<Action> actions = Arrays.stream(ManagementCrudAction.values())
      .map(this::toSlackAction).collect(Collectors.toList());
    Attachment attachment = Attachment.builder()
      .text(messages.getMessage("choose_action_attachment", null, locale))
      .callbackId(Command.MANAGEMENT.getId())
      .actions(actions)
      .build();
    slackHelper.respond(event, messages.getMessage("choose_action_message", null, locale), attachment);
  }

  @Override
  public boolean test(MessagePostedEvent event) {
    return messages.getMessage("command_management", null, locale).equals(event.getText());
  }
}
