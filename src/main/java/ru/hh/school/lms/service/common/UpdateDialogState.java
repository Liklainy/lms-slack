package ru.hh.school.lms.service.common;

public class UpdateDialogState {
  private Long lmsEntityId;
  private String channel;
  private String ts;

  public UpdateDialogState(Long lmsEntityId, String channel, String ts) {
    this.lmsEntityId = lmsEntityId;
    this.channel = channel;
    this.ts = ts;
  }

  public Long getLmsEntityId() {
    return lmsEntityId;
  }

  public String getTs() {
    return ts;
  }

  public String getChannel() {
    return channel;
  }
}
