package ru.hh.school.lms.service.grading.handler.message;

import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.grading.dto.SubmissionDto;
import ru.hh.school.lms.service.grading.handler.data.GradingAttachmentsFactory;
import ru.hh.school.lms.service.grading.services.GradingService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackStorage;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;

import java.util.List;
import java.util.Locale;

@Component
public class GradingGetMySubmissionsMessageHandler extends BaseGradingMessageHandler {

  @Autowired
  public GradingGetMySubmissionsMessageHandler(
    GradingService gradingService,
    SlackHelper client,
    CallbackStorage callbackStorage,
    GradingAttachmentsFactory gradingAttachmentsFactory,
    @Qualifier("gradingMessageSource") MessageSource messages,
    Locale locale
  ) {
    super(gradingService, client, callbackStorage, gradingAttachmentsFactory, messages, locale);
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    if (event.getContext().getSender().isEmpty()) {
      client.respond(event, messages.getMessage("not_available", null, locale));
      return;
    }
    Long userId = event.getContext().getSender().get().getId();

    List<SubmissionDto> submissions = gradingService.getSubmissionsByStudent(userId);
    Attachment[] attachments = gradingAttachmentsFactory.getSubmissions(submissions);
    client.respond(event, messages.getMessage("get_my_submissions_message", null, locale), attachments);
  }

  @Override
  String regExpKey() {
    return "get_my_submissions";
  }
}
