package ru.hh.school.lms.service.lectures;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.LectureRepository;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.sheets.GoogleSheetsClient;
import ru.hh.school.lms.sheets.GoogleSpreadsheetRelation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Qualifier("lecturesMessageSource")
@PropertySource(value = "classpath:spreadsheets.properties", encoding = "UTF-8")
final class LectureSpreadsheetRelation implements GoogleSpreadsheetRelation {
  private final List<Object> lectureSheetHeader = Arrays.asList("Дата", "Тема лекции", "Лектор", "Специализация", "ДЗ");
  private final LectureRepository lectureRepository;

  @Value("${lectures_spreadsheet_title}")
  private String lecturesSpreadsheetTitle;

  @Autowired
  public LectureSpreadsheetRelation(LectureRepository lectureRepository) {
    this.lectureRepository = lectureRepository;
  }

  @Override
  public String getSpreadsheetTitle() {
    return lecturesSpreadsheetTitle;
  }

  @Override
  public void mapDbToSpreadsheet(GoogleSheetsClient client, String spreadsheetId) throws IOException {
    List<List<Object>> response = new ArrayList<>(Collections.singletonList(lectureSheetHeader));
    List<List<Object>> lectureItems = lectureRepository.findAll()
      .stream()
      .sorted(Comparator.comparing(Lecture::getDate))
      .map(this::lectureToSheetRow)
      .collect(Collectors.toList());
    response.addAll(lectureItems);
    client.setSheet(
      spreadsheetId,
      "A1",
      response
    );
  }

  private List<Object> lectureToSheetRow(Lecture entity) {
    return List.of(
      entity.getDate().format(getDateTimeFormatter()),
      entity.getTitle(),
      entity.getLecturer().toString(),
      entity.getSpecialization(),
      entity.isHomework() ? "Дз будет" : ""
    );
  }
}
