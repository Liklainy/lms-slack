package ru.hh.school.lms.service.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.repository.PersonRepository;
import ru.hh.school.lms.repository.RoleRepository;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.service.person.model.Role;

import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
class SpringDataPersonService implements PersonService {

  private final PersonRepository personRepository;
  private final RoleRepository roleRepository;

  @Autowired
  SpringDataPersonService(PersonRepository personRepository, RoleRepository roleRepository) {
    this.personRepository = personRepository;
    this.roleRepository = roleRepository;
  }


  @Override
  public Iterable<Person> findAll() {
    return personRepository.findAll();
  }

  @Override
  public Iterable<Person> findAllWithRole(String roleTitle) {
    Optional<Role> role = roleRepository.findByTitle(roleTitle);
    if (role.isPresent()) {
      return personRepository.findAllByRolesContains(role.get());
    }
    return Collections.emptyList();
  }

  @Override
  public Optional<Person> findByID(Long id) {
    return personRepository.findById(id);
  }

  @Override
  public Optional<Person> findBySlackAccountId(String slackAccountId) {
    return personRepository.findBySlackAccountId(slackAccountId);
  }

  @Override
  public void deleteById(Long id) {
    personRepository.deleteById(id);
  }

}
