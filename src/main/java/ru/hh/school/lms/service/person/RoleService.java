package ru.hh.school.lms.service.person;

import ru.hh.school.lms.service.person.model.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {
  Optional<Role> findByTitle(String title);

  List<Role> findAll();
}
