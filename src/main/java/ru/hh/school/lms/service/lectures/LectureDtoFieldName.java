package ru.hh.school.lms.service.lectures;

public final class LectureDtoFieldName {
  public static final String ID = "lecture_id";
  public static final String DATE = "date";
  public static final String TITLE = "title";
  public static final String LECTURER_ID = "lecturer_id";
  public static final String SPECIALIZATION = "specialization";
  public static final String HOMEWORK = "homework";
}
