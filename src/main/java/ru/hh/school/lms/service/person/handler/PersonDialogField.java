package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.dialog.DialogElement;
import com.github.seratch.jslack.api.model.dialog.DialogTextElement;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.person.model.Person;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

enum PersonDialogField {
  FIRST_NAME,
  FAMILY_NAME,
  PATRONYMIC,
  SLACK_ACCOUNT_ID,
  GITHUB_ACCOUNT_LOGIN;

  private String label;
  private String placeholder;


  @Component
  static class PersonDialogFieldInjector {

    private final MessageSource messages;
    private final Locale locale;

    public PersonDialogFieldInjector(@Qualifier("personMessageSource") MessageSource messages, Locale locale) {
      this.messages = messages;
      this.locale = locale;
    }

    @PostConstruct
    public void postConstruct() {
      for (PersonDialogField field : PersonDialogField.values()) {
        field.label = messages.getMessage("dialog_label_" + field.name().toLowerCase(), null, locale);
        field.placeholder = messages.getMessage("dialog_placeholder_" + field.name().toLowerCase(), null, locale);
      }
    }
  }

  private static final int MAX_FIELD_LEN = 150;
  private static final int MIN_FIELD_LEN = 1;

  public static List<DialogElement> toDialogElements(Person person) {
    List<DialogElement> elements = new ArrayList<>(PersonDialogField.values().length);
    for (PersonDialogField field : PersonDialogField.values()) {
      elements.add(DialogTextElement.builder()
        .name(field.name().toLowerCase())
        .value(field.valueFromPerson(person))
        .label(field.label)
        .placeholder(field.placeholder)
        .maxLength(MAX_FIELD_LEN)
        .minLength(MIN_FIELD_LEN)
        .build());
    }
    return elements;
  }

  private String valueFromPerson(Person person) {
    if (person == null) {
      return "";
    }
    switch (this) {
      case FIRST_NAME:
        return person.getFirstName();
      case FAMILY_NAME:
        return person.getFamilyName();
      case PATRONYMIC:
        return person.getPatronymic();
      case SLACK_ACCOUNT_ID:
        return person.getSlackAccountId();
      case GITHUB_ACCOUNT_LOGIN:
        return person.getGithubAccountLogin();
      default:
        return "";
    }
  }

  public static List<DialogElement> toDialogElements() {
    return toDialogElements(null);
  }
}
