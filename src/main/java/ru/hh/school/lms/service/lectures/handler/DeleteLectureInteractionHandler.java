package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.Locale;
import java.util.Optional;

@Component
final class DeleteLectureInteractionHandler implements MessageInteractionHandler {
  private final MessageSource messages;
  private final Locale locale;
  private final SlackHelper slackHelper;
  private final LectureService lectureService;

  public DeleteLectureInteractionHandler(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale,
                                         SlackHelper slackHelper, LectureService lectureService) {
    this.messages = messages;
    this.locale = locale;
    this.slackHelper = slackHelper;
    this.lectureService = lectureService;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    Long id = Long.valueOf(
      event.getActions().get(0)
        .getSelectedOptions().get(0)
        .getValue()
    );
    Optional<Lecture> lectureToDeleteOpt = lectureService.findById(id);
    String message = messages.getMessage("not_found", null, locale);
    lectureService.deleteById(id);
    if (lectureToDeleteOpt.isPresent()) {
      message = messages.getMessage("delete_success", new Lecture[]{lectureToDeleteOpt.get()}, locale);
    }

    slackHelper.update(
      event,
      event.getOriginalMessage().getText(),
      Attachment.builder()
        .text(message)
        .build()
    );
  }
}
