package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.model.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;
import ru.hh.school.lms.slack.event.MessagePostedHandler;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
final class LectureMessagePostedHandler implements MessagePostedHandler {

  private LectureService lectureService;
  private SlackHelper slackHelper;
  private final MessageSource messages;
  private final Locale locale;

  @Autowired
  public LectureMessagePostedHandler(@Qualifier("lecturesMessageSource") MessageSource messages, Locale locale,
                                     LectureService lectureService, SlackHelper slackHelper) {
    this.messages = messages;
    this.locale = locale;
    this.lectureService = lectureService;
    this.slackHelper = slackHelper;
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    String message = messages.getMessage("no_more_lectures", null, locale);
    if (event.getText().equals(messages.getMessage("next", null, locale))) {
      Lecture nextLecture = lectureService.getNextLecture();
      if (nextLecture != null) {
        message = nextLecture.toString();
      }
    } else if (event.getText().equals(messages.getMessage("next_all", null, locale))) {
      List<Lecture> nextLectures = lectureService.getNextLectures();
      if (nextLectures != null) {
        message = nextLectures.stream().map(Lecture::toString).collect(Collectors.joining("\n---------\n"));
      }
    } else if (event.getText().equals(messages.getMessage("sheets_url", null, locale))) {
      message = lectureService.getSheetUrl();
    }
    slackHelper.respond(event, "", Attachment.builder().text(message).build());
  }

  @Override
  public boolean test(MessagePostedEvent event) {
    return messages.getMessage("next_all", null, locale).equals(event.getText())
      || messages.getMessage("next", null, locale).equals(event.getText())
      || messages.getMessage("sheets_url", null, locale).equals(event.getText());
  }

}
