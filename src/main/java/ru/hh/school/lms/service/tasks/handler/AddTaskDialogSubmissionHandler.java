package ru.hh.school.lms.service.tasks.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.github.seratch.jslack.api.model.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.notifications.service.NotificationService;
import ru.hh.school.lms.service.tasks.TaskDtoFieldName;
import ru.hh.school.lms.service.tasks.TaskService;
import ru.hh.school.lms.service.tasks.model.Task;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.DialogSubmissionEvent;
import ru.hh.school.lms.slack.event.DialogSubmissionHandler;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.webhook.interactionexc.Error;
import ru.hh.school.lms.webhook.interactionexc.ErrorItem;
import ru.hh.school.lms.webhook.interactionexc.InteractionException;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


@Component
final class AddTaskDialogSubmissionHandler implements DialogSubmissionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(AddTaskDialogSubmissionHandler.class);

  private final TaskService taskService;
  private final NotificationService notificationService;
  private final SlackHelper slackHelper;
  private final ObjectMapper objectMapper;
  private final MessageSource messages;
  private final Locale locale;

  @Autowired
  public AddTaskDialogSubmissionHandler(TaskService taskService, NotificationService notificationService,
                                        SlackHelper slackHelper,
                                        @Qualifier("taskMessageSource") MessageSource messages, Locale locale, ObjectMapper objectMapper) {
    this.taskService = taskService;
    this.notificationService = notificationService;
    this.slackHelper = slackHelper;
    this.messages = messages;
    this.locale = locale;
    this.objectMapper = objectMapper;
  }

  @Override
  public void onEvent(DialogSubmissionEvent event) throws EventHandlingException {
    try {
      Task taskToAdd = objectMapper.readValue(event.getSubmission().toString(), Task.class);
      taskService.saveTask(taskToAdd);
      taskToAdd = taskService.findById(taskToAdd.getId());

      slackHelper.respond(
        event,
        "",
        Attachment.builder()
          .text(messages.getMessage("create_success", new Task[]{taskToAdd}, locale))
          .build()
      );

//      Long notificationId = notificationService.setNotification(new Notification(
//        new Timestamp(System.currentTimeMillis()).toLocalDateTime().plusSeconds(35),
//        "(Test notification) Создана новая задача: " + taskToAdd.toString(),
//        event.getUser().getId(),
//        ""
//      ));

    } catch (InvalidFormatException e) {
      LOGGER.info("Invalid format exception (deserialization error)");
      Error error = new Error();
      error.setErrors(List.of(new ErrorItem(
        TaskDialogElement.DEADLINE.getName(),
        "Сheck that date is entered correctly using example: 01/01/2000"
      ), new ErrorItem(
        TaskDtoFieldName.DEADLINE,
        "Сheck that date is entered correctly using example: 01/01/2000"
      )));
      throw new InteractionException(event, error);
    } catch (IOException e) {
      LOGGER.error("Map event.submission to Task failed (deserialization error)", e);
    }
  }
}
