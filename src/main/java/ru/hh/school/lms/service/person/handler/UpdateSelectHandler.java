package ru.hh.school.lms.service.person.handler;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.github.seratch.jslack.api.model.dialog.DialogElement;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.repository.PersonRepository;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.person.model.Person;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static ru.hh.school.lms.service.common.ManagementCrudAction.UPDATE;

@Component
final class UpdateSelectHandler implements MessageInteractionHandler {

  private static final int MAX_FIELD_LEN = 150;
  private static final int MIN_FIELD_LEN = 1;
  private final MessageSource messages;
  private final Locale locale;
  private final PersonRepository personRepository;
  private final SlackHelper slackHelper;
  private final Gson gson;

  public UpdateSelectHandler(@Qualifier("personMessageSource") MessageSource messages, Locale locale,
                             PersonRepository personRepository, SlackHelper slackHelper, Gson gson) {
    this.messages = messages;
    this.locale = locale;
    this.personRepository = personRepository;
    this.slackHelper = slackHelper;
    this.gson = gson;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    try {
      Long personToUpdateId = Long.valueOf(event.getActions().get(0)
        .getSelectedOptions().get(0).getValue());
      Optional<Person> personToUpdate = personRepository.findById(personToUpdateId);
      if (!personToUpdate.isPresent()) {
        throw new EventHandlingException(event, "There is no person with id=" + personToUpdateId);
      }
      List<DialogElement> elements = PersonDialogField.toDialogElements(personToUpdate.get());
      Dialog dialog = Dialog.builder()
        .callbackId(UPDATE.getId())
        .submitLabel(messages.getMessage("update_dialog_submit", null, locale))
        .title(messages.getMessage("update_dialog_title", null, locale))
        .elements(elements)
        .build();
      UpdateDialogState dialogState = new UpdateDialogState(personToUpdateId
        , event.getChannel().getId(), event.getOriginalMessage().getTs());
      slackHelper.dialog(event, dialog, gson.toJson(dialogState));
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has one action and one selected option");
    }
  }

  @Override
  public boolean test(MessageInteractionEvent event) {
    return UPDATE.getId().equals(event.getCallbackId());
  }
}
