package ru.hh.school.lms.service.notifications.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.seratch.jslack.api.methods.SlackApiException;
import com.github.seratch.jslack.api.model.Attachment;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.school.lms.service.notifications.dao.NotificationDao;
import ru.hh.school.lms.service.notifications.model.Notification;
import ru.hh.school.lms.slack.SlackHelper;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
class SpringDataNotificationService implements NotificationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(SpringDataNotificationService.class);

  private final NotificationDao notificationDao;
  private final ObjectMapper objectMapper;
  private final SlackHelper slackHelper;
  private final Gson gson;

  @Autowired
  public SpringDataNotificationService(NotificationDao notificationDao,
                                       ObjectMapper objectMapper,
                                       SlackHelper slackHelper, Gson gson) {
    this.notificationDao = notificationDao;
    this.objectMapper = objectMapper;
    this.slackHelper = slackHelper;
    this.gson = gson;
  }

  private LocalDateTime nextNotificationDate = null;

  @Override
  public LocalDateTime getNextNotificationDate() {
    return nextNotificationDate;
  }

  @Override
  public void setNextNotificationDate() {
    if (notificationDao.findFirstRowOrderByDateTime().isEmpty()) {
      nextNotificationDate = LocalDateTime.MAX;
    } else {
      nextNotificationDate = notificationDao.findFirstRowOrderByDateTime().get().getDateTime();
    }
  }

  @Override
  public Long setNotification(Notification notification) {
    notificationDao.addNotification(notification);
    nextNotificationDate = notificationDao.findFirstRowOrderByDateTime().get().getDateTime();
    return notification.getNotificationId();
  }

  @Override
  public void removeNotification(Long notificationId) {
    notificationDao.deleteById(notificationId);
  }

  @Override
  public void postAndDeleteExpiredNotifications(LocalDateTime timeStamp) {
    List<Notification> notifications = notificationDao.getLessThanEqualTimeStamp(timeStamp);
    for (Notification notification : notifications) {
      try {
        List<Attachment> attachments = gson.fromJson(notification.getAttachments(), new TypeToken<List<Attachment>>(){}.getType());
        slackHelper.notify(notification.getUserId(), notification.getText(), attachments);
        LOGGER.info("Notification \"" + notification.getText() + "\" send to Slack to user with ID: " + notification.getUserId());
        removeNotification(notification.getNotificationId());
      } catch (NullPointerException e) {
        LOGGER.info("There is no user in Slack with ID: " + notification.getUserId());
        removeNotification(notification.getNotificationId());
      } catch (JsonParseException e) { // IllegalStateException
        LOGGER.info("JSON format Exception in notification with ID: " + notification.getNotificationId());
        removeNotification(notification.getNotificationId());
      } catch (IOException | SlackApiException e) {
        LOGGER.error("Error with respond notification to channel", e);
      }
    }
  }
}
