package ru.hh.school.lms.service.lectures.handler;

import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.common.UpdateDialogState;
import ru.hh.school.lms.service.lectures.LectureService;
import ru.hh.school.lms.service.lectures.model.Lecture;
import ru.hh.school.lms.service.person.PersonService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessageInteractionEvent;
import ru.hh.school.lms.slack.event.MessageInteractionHandler;

import java.util.Locale;
import java.util.Optional;

@Component
final class UpdateLectureInteractionHandler implements MessageInteractionHandler {
  private final MessageSource messages;
  private final Locale locale;
  private final CommonHandleMethods commonMethods;
  private final SlackHelper slackHelper;
  private final Gson gson;
  private final LectureService lectureService;
  private final PersonService personService;

  @Autowired
  public UpdateLectureInteractionHandler(SlackHelper slackHelper,
                                         Gson gson,
                                         LectureService lectureService,
                                         PersonService personService,
                                         @Qualifier("lecturesMessageSource") MessageSource messages,
                                         Locale locale,
                                         CommonHandleMethods commonMethods) {
    this.slackHelper = slackHelper;
    this.gson = gson;
    this.lectureService = lectureService;
    this.personService = personService;
    this.messages = messages;
    this.locale = locale;
    this.commonMethods = commonMethods;
  }

  @Override
  public void onEvent(MessageInteractionEvent event) throws EventHandlingException {
    try {
      Long lectureToUpdateId = Long.valueOf(
        event.getActions().get(0)
          .getSelectedOptions().get(0)
          .getValue()
      );

      Optional<Lecture> lectureToUpdateOpt = lectureService.findById(lectureToUpdateId);

      if (lectureToUpdateOpt.isEmpty()) {
        throw new EventHandlingException(event, "There is no lecture with id=" + lectureToUpdateId);
      }
      Dialog dialog = commonMethods
        .lectureUpdateDialog(
          messages.getMessage("update_dialog_title", null, locale),
          lectureToUpdateOpt.get(),
          personService.findAllWithRole("LECTURER"));

      UpdateDialogState dialogState = new UpdateDialogState(
        lectureToUpdateId,
        event.getChannel().getId(),
        event.getOriginalMessage().getTs()
      );
      slackHelper.dialog(event, dialog, UpdateLectureDialogHandler.class, gson.toJson(dialogState));
    } catch (IndexOutOfBoundsException exc) {
      throw new EventHandlingException(event, "Expected that message has one action and one selected option");
    }
  }
}
