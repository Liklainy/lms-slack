package ru.hh.school.lms.service.grading.handler.message;


import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.model.dialog.Dialog;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import ru.hh.school.lms.service.grading.handler.data.GradingAttachmentsFactory;
import ru.hh.school.lms.service.grading.handler.data.SubmissionData;
import ru.hh.school.lms.service.grading.services.GradingService;
import ru.hh.school.lms.slack.SlackHelper;
import ru.hh.school.lms.slack.event.CallbackStorage;
import ru.hh.school.lms.slack.event.EventHandlingException;
import ru.hh.school.lms.slack.event.MessagePostedEvent;

import java.util.Locale;
import java.util.UUID;

@Component
public class GradingSubmitMessageHandler extends BaseGradingMessageHandler {

  private final Gson gson;

  @Autowired
  public GradingSubmitMessageHandler(
    GradingService gradingService,
    SlackHelper client,
    CallbackStorage callbackStorage,
    GradingAttachmentsFactory gradingAttachmentsFactory,
    Gson gson,
    @Qualifier("gradingMessageSource") MessageSource messages,
    Locale locale
  ) {
    super(gradingService, client, callbackStorage, gradingAttachmentsFactory, messages, locale);
    this.gson = gson;
  }

  @Override
  public void onEvent(MessagePostedEvent event) throws EventHandlingException {
    if (event.getContext().getSender().isEmpty()) {
      client.respond(event, messages.getMessage("not_available", null, locale));
      return;
    }
    Long userId = event.getContext().getSender().get().getId();

    String selectCallbackId = UUID.randomUUID().toString();
    String selectedCallbackId = UUID.randomUUID().toString();
    String dialogCallbackId = UUID.randomUUID().toString();

    Attachment selectAttachment = gradingAttachmentsFactory.getSelectHomeworkNotSubmitted(userId, selectCallbackId);

    callbackStorage.putInteractiveMessageConsumer(selectCallbackId, e -> {
      Long selectedHomeworkId = Long.parseLong(e.getActions().get(0).getSelectedOptions().get(0).getValue());

      Attachment selectedAttachment = gradingAttachmentsFactory.getSelectedHomeworkAttachment(selectedCallbackId, selectedHomeworkId);
      callbackStorage.putInteractiveMessageConsumer(selectedCallbackId, se -> {
        switch (se.getActions().get(0).getValue()) {
          case GradingAttachmentsFactory.BUTTON_SUBMIT_VALUE:
            try {

              Dialog dialog = gradingAttachmentsFactory.getSubmissionDialog(dialogCallbackId);

              callbackStorage.putDialogSubmissionConsumer(dialogCallbackId, de -> {
                SubmissionData data = gson.fromJson(de.getSubmission(), SubmissionData.class);
                gradingService.submitHomework(userId, selectedHomeworkId, data.githubUrl, data.description);

                client.update(e,
                  messages.getMessage("submitted_homework",
                    new Object[]{gradingAttachmentsFactory.getTaskNameById(selectedHomeworkId)},
                    locale));

                callbackStorage.removeInteractiveMessageConsumer(selectCallbackId);
                callbackStorage.removeInteractiveMessageConsumer(selectedCallbackId);
                callbackStorage.removeDialogSubmissionConsumer(dialogCallbackId);
              });

              client.dialog(se, dialog);
            } catch (NoSuchFieldException ex) {
              throw new EventHandlingException(se, ex.getMessage(), ex.getCause());
            }
            break;
          case GradingAttachmentsFactory.BUTTON_SELECT_AGAIN_VALUE:
            client.update(se, null, selectAttachment);
            break;
        }
      });

      client.update(e, null, selectedAttachment);
    });

    client.respond(event, null, selectAttachment);
  }

  @Override
  String regExpKey() {
    return "post_submit";
  }
}
