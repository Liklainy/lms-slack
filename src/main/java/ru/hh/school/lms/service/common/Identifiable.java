package ru.hh.school.lms.service.common;

import java.util.Arrays;
import java.util.stream.Collectors;

public interface Identifiable {

  default String getId() {
    return this.getClass().getCanonicalName() + "." + this.toString();
  }

  static <E extends Enum<E> & Identifiable> E ofId(String id, Class<E> clazz) {
    int lastPointIndex = id.lastIndexOf(".");
    if (lastPointIndex < 0 || lastPointIndex + 1 > id.length()) {
      throw new IllegalArgumentException("There is no action corresponding to id: " + id +
        ", available id values are:" +
        Arrays.stream(ManagementCrudAction.values()).map(ManagementCrudAction::getId).collect(Collectors.joining(", ")));
    }
    return E.valueOf(clazz, id.substring(lastPointIndex + 1));
  }
}
