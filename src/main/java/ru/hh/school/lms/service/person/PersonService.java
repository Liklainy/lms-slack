package ru.hh.school.lms.service.person;

import ru.hh.school.lms.service.person.model.Person;

import java.util.Optional;

public interface PersonService {

  Iterable<Person> findAll();

  Iterable<Person> findAllWithRole(String roleTitle);

  Optional<Person> findByID(Long id);

  Optional<Person> findBySlackAccountId(String slackAccountId);

  void deleteById(Long id);
}
