package ru.hh.school.lms.sheets;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Class for mapping database entities to spreadsheet. One should implement this interface for synchronisation.
 * One can force synchronisation by GoogleSheetsSynchronizer function synchronize().
 */

public interface GoogleSpreadsheetRelation {
  String SPREADSHEETS_URL_BASE = "https://docs.google.com/spreadsheets/d/";

  /**
   * @return name of spreadsheet to synchronize with (must be unique)
   */
  String getSpreadsheetTitle();

  /**
   * Function provide instruction for synchronisation. It called by GoogleSheetsSynchronizer function synchronize().
   *
   * @param client manages spreadsheet.
   */
  void mapDbToSpreadsheet(GoogleSheetsClient client, String spreadsheetId) throws IOException;

  default DateTimeFormatter getDateTimeFormatter() {
    return DateTimeFormatter.ofPattern("dd/MM/yyyy");
  }

  default String getSpreadsheetUrl() {
    return SPREADSHEETS_URL_BASE + getSpreadsheetsId();
  }

  default String getSpreadsheetsId() {
    return GoogleSheetsSynchronizer.getSpreadsheetId(this);
  }
}
