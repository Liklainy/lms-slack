package ru.hh.school.lms.sheets;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hh.nab.common.properties.FileSettings;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Configuration
public class GoogleSpreadsheetsConfig {
  private static final String GOOGLE_SHEETS_APPLICATION_NAME = "Google Sheets App";
  private static final String GOOGLE_DRIVE_APPLICATION_NAME = "Google Drive App";

  @Bean
  public GoogleSheetsClient googleSheetsClient(FileSettings settings) throws GeneralSecurityException, IOException {
    return new GoogleSheetsClient(googleSheets(settings), googleDrive(settings));
  }

  @Bean
  public Sheets googleSheets(FileSettings settings) throws GeneralSecurityException, IOException {
    return new Sheets.Builder(
      GoogleNetHttpTransport.newTrustedTransport(),
      JacksonFactory.getDefaultInstance(),
      credential(SheetsScopes.SPREADSHEETS, settings))
      .setApplicationName(GOOGLE_SHEETS_APPLICATION_NAME)
      .build();
  }

  @Bean
  public Drive googleDrive(FileSettings settings) throws GeneralSecurityException, IOException {
    return new Drive.Builder(
      GoogleNetHttpTransport.newTrustedTransport(),
      JacksonFactory.getDefaultInstance(),
      credential(DriveScopes.DRIVE, settings))
      .setApplicationName(GOOGLE_DRIVE_APPLICATION_NAME)
      .build();
  }

  private Credential credential(String scopes, FileSettings settings) throws IOException {
    final String credentialFile = settings.getString("ru.hh.school.lms.sheets.googleCredentialFilePath");
    InputStream is = new FileInputStream(credentialFile);
    return GoogleCredential.fromStream(is).createScoped(Collections.singleton(scopes));
  }

}
