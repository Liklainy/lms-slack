package ru.hh.school.lms.sheets;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GoogleSheetsSynchronizer {

  private static final Logger LOGGER = LoggerFactory.getLogger(GoogleSheetsSynchronizer.class);

  private final List<GoogleSpreadsheetRelation> relations;
  private final GoogleSheetsClient sheetsClient;
  private final Drive googleDrive;
  private final AnnotationConfigWebApplicationContext appContext;
  private static BiMap<String, GoogleSpreadsheetRelation> relationBySpreadsheetId = HashBiMap.create();

  @Autowired
  public GoogleSheetsSynchronizer(List<GoogleSpreadsheetRelation> relations,
                                  GoogleSheetsClient sheetsClient,
                                  Drive googleDrive,
                                  AnnotationConfigWebApplicationContext appContext) {
    this.relations = relations;
    this.sheetsClient = sheetsClient;
    this.googleDrive = googleDrive;
    this.appContext = appContext;
  }

  @EventListener
  private void initialSynchronization(ContextRefreshedEvent event) throws IOException {
    final ApplicationContext applicationContext = event.getApplicationContext();
    if (!appContext.equals(applicationContext)) {
      return;
    }
    indexSpreadsheets();
    synchronize();
  }

  // create corresponding spreadsheets by titles if not exist
  private void indexSpreadsheets() throws IOException {
    List<File> spreadsheets = googleDrive.files().list().setQ("mimeType='application/vnd.google-apps.spreadsheet'").execute().getFiles();
    List<String> spreadsheetNames = spreadsheets.stream().map(File::getName).collect(Collectors.toList());
    logAllSpreadsheets(spreadsheets);
    for (GoogleSpreadsheetRelation relation : relations) {
      String spreadsheetName = relation.getSpreadsheetTitle();
      if (spreadsheetNames.contains(spreadsheetName)) {
        int spreadsheetIndex = spreadsheetNames.indexOf(spreadsheetName);
        relationBySpreadsheetId.put(spreadsheets.get(spreadsheetIndex).getId(), relation);
        continue;
      }
      Spreadsheet createdSpreadsheet = sheetsClient.createSpreadsheet(spreadsheetName);
      logSpreadsheet("Created new spreadsheet",
        createdSpreadsheet.getProperties().getTitle(),
        createdSpreadsheet.getSpreadsheetId(),
        createdSpreadsheet.getSpreadsheetUrl()
      );
      relationBySpreadsheetId.put(createdSpreadsheet.getSpreadsheetId(), relation);
    }
  }

  public void synchronize() {
    // TODO: 31.03.2019 синхронизация с гугл таблицами делается параллельно, ибо их может быть много, а запросы дорогие
    new Thread(this::synchronizeAllSheets).start();
  }

  synchronized private void synchronizeAllSheets() {
    try {
      for (GoogleSpreadsheetRelation relation : relations) {
        relation.mapDbToSpreadsheet(sheetsClient, getSpreadsheetId(relation));
      }
      logUpdatedSpreadsheets(relations);
    } catch (IOException exc) {
      LOGGER.error("Google Sheets synchronization failed", exc);
    }
  }

  static String getSpreadsheetId(GoogleSpreadsheetRelation spreadsheetRelation) {
    return relationBySpreadsheetId.inverse().get(spreadsheetRelation);
  }

  private void logAllSpreadsheets(List<File> spreadsheets) {
    for (File spreadsheetFile : spreadsheets) {
      logSpreadsheet(
        "Have access to spreadsheet",
        spreadsheetFile.getName(),
        spreadsheetFile.getId(),
        GoogleSpreadsheetRelation.SPREADSHEETS_URL_BASE + spreadsheetFile.getId()
      );
    }
  }

  private void logUpdatedSpreadsheets(List<GoogleSpreadsheetRelation> spreadsheets) {
    for (GoogleSpreadsheetRelation spreadsheetRelation : spreadsheets) {
      logSpreadsheet(
        "Spreadsheet successfully updated",
        spreadsheetRelation.getSpreadsheetTitle(),
        spreadsheetRelation.getSpreadsheetsId(),
        spreadsheetRelation.getSpreadsheetUrl());
    }
  }

  private void logSpreadsheet(String logEvent, String spreadsheetTitle, String spreadsheetId, String spreadsheetUrl) {
    LOGGER.info(
      String.format(
        logEvent + ": {title='%s' | spreadsheetId='%s' | check url='%s'}",
        spreadsheetTitle,
        spreadsheetId,
        spreadsheetUrl
      )
    );
  }
}
