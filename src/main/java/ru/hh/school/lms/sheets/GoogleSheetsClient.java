package ru.hh.school.lms.sheets;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * В этом классе все функции делают get set обращения к (прямоугольным группам ячеек)/(range) некоторой таблицы(spreadsheet)
 */
public class GoogleSheetsClient {
  // предоставляет более гибкий и не очень удобный функционал управления табицами
  private final Sheets sheetsService;
  private final Drive googleDrive;
  // ValueInputOption == USER_ENTERED значит, что таблица будет обрабатывать текст так, как будто его ввел человек
  private static final String ALL_RANGE = "a:z";

  public GoogleSheetsClient(Sheets sheetsService, Drive googleDrive) {
    this.sheetsService = sheetsService;
    this.googleDrive = googleDrive;
  }

  public Sheets getSheets() {
    return sheetsService;
  }

  public List<List<Object>> getAllRows(String spreadsheetId) throws IOException {
    return getRange(spreadsheetId, ALL_RANGE);
  }

  public List<List<Object>> getRange(String spreadsheetId, String range) throws IOException {
    ValueRange readResult = sheetsService
      .spreadsheets()
      .values()
      .get(spreadsheetId, range)
      .execute();
    return readResult.getValues();
  }

  // cellLocation in "A1" (letter-number) notation
  public int setCell(String spreadsheetId, String cellLocation, String value) throws IOException {
    return setRange(spreadsheetId, cellLocation, Collections.singletonList(Collections.singletonList(value)));
  }

  public int setRange(String spreadsheetId, String range, List<List<Object>> values) throws IOException {
    ValueRange body = new ValueRange()
      .setValues(values);
    UpdateValuesResponse result = sheetsService.spreadsheets().values()
      .update(spreadsheetId, range, body)
      .setValueInputOption("USER_ENTERED")
      .execute();

    Integer updatedCells = result.getUpdatedCells();
    return updatedCells == null ? 0 : updatedCells;
  }

  public void setSheet(String spreadsheetId, String range, List<List<Object>> values) throws IOException {
    // TODO: 01.04.2019 avoid 2 requests (probably it's not possible: clear sheet + update)
    sheetsService.spreadsheets().values().clear(spreadsheetId, ALL_RANGE, new ClearValuesRequest()).execute();
    setRange(spreadsheetId, range, values);
  }

  Spreadsheet createSpreadsheet(String spreadsheetTitle) throws IOException {
    Spreadsheet newSpreadsheet = sheetsService.spreadsheets().create(
      new Spreadsheet().setProperties(new SpreadsheetProperties().setTitle(spreadsheetTitle))
    ).execute();

    // TODO: 09.04.2019 тут можно регулировать права доступа к созданной таблице, в т.ч. через email пользователей
    googleDrive.permissions().create(newSpreadsheet.getSpreadsheetId(), new Permission().setType("anyone").setRole("writer")).execute();

    return newSpreadsheet;
  }
}
