package ru.hh.school.lms;

import org.springframework.context.annotation.Import;
import ru.hh.school.lms.webhook.WebHookResource;

/**
 * Register resource that handles POST request of
 * slash commands, interactive components and dialog webhooks.
 */

@Import(WebHookResource.class)
public class JerseyConfig {
}
