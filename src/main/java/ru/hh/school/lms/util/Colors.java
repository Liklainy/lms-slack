package ru.hh.school.lms.util;

import java.awt.Color;

public final class Colors {

  // use static methods
  private Colors() {

  }

  public static String hex(Color color) {
    return String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
  }
}
