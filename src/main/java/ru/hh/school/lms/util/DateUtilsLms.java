package ru.hh.school.lms.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public final class DateUtilsLms {
  public static final String DATE_PATTERN = "dd/MM/yyyy";
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);

  // use static methods
  private DateUtilsLms() {
  }

  public static LocalDate parseEnteredDate(String date) {
    return LocalDate.parse(date, DATE_TIME_FORMATTER);
  }

  public static String dateFormat(LocalDate date) {
    return date.format(DATE_TIME_FORMATTER);
  }

  public static boolean isValidDate(String input) {
    try {
      parseEnteredDate(input);
      return true;
    } catch (DateTimeParseException ignore) {
      return false;
    }
  }
}
