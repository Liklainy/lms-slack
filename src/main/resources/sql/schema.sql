DROP TABLE IF EXISTS notification;
DROP TABLE IF EXISTS submission;
DROP TABLE IF EXISTS review;
DROP TABLE IF EXISTS task;
DROP TABLE IF EXISTS lecture;
DROP TABLE IF EXISTS privileges;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS role;

CREATE TABLE person (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    family_name VARCHAR(255) NOT NULL,
    patronymic VARCHAR(255),
    slack_account_id VARCHAR(255) UNIQUE NOT NULL,
    github_account_login VARCHAR(255)
);

CREATE TABLE role (
    id SERIAL PRIMARY KEY,
    title VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE privileges (
    id SERIAL PRIMARY KEY,
    person_id INTEGER REFERENCES person(id),
    role_id INTEGER REFERENCES role(id),
    UNIQUE (person_id, role_id)
);

CREATE TABLE lecture (
    id SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    title VARCHAR(255) NOT NULL,
    lecturer_id INTEGER REFERENCES person(id) NOT NULL,
    specialization VARCHAR(256),
    homework BOOLEAN
);

CREATE TABLE task (
    id SERIAL PRIMARY KEY,
    lecture_id INTEGER REFERENCES lecture(id),
    verifier_id INTEGER REFERENCES person(id),
    deadline DATE NOT NULL,
    task_url VARCHAR(255) NOT NULL,
    description VARCHAR(255)
);

CREATE TABLE submission (
    submission_id SERIAL PRIMARY KEY,
    student_id INTEGER,
    homework_id INTEGER,
    github_url VARCHAR(255),
    description VARCHAR(3000),
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE review (
    review_id SERIAL PRIMARY KEY,
    submission_id INTEGER,
    reviewer_id INTEGER,
    grade DOUBLE PRECISION,
    content VARCHAR(3000),
    created TIMESTAMP,
    updated TIMESTAMP
);

CREATE TABLE notification (
    notification_id SERIAL PRIMARY KEY,
    date_time TIMESTAMP NOT NULL,
    text VARCHAR(1023) NOT NULL,
    user_id VARCHAR(255) NOT NULL,
    attachments VARCHAR(2047) NOT NULL
);

