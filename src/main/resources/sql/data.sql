INSERT INTO person(first_name, family_name, patronymic, slack_account_id, github_account_login) VALUES
  ('Пётр', 'Еремейкин', 'Александрович', 'UGHDF337U', 'eremeykin'),
  ('Борис','Иванов', 'Николаевич', 'UGUF2KAR0', 'ivanov'),
  ('Федор','Кошкин', 'Ефимович', 'UGACSCXD5', 'ushakov'),
  ('Иван','Коневский', 'Николаевич', 'UGVCXVXB5', 'konevskiy'),
  ('Егор','Грибов', 'Васильевич', 'UGC2FKS', 'gribov'),
  ('Тимур','Бульба', 'Сергеевич', 'UGSF2YTK5', 'bulba'),
  ('Глеп','Жаров', 'Алексеевич', 'UBNCVBRFS', 'zharov'),
  ('Александр','Линьков', 'Евгеньевич', 'UGMYWR42', 'bober'),
  ('Иван','Воробьев', 'Андреевич', 'UGSCTYEX6', 'phasanov'),
  ('Николай','Вознесенский', 'Тимурович', 'UGSC25FG', 'kuk'),
  ('Владимир','Трубов', 'Ильич', 'UGDCA2SDW', 'trubov'),
  ('Какой','то', 'фиг', 'ASDFABRFS', 'fig'),
  ('Тимофей', 'Волгин', 'Андреевич', 'UGLRQ7CBH', 'volgin');

INSERT INTO role(title) VALUES
  ('ADMIN'),
  ('LECTURER'),
  ('STUDENT');

INSERT INTO privileges(person_id, role_id) VALUES
  (1,1),
  (2,1),
  (2,2),
  (3,3),
  (4,2),
  (5,3),
  (6,1),
  (7,2),
  (8,2),
  (9,2),
  (10,2),
  (12,3),
  (4,1),
  (5,2),
  (6,3),
  (7,3),
  (8,1),
  (13,3),
  (13,2),
  (13,1);

INSERT INTO lecture(date, title, lecturer_id, specialization, homework) VALUES
('03/05/2019', '*NIX CLI и bash', 8, 'All', True),
('12/05/2019', 'git', 8, 'All', True),
('15/04/2019', 'CSS и вёрстка, часть I', 9, 'Frontend', True),
('09/05/2019', 'jvm', 9, 'Backend', True),
('30/04/2019', 'Дизайн', 10, 'All', False),
('19/04/2019', 'Базовый доклад по Канбану. Игра getKanban', 10, 'All', False),
('28/06/2019', 'hibernate jdbc', 10, 'backend', True);

INSERT INTO task(lecture_id, verifier_id, deadline, task_url, description) VALUES
  (2, 8, '18/02/2019', 'https://github.com/alinkov/git-tasks', 'Команды git'),
  (3, 9, '29/04/2019', 'https://github.com/vorobey92/hibernate-hw', 'Необходимо поправить код тестов/сущностей так, чтобы проходили все тесты'),
  (4, 8, '05/05/2019', 'https://github.com/alinkov/test-practice', 'Необходимо покрыть Unit-тестами и интеграционными тестами какой-нибудь сервис (можно использовать классы из предыдущих домашних заданий).'),
  (6, 10, '30/04/2019', 'https://github.com/kincajou/hh-school', 'Задание про асинхронность');

INSERT INTO notification(date_time, text, user_id, attachments) VALUES
  ('2019-04-23T16:51:01', 'test notification1', 'user_1', 'attachments_1'),
  ('2019-04-23T16:51:41', 'test notification2', 'user_1', 'attachments_2'),
  ('2019-04-23T17:23:31', 'test notification3', 'user_1', 'attachments_3'),
  ('2019-04-23T17:24:11', 'test notification4', 'user_2', 'attachments_4'),
  ('2019-04-22T17:24:21', 'test notification5', 'user_2', 'attachments_5');
